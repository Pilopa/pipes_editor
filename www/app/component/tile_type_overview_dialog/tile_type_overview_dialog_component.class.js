/**
 * @module component/tileTypeOverviewDialog
 */
define (["jquery", "jquery.color", "mcustomscrollbar", "core/dialog_component.class"], function ($, $color, mCustomScrollbar, DialogComponent) {
	
	/**
	 * DialogComponent which shows a list of all tile types in the active level, excluding default ones.
	 * It allows for the modification of existing types and the creation of new tile types.
	 * 
	 * @param {Application} application An application to manipulate data of
	 * @param {TileTypeEditorDialogComponent}
	 * 
	 * @constructor
	 * @alias module:component/tileTypeOverviewDialog
	 * 
	 * @author Konstantin Schaper
	 * @since 1.0.1
	 */
	function TileTypeOverviewDialogComponent(application, tileTypeEditorDialogComponent) {
		// Super constructor call
		TileTypeOverviewDialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * @type {Applcation}
		 */
		this.application = application;
		
		/**
		 * @type {TileTypeEditorDialogComponent}
		 */
		this.tileTypeEditorDialogComponent = tileTypeEditorDialogComponent;
		
		/**
		 * @type {IScroll}
		 */
		this.scroller = undefined;
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	TileTypeOverviewDialogComponent.prototype = new DialogComponent;
	TileTypeOverviewDialogComponent.prototype.constructor = TileTypeOverviewDialogComponent;
	TileTypeOverviewDialogComponent.prototype.parent = DialogComponent.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	TileTypeOverviewDialogComponent.prototype.getViewURL = function() {
		return "app/component/tile_type_overview_dialog/tile_type_overview_dialog_view.html";
	};
	
	TileTypeOverviewDialogComponent.prototype.getStyleURL = function() {
		return "app/component/tile_type_overview_dialog/tile_type_overview_dialog_style.css";
	};
	
	TileTypeOverviewDialogComponent.prototype.bindEvents = function() {
		this.application.registerListener("onTileTypeAdded", (this.onTileTypeAdded).bind(this));
		this.application.registerListener("onTileTypeRemoved", (this.onTileTypeRemoved).bind(this));
		this.application.registerListener("onTileTypeUpdated", (this.onTileTypeUpdated).bind(this));
		this.application.registerListener("onLevelLoaded", (this.onLevelLoaded).bind(this));
		this.getView().find(".tile-type-create").click((this.onCreateButtonClick).bind(this));
	};
	
	TileTypeOverviewDialogComponent.prototype.initialize = function () {
		// Create scroll bar
		this.getView().find(".scroller").mCustomScrollbar({
			autoHideScrollbar: true,
			autoExpandScrollbar: true,
			scrollInertia: 100
		});
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method that is being called whenever a new level is loaded within the associated application's context.
	 * Resets the list.
	 */
	TileTypeOverviewDialogComponent.prototype.onLevelLoaded = function (error, level) {
		this.resetTileTypeList();
	};
	
	TileTypeOverviewDialogComponent.prototype.onTileTypeAdded = function (tileTypeClass) {
		this.addTileTypeWrapper(tileTypeClass.name).addClass("animation-added");
	};
	
	TileTypeOverviewDialogComponent.prototype.onTileTypeRemoved = function (tileTypeId) {
		this.getView().find(".tile-type.wrapper[data-ttid='" + tileTypeId + "']").animate({
			"backgroundColor": "#ff6666",
			"opacity": 0
		}, 750, "swing", function () {
			$(this).remove();
		});
	};
	
	TileTypeOverviewDialogComponent.prototype.onTileTypeUpdated = function (tileTypeClass) {
		this.getView().find(".tile-type.wrapper[data-ttid='" + tileTypeClass.name + "']").addClass("animation-updated");
	};
	
	TileTypeOverviewDialogComponent.prototype.onCreateButtonClick = function (event) {
		this.tileTypeEditorDialogComponent.switchFrom(this, {
			tileTypeClass: undefined,
			editable: true
		});
	};
	
	TileTypeOverviewDialogComponent.prototype.onViewButtonClick = function (event) {
		var that = event.data;
		var tileTypeId = $(this).parent().parent().data("ttid");
		var tileTypeClass = that.application.getTileType(tileTypeId);
		that.tileTypeEditorDialogComponent.switchFrom(that, {
			tileTypeClass: tileTypeClass,
			editable: false
		});
	};
	
	TileTypeOverviewDialogComponent.prototype.onEditButtonClick = function (event) {
		var that = event.data;
		var tileTypeId = $(this).parent().parent().data("ttid");
		var tileTypeClass = that.application.getTileType(tileTypeId);
		that.tileTypeEditorDialogComponent.switchFrom(that, {
			tileTypeClass: tileTypeClass,
			editable: true
		});
	};
	
	TileTypeOverviewDialogComponent.prototype.onDeleteButtonClick = function (event) {
		var that = event.data;
		var tileTypeId = $(this).parent().parent().data("ttid");
		that.application.removeTileType(tileTypeId);
	};
	
	TileTypeOverviewDialogComponent.prototype.resetTileTypeList = function () {
		// Store list view for later reference
		var listView = this.getView().find(".tile-type-list");
		
		// Empty the list to make space for new content
		listView.empty();
		
		// Get new list data to display
		var tileTypes = this.application.getTileTypes();
		
		// Fill the list view
		for (var tileTypeId in tileTypes) 
			this.addTileTypeWrapper(tileTypeId);
	};
	
	/**
	 * @returns {JQuery} The created tile type list element
	 */
	TileTypeOverviewDialogComponent.prototype.addTileTypeWrapper = function(tileTypeId) {
		// Store list view for later reference
		var listView = this.getView().find(".tile-type-list");
		var isDefault = this.application.isDefaultTileType(tileTypeId);
		
		// Create the container
		var wrapper = $("<li class='tile-type wrapper' data-ttid='" + tileTypeId + "'></li>");
		
		// Create the image
		var image = $("<div class='image'></div>")
		.css({
			"background-image": this.application.getTileTypeImageUrl(tileTypeId, this.application.activeLevel),
		});
		
		// Create the title
		var title = $("<div class='title'>" + tileTypeId + "</div>");
		
		// Create buttons container
		var buttonsContainer = $("<div class='buttons-container'></div>");
		
		if (isDefault) {
			// Create view button
			var viewButton = $("<button class='view ui-btn ui-btn-inline ui-icon-eye ui-btn-icon-notext ui-shadow-icon'></button>");
			viewButton.click(this, this.onViewButtonClick);
			
			// Put buttons into container
			buttonsContainer.append([viewButton]);
		} else {
			// Create edit button
			var editButton = $("<button class='edit ui-btn ui-btn-inline ui-icon-edit ui-btn-icon-notext ui-shadow-icon'></button>");
			editButton.click(this, this.onEditButtonClick);
			
			// Create delete button
			var deleteButton = $("<button class='delete ui-btn ui-btn-inline ui-icon-delete ui-btn-icon-notext ui-shadow-icon'></button>");
			deleteButton.click(this, this.onDeleteButtonClick);
			
			// Put buttons into container
			buttonsContainer.append([editButton, deleteButton]);
		}
		
		// Put the content in the wrapper
		wrapper.append([image, title, buttonsContainer]);
		
		// Add animation end listener
		wrapper.on("webkitAnimationEnd oanimationend msAnimationEnd animationend", function () {
			$(this).removeClass("animation-added animation-updated");
		});
		
		// Put the wrapper in the list view
		listView.append(wrapper);
		
		// Return the wrapper
		return wrapper;
	};

	// Return class definition to dependency container
	return TileTypeOverviewDialogComponent;
	
});