/**
 * @module component/helpSlidesDialog
 */
define (["jquery", "slick", "core/component.class", "core/dialog_component.class", "component/helpSlide"],
		function ($, slick, Component, DialogComponent, HelpSlideComponent) {

	/**
	 * DialogComponent which consists of a carousel to display HelpSlideComponents.
	 * The slides are defined in the overriden Component.initialize method.
	 * 
	 * @constructor
	 * @alias module:component/helpSlidesDialog
	 * 
	 * @author Konstantin Schaper
	 */
	function HelpSlidesDialogComponent () {
		// Super constructor call
		HelpSlidesDialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * The dialog that opened this dialog.
		 * 
		 * @type {DialogComponent}
		 */
		this.fromDialog = undefined;
		
		/**
		 * The initial slide to display.
		 * 
		 * @type {Integer}
		 */
		this.keywordId = undefined;
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	HelpSlidesDialogComponent.prototype = new DialogComponent;
	HelpSlidesDialogComponent.prototype.constructor = HelpSlidesDialogComponent;
	HelpSlidesDialogComponent.prototype.parent = DialogComponent.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	HelpSlidesDialogComponent.prototype.getViewURL = function() {
		return "app/component/help_slides_dialog/help_slides_dialog_view.html";
	};
	
	HelpSlidesDialogComponent.prototype.getStyleURL = function() {
		return "app/component/help_slides_dialog/help_slides_dialog_style.css";
	};
	
	HelpSlidesDialogComponent.prototype.bindEvents = function() {
		this.getView().on("popupafteropen", (this.onOpen).bind(this));
	};
	
	HelpSlidesDialogComponent.prototype.initialize = function () {
		// Create carousel
		this.carousel = this.getView().find(".carousel");
		this.carousel.slick({
			dots: true
		});
		this.carousel.on("afterChange", (this.afterChange).bind(this));
		
		// Create slides
		var slides = [
		
			new HelpSlideComponent(this.carousel, 0, "The Game", "<b>'Pipes' is a single player 2d puzzle game</b><br>" +
					" (<i>This application also includes an editor to create custom puzzle levels</i>).<br>" +
					"<br>" +
					" The playfield is a square grid, where each square on the grid may have a tile placed on it." +
					" Tiles both describe the layout of a level, as well as offering the player to solve the puzzle it presents." +
					" There are several types of tiles for different purposes." +
					" Each tile placed by the player can be manipulated in the following ways: rotate, swap, delete. Pre-defined tiles cannot be modified in such ways." +
					" There are multiple Liquids of different colours.<br>" +
					"<br>" +
					"Tiles can have special functionality attached to them:<br> " +
					"- Source (Serves as a starting point for a stream of liquid in a specific colour)<br>" +
					"- Destination (Must be filled with a specific colour)<br>" +
					"<br>" +
					"To successfully solve a puzzle level, each destination must be reached by a liquid of appropriate colour and no errors must occur. Possible errors are:<br>"+
					"1. Two streams of unequal colours collide<br>" +
					"2. Leaking a liquid onto an empty tile<br>" +
					"3. Leaking a liquid onto an adjacent tile with no opening in this direction", ""),
			
			new HelpSlideComponent(this.carousel, 1, "Modes", "This application has two modes of operation:<br>" +
					"- First the <span class='text-highlighting-a'>Editor Mode</span> where you create, save and load puzzle levels. You can also define which and how many tiles the player can place.<br>" +
					"- Second the <span class='text-highlighting-b'>Play Mode</span> where you can test levels and verify your solution to the puzzle.", "app/asset/img/help_slide/help_slide_modes.png"),
			
			new HelpSlideComponent(this.carousel, 2, "Menues", "There are two different types of menues:<br>" +
					"- First the <span class='text-highlighting-a ui-icon-bars ui-btn-icon-left'>Main Menu</span> which provides Navigation and lets you change the current Application Mode (See '<span class='link' data-keywordid='1'>Modes</span>').<br>" +
					"- Second the <span class='text-highlighting-b ui-icon-edit ui-btn-icon-left'>Toolbox (See '<span class='link' data-keywordid='3'>Toolbox</span>')</span> which allows you to interact with the playfield.<br>" +
					"<br><b>The functionality and appearance of both menues is dependant on the current Application Mode!</b>"
					, ""),
			
			new HelpSlideComponent(this.carousel, 3, "Toolbox", "<b>The toolbox allows you to place tiles on the playfield by dragging the image of a tool onto the desired square.</b><br>" +
					"<br>" +
					"<span class='text-highlighting-a'>Editor Mode</span>: You can change the number of tiles of each type a player who plays your level can place. To do so, adjust " + 
					"the value in the input field on the left of each tool. If you enter a zero, the associated tool will not be available to the player in 'Play Mode'. You can of course " +
					"still place any number of those tiles in 'Editor Mode'. " +
					"All tiles placed in Editor Mode become fixed in 'Play Mode' (See '<span class='link' data-keywordid='0'>The Game</span>') and cannot be manipulated anymore.<br>" +
					"<br>" +
					"<span class='text-highlighting-b'>Play Mode Mode</span>: Each time you place a tile in 'Play Mode', it reduces the number of further tiles of that type you can place by one." +
					"If this number reaches zero, you cannot place any more tiles of that type until you remove one from the playfield.<br>" +
					"<br>" +
					"(See '<span class='link' data-keywordid='1'>Modes</span>')"
					, ""),
			
			new HelpSlideComponent(this.carousel, 4, "Settings", "The<span class='text-highlighting-a ui-icon-bars ui-btn-icon-left'>Settings</span> submenu gives you the option" +
					"to define two types of settings:<br>" +
					"<br>" +
					"<span class='text-highlighting-b'>Level Settings</span> allow you to modify the general definition of your level, such as its dimensions." +
					" Those settings are tied to the level and will change once you close the application or load a different level.<br>" +
					"<span class='text-highlighting-c'>Editor Settings</span> provide customization options for your user experience. They remain after an application " +
					"restart or when a new level is loaded.", "")
				
		];
		
		// Inject all views in order
		var carousel = this.getView().find(".carousel");
		
		return Component.injectAll(
				slides,
				carousel,
				true
		);
	};
	
	// === DialogComponent ===
	
	HelpSlidesDialogComponent.prototype.switchFrom = function (fromDialog, keywordId) {
		this.keywordId = keywordId;
		HelpSlidesDialogComponent.prototype.parent.switchFrom.call(this, fromDialog);
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method that is called whenever this dialog is opened.
	 * Responsible for moving the cursor of the carousel to the slide selected in a previous menu.
	 */
	HelpSlidesDialogComponent.prototype.onOpen = function (event) {
		this.carousel.slick("slickGoTo", this.keywordId, true);
	};
	
	/**
	 * Event handler method that is called whenever the carousel slide is changed.
	 * Changes the title of the dialog to the name of the keyword displayed on the active slide.
	 */
	HelpSlidesDialogComponent.prototype.afterChange = function (event, slick, currentSlide) {
		this.getView().find("h1").text($(slick.$slides.get(currentSlide)).data("title"));
	};

	// Return class definition to dependency container
	return HelpSlidesDialogComponent;
});