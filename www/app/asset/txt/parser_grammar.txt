/* === Initializer === */
{
    
    // Reserved keywords
    var reservedKeywords = [
    	"TileType",
        "define",
        "override",
        "with",
        "function",
        "takes",
        "nothing",
        "return",
        "if",
        "else",
        "endif",
        "endfunction",
        "null",
        "string",
        "integer",
        "number",
        "float",
        "array",
        "tile",
        "and",
        "or"
    ];

	// Value Definitions
    var baseOpenings = undefined;
    var additionalValuesLayout = undefined;
    
    // Function Overrides
    var isDestinationOverride = undefined;
    var isSourceOverride = undefined;
    var isRotatableOverride = undefined;
    var isMovableOverride = undefined;
    var getPointValueOverride = undefined;
    var getImageNameOverride = undefined;
    var getExitsOverride = undefined;
    var preEntryOverride = undefined;
    var postEntryOverride = undefined;
    var preExitOverride = undefined;
    var postExitOverride = undefined;
    
    // Get Array Type
    function getArrayType(typeString) {
    	return typeString.split("[")[1].split("]")[0];
    };
    
    // Verify type
    function verifyType(givenType, expectedType) {
    	return givenType.toLowerCase() === expectedType.toLowerCase();
    };
    
    // Verify Variable Name
    function variableNameNotTakenOrIllegal(varName) {
    	return !(Object.keys(getLocals()).indexOf(varName) === -1 && 
        		Object.keys(getGlobals()).indexOf(varName) === -1 &&
        		Object.keys(reservedKeywords).indexOf(varName) === -1);
    };
    
    function variableExists(varName) {
    	return localVariableExists(varName) &&
        		globalVariableExists(varName);
    };
    
    function localVariableExists(varName) {
    	return typeof getLocals()[varName] != "undefined";
    };
    
    function globalVariableExists(varName) {
    	return typeof getGlobals()[varName] != "undefined";
    };
    
    // Variable Definition
    function Variable (type, name, value) {
    	return {
        	"type": type,
            "name": name,
            "value": value
        };
    };
    
    // Function Definition
    function FunctionDefinition(returnType, args, name, code) {
    	var result = Variable("function", name, code);
        result.args = args;
        result.returnType = returnType;
    };
    
    // Scope Variables
    var curFunc = [];
    var curVars = [];
    var depth = 0;
    
    // Define Globals
    curVars[0] = {
    	"ActiveTile": Variable("tile", "ActiveTile", "this")
    };
    
    function getGlobals() {
    	return curVars[0];
    };
    
    function getLocals() {
    	return curVars[depth];
    }
}

/* === Main definition === */

tileTypeDefinition 
	= "TileType:" _ typeName:Text __ (tileTypeValueDefinition / tileTypeMethodOverride)* _
    { 
    	// TileTypeIds are strictly lower case
    	typeName = typeName.toLowerCase();
    
        var resultedCode = "function " + typeName + "(preplaced, position, additionalValues, initialRotation) {\n"
        + "\t" + "this.getAdditionalValues = function() {return additionalValues;};\n"
        + "\t" + (typeof baseOpenings !== "undefined" ? baseOpenings : "var baseOpenings = undefined;") + "\n"
        + "\t" + typeName + ".prototype.parent.constructor.call(this, preplaced, position,\n\t baseOpenings, additionalValues, initialRotation);\n"
        + "};\n"
        + typeName + ".prototype = Object.create(Tile.prototype);\n"
        + typeName + ".prototype.constructor = " + typeName + ";\n"
        + typeName + ".prototype.parent = Tile.prototype;\n"
        + typeName + ".prototype.getTileTypeId = function () {"
        +	"return " + '"' + typeName + '"' + ";"
        + "};\n"
        + (typeof additionalValuesLayout !== "undefined" ? typeName + ".additionalValues = " + JSON.stringify(additionalValuesLayout) + ";\n" : "");
        
       	var typeOfIsRotatableOverride = typeof isRotatableOverride;
        if (typeOfIsRotatableOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.isRotatable = "
            + (isRotatableOverride.type === "boolean" ? "function() {return " + isRotatableOverride.value + ";}" :
            isRotatableOverride.value) + ";\n";
        }
        
        var typeOfIsMovableOverride = typeof isMovableOverride;
        if (typeOfIsMovableOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.isMovable = "
            + (isMovableOverride.type === "boolean" ? "function() {return " + isMovableOverride.value + ";}" :
            isMovableOverride.value) + ";\n";
        }
        
        var typeOfGetPointValueOverride = typeof getPointValueOverride;
        if (typeOfGetPointValueOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.getPointValue = "
            + (getPointValueOverride.type === "integer" ? "function() {return " + getPointValueOverride.value + ";}" :
            getPointValueOverride.value) + ";\n";
        }
        
        var typeOfIsSourceOverrideOverride = typeof isSourceOverride;
        if (typeOfIsSourceOverrideOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.isSource = "
            + (isSourceOverride.type === "boolean" ? "function() {return " + isSourceOverride.value + ";}" :
            isSourceOverride.value) + ";\n";
        }
        
        var typeOfIsDestinationOverride = typeof isDestinationOverride;
        if (typeOfIsDestinationOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.isDestination = "
            + (isDestinationOverride.type === "boolean" ? "function() {return " + isDestinationOverride.value + ";}" :
            isDestinationOverride.value) + ";\n";
        }
        
        var typeOfGetImageNameOverride = typeof getImageNameOverride;
        if (typeOfGetImageNameOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.getBaseImageURL = "
            + (getImageNameOverride.type === "string" ? "function() {return " + getImageNameOverride.value + ";}" :
            getImageNameOverride.value) + ";\n";
        }
        
        var typeOfGetExitsOverride = typeof getExitsOverride;
        if (typeOfGetExitsOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.getExits = " + getExitsOverride.value + ";\n";
        }
        
        var typeOfPreEntryOverride = typeof preEntryOverride;
        if (typeOfPreEntryOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.preEntry = " + preEntryOverride.value + ";\n";
        }
        
        var typeOfPostEntryOverride = typeof postEntryOverride;
        if (typeOfPostEntryOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.postEntry = " + postEntryOverride.value + ";\n";
        }
        
        var typeOfPreExitOverride = typeof preExitOverride;
        if (typeOfPreExitOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.preExit = " + preExitOverride.value + ";\n";
        }
        
        var typeOfPostExitOverride = typeof postExitOverride;
        if (typeOfPostExitOverride !== "undefined") {
        	resultedCode += typeName + ".prototype.postExit = " + postExitOverride.value + ";\n";
        }
        
        resultedCode += typeName + ".prototype.getTileTypeDefinitionCode = function(){\nreturn " + JSON.stringify(text()) + ";\n};\n";

		resultedCode += "return " + typeName + ";";

    	return resultedCode;
    }
    
/* === AdditionalValuesObject === */

AdditionalValuesObject = 
	"{" _ first:additionalValuesObjectProperty _ others:("," _ additionalValuesObjectProperty)* _ "}" {
    	var othersString = "";
        
        for (var i = 0; i < others.length; i++) {
        	var other = others[i];
            var otherString = "," + other.slice(2, other.length);
            othersString += otherString;
        }

        return "{" + first + othersString + "}";
    }
    
additionalValuesObjectProperty =
	name:String _ objectPropertyDefinitionSeparator _ value:(additionalNumberLayout / additionalColorLayout / additionalDirectionLayout) {
    	return name.value.toLowerCase() + ':' + value;
    }
    
/* - Additional Number - */
    
additionalNumberLayout =
	_ "{" _ first:additionalNumberLayoutType _ others:("," _ (additionalNumberLayoutMin / additionalNumberLayoutMax / additionalNumberLayoutDefault))* _ "}"
    {
   		var othersString = "";
        
        for (var i = 0; i < others.length; i++) {
        	var other = others[i];
            var otherString = "," + other.slice(2, other.length);
            othersString += otherString;
        };

        return "{" + first + othersString + "}";
    }
    
additionalNumberLayoutType =
	"type" _ objectPropertyDefinitionSeparator _ "integer" {
    	return '"type":' + '"integer"';
    }
   
additionalNumberLayoutMin =
	"min" _ objectPropertyDefinitionSeparator _ value:Integer {
    	return '"min":' + value;
    }
    
additionalNumberLayoutMax =
	"max" _ objectPropertyDefinitionSeparator _ value:Integer {
    	return '"max":' + value;
    }
    
additionalNumberLayoutDefault =
	"default" _ objectPropertyDefinitionSeparator _ value:Integer {
    	return '"default":' + value;
    }
    
/* - Additional Color - */
    
additionalColorLayout =
	_ "{" _ first:additionalColorLayoutType _ "}"
    {
        return "{" + first + "}";
    }
    
additionalColorLayoutType =
	"type" _ objectPropertyDefinitionSeparator _ "color" {
    	return '"type":' + '"color"';
    }
    
/* - Additional Direction - */
    
additionalDirectionLayout =
	_ "{" _ first:additionalDirectionLayoutType _ "}"
    {
        return "{" + first + "}";
    }
    
additionalDirectionLayoutType =
	"type" _ objectPropertyDefinitionSeparator _ "direction" {
    	return '"type":' + '"direction"';
    }
    
/* === BaseOpeningObject === */

BaseOpeningObject = 
	"{" _ first:baseOpeningObjectProperty _ others:("," baseOpeningObjectProperty)* _ "}" {
        var othersString = "";
        
        for (var i = 0; i < others.length; i++) {
        	var other = others[i];
            othersString += other.slice(1, other.length);
        }
        
        return "var baseOpenings = {};\n" + first + othersString;
    }
    
baseOpeningObjectProperty =
	_ nameVariable:directionValue _ objectPropertyDefinitionSeparator _ valueVariable:colorValue _ {
    	return "\tbaseOpenings[" + '' + nameVariable.value + ']=' + valueVariable.value + ";\n";
    }
    
/* === Object === */

objectProperty =
	_ '"' name:Text '"' _ objectPropertyDefinitionSeparator _ value:value _
    
objectPropertyDefinitionSeparator =
	"=>"
 
Object = 
	"{" _ (objectProperty)* _ "}"
    
/* ==================================================== */
/* === 			  Tile Type Modifications			=== */
/* ==================================================== */
    
tileTypeValueDefinition
	=  _ (baseOpeningsDefinition / additionalValuesLayoutDefinition) _
    
tileTypeMethodOverride
	= _ (isRotatableOverride / isMovableOverride / getPointValueOverride / isSourceOverride / isDestinationOverride / getImageNameOverride / getExitsOverride
    	/ preEntryOverride / postEntryOverride / preExitOverride / postExitOverride) _
    
/* === Value definitions === */
    
baseOpeningsDefinition 
	= "define" " " _ "BaseOpenings" " " "as" " " _ value:BaseOpeningObject {
    	if (typeof baseOpenings === "undefined")
    		baseOpenings = value;
        else
        	error('"BaseOpenings" may only be defined once!');
    }
    
additionalValuesLayoutDefinition 
	= "define" " " _ "AdditionalValuesLayout" " " "as" " " _ value:AdditionalValuesObject {
    	if (typeof additionalValuesLayout === "undefined")
    		additionalValuesLayout = JSON.parse(value);
        else
        	error('"AdditionalValuesLayout" may only be defined once!');
    }

/* === Method overrides === */

isRotatableOverride =
	_ "override" " " _ "isRotatable" " " "with" " " _ override:(FALSE / functionDefinition) {
    	if (typeof isRotatableOverride === "undefined")
    		isRotatableOverride = override;
        else
        	error('"isRotatable" may only be overridden once!');
    }
    
isMovableOverride =
	_ "override" " " _ "isMovable" " " "with" " " _ override:(FALSE / functionDefinition) {
    	if (typeof isMovableOverride === "undefined")
    		isMovableOverride = override;
        else
        	error('"isMovable" may only be overridden once!');
    }
    
getPointValueOverride =
	_ "override" " " _ "getPointValue" " " "with" " " _ override:(integerValue / functionDefinition / arithmetic) {
    	if (typeof getPointValueOverride === "undefined")
    		getPointValueOverride = override;
        else
        	error('"getPointValue" may only be overridden once!');
    }
    
isSourceOverride =
	_ "override" " " _ "isSource" " " "with" " " _ override:(booleanValue / functionDefinition) {
    	if (typeof isSourceOverride === "undefined")
    		isSourceOverride = override;
        else
        	error('"isSource" may only be overridden once!');
    }
    
isDestinationOverride =
	_ "override" " " _ "isDestination" " " "with" " " _ override:(booleanValue / functionDefinition) {
    	if (typeof isDestinationOverride === "undefined")
    		isDestinationOverride = override;
        else
        	error('"isDestination" may only be overridden once!');
    }
    
getImageNameOverride =
	_ "override" " " _ "getImageName" " " "with" " " _ override:(stringValue / functionDefinition) {
    	if (typeof getImageNameOverride === "undefined")
    		getImageNameOverride = override;
        else
        	error('"getImageName" may only be overridden once!');
    }
    
getExitsOverride =
	_ "override" " " _ "getExits" " " "with" " " _ override:(functionDefinition) {
    	if (typeof getExitsOverride === "undefined")
    		getExitsOverride = override;
        else
        	error('"getExits" may only be overridden once!');
    }    
    
preEntryOverride =
	_ "override" " " _ "preEntry" " " "with" " " _ override:(functionDefinition) {
    	if (typeof preEntryOverride === "undefined")
    		preEntryOverride = override;
        else
        	error('"preEntry" may only be overridden once!');
    }    
    
postEntryOverride =
	_ "override" " " _ "postEntry" " " "with" " " _ override:(functionDefinition) {
    	if (typeof postEntryOverride === "undefined")
    		postEntryOverride = override;
        else
        	error('"postEntry" may only be overridden once!');
    }  
    
preExitOverride =
	_ "override" " " _ "preExit" " " "with" " " _ override:(functionDefinition) {
    	if (typeof preExitOverride === "undefined")
    		preExitOverride = override;
        else
        	error('"preExit" may only be overridden once!');
    }  
    
postExitOverride =
	_ "override" " " _ "postExit" " " "with" " " _ override:(functionDefinition) {
    	if (typeof postExitOverride === "undefined")
    		postExitOverride = override;
        else
        	error('"postExit" may only be overridden once!');
    }  

/* ==================================================== */
/* === 				Programming Logic				=== */
/* ==================================================== */

/* === Classes === */

objectValueAccessor = "."

/* === Tile === */

TileType = "tile"
    
/* - Functions - */

tileFunctionCall = 
	getAdditionalValue / getBaseDirection / getRotatedDirection  / getRotation
    
/* Additional Values */

getAdditionalValue =
	tile:Tile objectValueAccessor "getAdditionalValue" _ "(" _ keyVariable:String &{
    	return typeof additionalValuesLayout[JSON.parse(keyVariable.value)] !== "undefined"
    }
    _ ")"
    {
        return Variable(additionalValuesLayout[JSON.parse(keyVariable.value)].type, undefined, tile.value + ".getAdditionalValues()[" + keyVariable.value + "]");
    }

/* Returns the base value for the given direction (Subtracts the tile's rotation from it) */
getBaseDirection =
	tile:Tile objectValueAccessor "getBaseDirection" "(" _ direction:directionValue _ ")" {
    	return Variable("direction", undefined, tile.value + ".getBaseDirection(" + direction.value + ")");
    }
    
/* Returns the rotated value for the given direction (Adds the tile's rotation to it) */
getRotatedDirection =
	tile:Tile objectValueAccessor "getRotatedDirection" "(" _ direction:directionValue _ ")" {
    	return Variable("direction", undefined, tile.value + ".getRotatedDirection(" + direction.value + ")");
    }
    
/* Returns the current rotation */
getRotation =
	tile:Tile objectValueAccessor "getRotation" "(" _ ")" {return Variable("direction", undefined, tile.value + ".getRotation()");}

/* === Stream === */

StreamType = "stream"
    
/* - Functions - */

streamFunctionCall = 
	streamGetColor / streamSetColor / streamGetCurrentDirection

/* Returns the current color of this stream,
 * returns Color.NONE if it is empty. */
streamGetColor =
	stream:Stream objectValueAccessor "getColor" "(" _ ")" {
    	return Variable("color", undefined, stream.value + ".getColor()");
    }
    
/* Overrides the active color of the stream with the given color. */
streamSetColor =
	stream:Stream objectValueAccessor "setColor" "(" _ newColor:colorValue _ ")" {
    	return Variable("nothing", undefined, stream.value + ".setColor(" + newColor.value + ")");
    }
    
/* Returns the direction from which the nextTile is going to be evaluated. */
streamGetCurrentDirection =
	stream:Stream objectValueAccessor "getCurrentDirection" "(" _ ")" {
    	return Variable("direction", undefined, stream.value + ".getNextEntryDirection()");
    }

/* === Input === */

value 
	= returnValue:(integerValue / stringValue / booleanValue / colorValue / directionValue / functionDefinition / Variable / Instantiation / NOTHING) {
        return returnValue;
    }

comparableValue 
	= Integer / String / Boolean / Color / Direction / functionCall / Variable / NOTHING

directionValue
 	= value:(directionAccess / functionCall / Variable) &{
    	return value.type === "direction"
    } {
    	return value;
    }
    
colorValue
 	= value:(colorAccess / functionCall / Variable) &{
    	return value.type === "color"
    } {
    	return value;
    }
    
booleanValue
 	= value:(Boolean / booleanExpression / functionCall / Variable) &{
    	return value.type === "boolean"
    } {
    	return value;
    }
    
integerValue
 	= value:(Integer / functionCall / Variable) &{
    	return value.type === "integer"
    } {
    	return value;
    }
    
floatValue
 	= value:(Integer / functionCall / Variable) &{
    	return value.type === "float"
    } {
    	return value;
    }
    
numberValue
 	= value:(Integer / functionCall / Variable) &{
    	return value.type === "number" || value.type === "integer" || value.type === "float"
    } {
    	return value;
    }
    
stringValue
 	= value:(String / functionCall / Variable) &{
    	return value.type === "string"
    } {
    	return value;
    }
    
arrayValue = arrayVariable:Variable &{
	if (typeof arrayVariable === "undefined" || typeof arrayVariable === "string") return false;
	else if (arrayVariable.type.startsWith("array")) return true;
	else return false;
} {
    return arrayVariable;
}

tileValue = tileVariable:Variable &{
	return typeof tileVariable !== "undefined" && tileVariable.type === "tile"
} {
    return tileVariable;
}

streamValue = streamVariable:Variable &{
	return typeof streamVariable !== "undefined" && streamVariable.type === "stream"
} {
    return streamVariable;
}

/* === Variables === */

variableAccess = localVariableAccess / globalVariableAccess
    
localVariableAccess =
	name:$Text &{ return localVariableExists(name)} { 
    	return getLocals()[name];
    }
    
globalVariableAccess =
	name:$Text &{ return globalVariableExists(name)} { 
    	return getGlobals()[name];
    }
    
variableDefinition =
	variableInitialization / variableDeclaration

variableDeclaration = "local" " " varType:$Type " " varName:Text {
    	if (!variableNameNotTakenOrIllegal(varName)) {
                
            // Remember variable
        	curVars[depth][varName] = Variable(varType, varName, varName);
            
            // Initialize with default value
            var defaultValue = undefined;
            if (varType === "integer" || varType === "number") defaultValue = JSON.stringify(0);
            else if (varType === "float") defaultValue = JSON.stringify(0.0);
            else if (varType === "string") defaultValue = JSON.stringify("");
            else if (typeof varType === "string" && varType.startsWith("array")) defaultValue = JSON.stringify([]);
            else error('Variables of type "' + varType + '" cannot be declared without initialization.');
            
            // Return javascript string
            return "var " + varName + "=" + defaultValue;
            
        } else
        	error('The variable name "' + varName + '" is already taken or illegal to define.');
    	}
	
    
variableInitialization = "local" " " varType:$Type " " varName:Text _ "=" _ varValue:value {
    	if (!variableNameNotTakenOrIllegal(varName)) {
        	
        	// Verify types
        	if (varValue.type !== varType) {
            	error("Expected " + varType + " but " + varValue.type + " found.");
            }
                
            // Remember variable
        	curVars[depth][varName] = Variable(varType, varName, varName);
            
            // Return javascript string
            return "var " + varName + "=" + varValue.value;
            
        } else
        	error('The variable name "' + varName + '" is already taken or illegal to define.');
    	}
    
variableAssignment = "set" " " varName:Text _ "=" _ varValue:value {
    	if (!variableNameNotTakenOrIllegal(varName)) {
        
        	// Get the variable
            var variable = curVars[depth][varName];
        	
        	// Verify types
        	if (varValue.type !== variable.type) {
            	error("Expected " + variable.type + " but " + varValue.type + " found.");
            }
            
            // Return javascript string
            return varName + "=" + varValue.value;
            
        } else
        	error('The variable with name "' + varName + '" must not be assigned.');
    	}
    
/* === Functions === */

/* Add links to all function call types here */
functionCall = arrayFunctionCall / tileFunctionCall / streamFunctionCall

functionCallStatement = "call" " " _ functionCall:functionCall {
	return functionCall.value;
}

functionDefinition
    = "function" clearVars "takes" _ params:("nothing" / (args:parameterDefinition+ !{curFunc[depth].args = args;})) _ "returns" " " _ returnType:$ReturnType !{curFunc[depth].returnType = returnType;} _ body:functionBody _ "endfunction" {
        // After all inner elements have been parsed, build the function object
        curFunc[depth].value = "function(" + (params === "nothing" ? "" : params[0]) + "){" + body + "}";
        
        // Remember function variable to return and reduce depth
        var returnFunc = curFunc[depth];
        depth--;
        return returnFunc;
    }  
    
/* Updates the scope for the current function */
clearVars =
	_ !{
        depth++;
    	curVars[depth] = {};
        curFunc[depth] = {"type": "function"};
    }
    
parameterDefinition = 
	type:Type " " name:Text {
    	if (!variableNameNotTakenOrIllegal(name)) {
    		curVars[depth][name] = Variable(type, name, name);
        } else {
        	error('The parameter variable with name "' + name + '" must not be redefined.');
        }
    	return name;
    }
    
functionBody = 
	multipleOptionalStatements

multipleOptionalStatements =
	statements:singleStatement* {
    	var returnText = "";
        
        for (var index = 0; index < statements.length; index++) {
        	returnText += statements[index];
        }
        
        return returnText;
    }
    
multipleStatements =
	statements:singleStatement+ {
    	var returnText = "";
        
        for (var index = 0; index < statements.length; index++) {
        	returnText += statements[index];
        }
        
        return returnText;
    }
    
singleStatement = 
	_ value:(ifBlock / nonIfStatement) {
    	return value + ";\n";
    }
    
nonIfStatement 
	= functionCallStatement / variableDefinition / variableAssignment / returnStatement / whileStatement
    
/* === Statements === */
    
returnStatement =
	"return" _ value:(value) {
    	if (value.type !== curFunc[depth].returnType) {
        	error ("The type of the returned value does not match this functions definition (Expected: "+curFunc[depth].returnType+", given: "+value.type+".");
        }
        
    	return"return " + value.value;
    }
    
/* - If Statement - */

ifBlock = statement:ifStatement _ "endif" {return statement}
    
ifStatement = "if" __ condition:booleanValue __ "then" _ actions:multipleOptionalStatements _ elseStatements:((elseStatement / elseIfStatement)*) {
    	return "if (" + condition.value + "){" + actions + "}" + elseStatements;
     }
    
elseIfStatement =
	"else" _ ifStatement:ifStatement {return "else " + ifStatement;}
    
elseStatement =
	"else" _ "then" _ statements:multipleOptionalStatements  {return "else {" + statements + "}";}
    
/* - While Statement - */

whileStatement = 
	"while" __ condition:booleanValue __ "do" actions:multipleOptionalStatements _ "endwhile" {
    	return "while (" + condition.value + "){" + actions + "}"
    }
    
/* === Boolean Expressions === */
    
booleanSubValue =
	"(" _ val:booleanValue _ ")"
    {
    	return Variable("boolean", undefined, "(" + val.value + ")");
    }
    
booleanExpression =
	booleanSubValue / concatination / comparison

/* - Comparison - */

comparison =
	left:comparableValue _ operator:comparisonOperator _ right:comparableValue {
    	return Variable("boolean", undefined, left.value + operator + right.value);
    }

comparisonOperator = (isEqualOperator / isUnequalOperator / isGreaterThanOperator
/ isGreaterThanOrEqualOperator / isLessThanOperator / isLessThanOrEqualOperator)
    
isEqualOperator = 
	"=="
    	{return "==";}
    
isUnequalOperator = 
	"!="
    	{return "!=";}
    
isGreaterThanOperator = 
	">"
    	{return ">";}
    
isGreaterThanOrEqualOperator = 
	">="
    	{return ">=";}
    
isLessThanOperator = 
	"<"
   	 	{return "<";}
    
isLessThanOrEqualOperator = 
	"<="
    	{return "<=";}
       
/* - Concatination - */

concatinationOperator = andOperator / orOperator
andOperator = "and" {return "&&";}
orOperator = "or" {return "||";}

concatination =
	left:(Boolean / comparison / booleanSubValue) _ operator:concatinationOperator _ right:(Boolean / comparison / booleanSubValue) {
    	return Variable("boolean", undefined, left.value + operator + right.value);
    }
    
/* === Arithmetic === */

arithmetic = left:comparableValue _ operator:additionOperator _ right:comparableValue {
	var leftType = left.type, rightType = right.type, type = leftType;
    
    if (leftType === rightType) {
    	if (type) {
        }
    } else {
    	error('Both sides of the arithmetic operator have to be of equal type (Left: "' + leftType + '", Right: "' + rightType + '").');
    }
}

additionOperator = "+" {return "+";}
subtractionOperator = "-" {return "-";}
multiplicationOperator = "*" {return "*";}
divisionOperator = "/" {return "/";}
    
/* ==================================================== */
/* === 					  Types						=== */
/* ==================================================== */

Type =
	"integer" / "float" / "string" / "boolean" / "function" / "color" / "direction" / ArrayType / TileType / StreamType

ReturnType =
	Type / NOTHING
    
/* === Primitive Types === */

Integer "Integer"
	= [0-9]+ { return Variable("integer", undefined, parseInt(text())); }
    
Boolean =
    (TRUE / FALSE)
    
String "String"
	= '"' value:$([^"]*) '"'
    	{
        	return Variable("string", undefined, JSON.stringify(value));
        }

FALSE "False" =
	value:Text &{
    	return value.toLowerCase() === "false";
    } { return Variable("boolean", "FALSE", false); }
    
TRUE "True" =
	value:Text &{
    	return value.toLowerCase() === "true";
    } { return Variable("boolean", "TRUE", true); }
    
NOTHING "Nothing" =
	value:Text &{
    	return value.toLowerCase() === "nothing";
    } { return Variable("boolean", "NOTHING", undefined); }
    
/* === Extended Types === */
 
Function = functionCall / functionDefinition
Variable = variableAccess
Instantiation = ArrayConstructor
Color = colorAccess
Direction = directionAccess
AdditionalValue = getAdditionalValue
Tile = tileValue
Stream = streamValue

/* - Array - */

arrayFunctionCall = (arrayLength / arrayAdd)

ArrayType = "array" "[" _ Type _ "]" {return text();}
ArrayConstructor = "[" _ type:Type _ "]" {return Variable("array[" + type + "]", undefined, "[]");}

arrayAccess = arrayValue:arrayValue "[" index:Integer "]" {
	
}

arrayAdd = arrayValue:arrayValue "." "add" "(" value:value ")" {
	var arrayType = getArrayType(arrayValue.type);
    if (arrayType !== value.type) 
    	error('Expected type "' + arrayType + '" but "' + value.type + '" found.');
    else 
    	return Variable("nothing", undefined, arrayValue.value + ".push(" + value.value + ")");
}

arrayLength = arrayValue:arrayValue "." "length" "(" ")" {
	return Variable("integer", undefined, arrayValue.value + ".length");
}

/* - Color - */
    
colorAccess
	= colorObject objectValueAccessor value:(colorNone / colorAlpha / colorBeta) { return value; }
    
colorObject
	= "Color"
    
colorNone
	= "NONE" { return Variable("color", undefined, 0); }
    
colorAlpha
	= "ALPHA" { return Variable("color", undefined, 1); }
    
colorBeta
	= "BETA" { return Variable("color", undefined, 2); }
    
/* - Direction - */

directionAccess
	= directionObject objectValueAccessor value:(directionUp / directionRight / directionDown / directionLeft) { return value; }
    
directionObject = "Direction"
directionUp 	= "UP" 		{ return Variable("direction", undefined, 0); }
directionRight 	= "RIGHT" 	{ return Variable("direction", undefined, 1); }
directionDown 	= "DOWN" 	{ return Variable("direction", undefined, 2); }   
directionLeft 	= "LEFT" 	{ return Variable("direction", undefined, 3); }
   
/* ==================================================== */
/* === Internal grammar definitions for convenience === */
/* ==================================================== */

OptionalText
	= $[a-zA-Z]*

Text
	= $[a-zA-Z]+

_ "whitespace" 
	= $[ \t\n\r]*
    
__ "whitespace" 
	= $[ \t\n\r]+ 