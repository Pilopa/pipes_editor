/**
 * @module asset/js/model/tile
 */
define(["jquery", "model/direction.class", "model/position.class"], function ($, Direction, Position) {
	
	/* ****************** */
	/*    Constructor     */
	/* ****************** */
	
	/**
	 * Abstract class which all different types of tiles should be based up on.
	 * 
	 * @param {boolean} preplaced Defines whether the tile can be moved or rotated, immutable value
	 * @param {Position} position Initial position of the tile
	 * @param {Object.<Direction, Color>} baseOpenings Map containing all base openings for this tile
	 * @param {object} [additionalValues] If the tile type contains additional values, they are stored as is for later reference
	 * @param {Direction} [initialRotation] The initial rotation value for this tile
	 * 
	 * @constructor
	 * @alias module:asset/js/model/tile
	 * 
	 * @author Konstantin Schaper
	 */
	function Tile (preplaced, position, baseOpenings, additionalValues, initialRotation) {
		
		// Verify preplaced parameter
		if (preplaced !== undefined) {
			// Verify input type
			if (typeof(preplaced) !== "boolean")
				throw new Error("The parameter 'preplaced' has to be of type 'boolean'.");
		} else 
			throw new Error("The parameter 'preplaced' has to be provided. Given: " + preplaced);
		
		// Verify position parameter
		if (position !== undefined) {
			// Verify input type
			if (!(position instanceof Position))
				throw new Error("The parameter 'position' has to be of type 'Position'.");
		} else 
			throw new Error("The parameter 'position' has to be provided. Given: " + position);
		
		/* ****************** */
		/* 	 Private Members  */
		/* ****************** */
		
		/**
		 * @type {Direction}
		 */
		var rotation = (Direction.verify(initialRotation)) ? initialRotation : Tile.baseRotation;
		
		/**
		 * @type {Object.<Direction, Color>}
		 * @see #baseOpenings
		 */
		var openings = $.extend({}, baseOpenings);
		
		/* ****************** */
		/* 	 Private Methods  */
		/* ****************** */
		
		/**
		 * Internal method that overrides the rotation of the tile,
		 * no matter if it is rotatable. Use with caution.
		 * 
		 * @param {Direction} newRotation
		 */
		var setRotation = function (newRotation) {
			newRotation = parseInt(newRotation);
			
			// Verify direction parameter
			if (newRotation !== undefined) {
				// Verify input type
				if (!Direction.verify(newRotation))
					throw "The parameter 'newRotation' has to be of type 'Direction'.";
			} else 
				throw "The parameter 'newRotation' has to be provided. Given: " + newRotation;
			
			rotation = newRotation;
		};
		
		/* ****************** */
		/* Privileged methods */
		/* ****************** */
		
		/**
		 * @returns {object} Returns all tile type specific values.
		 */
		this.getAdditionalValues = function () {
			return additionalValues;
		};
		
		/**
		 * Method which returns a map of base openings.
		 * The key of the map corresponds to the opening's direction.
		 * 
		 * @returns {Object.<Direction, Color>} 
		 */
		this.getBaseOpenings = function () {
			return baseOpenings;
		};
	
		/**
		 * Method which returns a map of the tile's openings.
		 * The key of the map corresponds to the opening's direction.
		 * 
		 * @returns {Object.<Direction, Color>} 
		 */
		this.getOpenings = function () {
			return openings;
		};
		
		/**
		 * @returns {Object.<Direction, Color>} A copied version of the openings, taking rotations into account
		 */
		this.getRotatedOpenings = function () {
			var rotatedOpenings = {};
			
			for (var direction in this.getOpenings()) {
				rotatedOpenings[this.getRotatedDirection(direction)] = parseInt(this.getOpenings()[direction]);
			};
			
			return rotatedOpenings;
		};
			
		/**
		 * Returns the tile's opening in the given direction, taking the tile's rotation into account.
		 * 
		 * @param {Direction} direction
		 * @returns {Color}
		 */
		this.getRotatedOpening = function (direction) {
			// Verify direction parameter
			if (direction !== undefined) {
				// Verify input type
				if (!Direction.verify(direction))
					throw "The parameter 'direction' has to be of type 'Direction'.";
			} else 
				throw "The parameter 'direction' has to be provided. Given: " + direction;
			return parseInt(this.getOpenings()[this.getBaseDirection(direction)]);
		};
		
		/**
		 * Returns the tile's opening in the given direction.
		 * 
		 * @param {Direction} direction
		 * @returns {Color}
		 */
		this.getOpening = function (direction) {
			// Verify direction parameter
			if (direction !== undefined) {
				// Verify input type
				if (!Direction.verify(direction))
					throw "The parameter 'direction' has to be of type 'Direction'.";
			} else 
				throw "The parameter 'direction' has to be provided. Given: " + direction;
			
			return parseInt(this.getOpenings()[direction]);
		};
		
		/**
		 * Transforms the given direction into a rotated direction by adding the tile's rotation.
		 * 
		 * @param {Direction} direction
		 * @returns {Direction}
		 */
		this.getRotatedDirection = function (direction) {
			return Direction.rotated(direction, this.getRotation());
		};
		
		/**
		 * Transforms the given direction into a base direction by subtracting the tile's rotation.
		 * 
		 * @param {Direction} direction
		 * @returns {Direction}
		 */
		this.getBaseDirection = function (direction) {
			return Direction.rotated(direction, -this.getRotation());
		};
		
		/**
		 * Rotates the tile clock-wise by one step, if it 'isRotatable'.
		 */
		this.rotate = function () {
			if (this.isRotatable())
				setRotation.call(this, Direction.rotated(this.getRotation(), 1));
		};
		
		/**
		 * @returns {boolean} Whether the tile was preplaced.
		 */
		this.isPreplaced = function () {
			return preplaced;
		};
		
		/**
		 * @returns {Position} The current position of this tile
		 */
		this.getPosition = function () {
			return position;
		};
		
		/**
		 * Changes the color the opening in the given direction is filled with, taking the tile's rotation into account.
		 * 
		 * @param {Direction} direction
		 * @param {Color} fillColor
		 */
		this.fillOpening = function (direction, fillColor) {
			direction = parseInt(direction);
			fillColor = parseInt(fillColor);
			this.getOpenings()[this.getBaseDirection(direction)] = fillColor;
		};
		
		/**
		 * Overrides the current position of this tile.
		 * 
		 * @param {Position} position The new value
		 */
		this.setPosition = function (newPosition) {
			// Verify position parameter
			if (newPosition !== undefined) {
				// Verify input type
				if (!(newPosition instanceof Position))
					throw "The parameter 'newPosition' has to be of type 'Position'.";
				else
					position = newPosition;
			} else 
				throw "The parameter 'newPosition' has to be provided. Given: " + newPosition;
		};
		
		/**
		 * @returns {Direction} The current facing direction/rotation
		 */
		this.getRotation = function() {
			return rotation;
		};
		
		/**
		 * Empties all openings of this tile,
		 * effectively resetting them to their base value.
		 */
		this.reset = function () {
			openings = $.extend({}, this.getBaseOpenings());
		};
		
	};
	
	/* ****************** */
	/*  Static Variables  */
	/* ****************** */
	
	/**
	 * All tiles start facing up
	 */
	Tile.baseRotation = Direction.UP;
	
	/**
	 * Each inheriting class may want to define this static variable,
	 * defining any additional values that need to be provided by the
	 * user upon placing this tile. The type is an object containing
	 * name-type pairs. If a sub-class utilizes this feature, it needs
	 * to expected an object of same layout to be passed to its constructor 
	 * after the two main arguments (See Tile.prototype.constructor).
	 * Possible type layouts are:<br>
	 * <code><br>
	 * #name#: {<br>
	 * 		type: "Number",<br>
	 * 		min: #number#,<br>
	 * 		max: #number#,<br>
	 * 		default: #number#<br>
	 * }<br>
	 * <br>
	 * #name#: {<br>
	 * 		type: "Color"<br>
	 * }<br>
	 * <br>
	 * #name#: {<br>
	 * 		type: "Direction"<br>
	 * }<br>
	 * </code><br>
	 */
	Tile.additionalValues = {};
	
	/* ****************** */
	/*   Public Methods   */
	/* ****************** */
	
	/**
	 * Returns the part of this object to be serialized via JSON.stringify.
	 * 
	 * @returns {Object}
	 */
	Tile.prototype.toJSON = function() {
		return {
			tileTypeId: this.getTileTypeId(),
			rotation: this.getRotation(),
			additionalValues: this.getAdditionalValues()
		};
	};
	
	/**
	 * @returns {Boolean} Whether the 'this' variable is actually a tile.
	 */
	Tile.prototype.isStaticContext = function () {
		return !(this instanceof Tile);
	};
	
	/* ********************* */
	/*  Overridable Methods  */
	/* ********************* */
	
	/**
	 * @returns {boolean} Whether the tile is rotatable.
	 */
	Tile.prototype.isRotatable = function () {
		return !this.isPreplaced();
	};
	
	/**
	 * @returns {boolean} Whether the tile is flagged as movable.
	 */
	Tile.prototype.isMovable = function () {
		return !this.isPreplaced();
	};
	
	/**
	 * The point value of a tile is used to determine the quality of a user-provided
	 * solution. The higher the value, the more powerful the tile is.
	 * Ultimately, only the proportion of the values for the tile types matter.
	 * You should therefore look for other implemented tile types and define this value accordingly.
	 * Defaults to return zero. 
	 * 
	 * <i>Warning:</i> This method might be used in a static context, so the 'this' variable
	 * is not guaranteed to be present.
	 * 
	 * @returns {number} The point value of this type of tile.
	 */
	Tile.prototype.getPointValue = function () {
		return 0;
	};
	
	/**
	 * Gets all exits for the given entry direction.
	 * 
	 * @param {Direction} entryDirection
	 * @returns {Direction[]} The exit directions for this entry
	 * @see Tile.prototype.getOpenings
	 */
	Tile.prototype.getExits = function(entryDirection) {
		var exits = [];
		var baseEntryDirection = this.getBaseDirection(entryDirection);
		
		for (var direction in this.getBaseOpenings()) {
			direction = parseInt(direction);
			if (direction !== baseEntryDirection)
				exits.push(this.getRotatedDirection(direction));
		};
		
		return exits;
	};
	
	/**
	 * Each opening of each source creates a stream of liquid in that openings color
	 * which is going to be verified in the associated verification stage.
	 * 
	 * @returns {Boolean} Whether this tile is a source tile or not.
	 */
	Tile.prototype.isSource = function () {
		return false;
	};
	
	/**
	 * @returns {Boolean} Whether this tile is a destination tile or not.
	 */
	Tile.prototype.isDestination = function () {
		return false;
	};
	
	/**
	 * Should be overriden by sub classes to return the url to the 
	 * image which represents this tile. <br>Returns the type id of the tile
	 * by default.
	 * 
	 * @returns {string}
	 */
	Tile.prototype.getBaseImageURL = function () {
		return this.getTileTypeId();
	};
	
	/**
	 * Event-handler method that is executed on the evaluated tile at the start of the evaluation process.<br>
	 * Defaults to do nothing.
	 * 
	 * @param {Stream} stream The stream containing the color and direction data, can be manipulated
	 * @returns {boolean} Whether the event was handled without an error
	 */
	Tile.prototype.preEntry = function (stream) {
		// Do nothing
	};
	
	/**
	 * Event-handler method that is executed on the evaluated tile after the entry has been evaluated.<br>
	 * Defaults to do nothing.
	 * 
	 * @param {Stream} stream The stream containing the color and direction data, can be manipulated
	 * @returns {boolean} Whether the event was handled without an error
	 */
	Tile.prototype.postEntry = function (stream) {
		// Do nothing
	};
	
	/**
	 * Event-handler method that is executed on each exit (opening that is not the entry) of the evaluated tile,
	 * before the stream is updated for the next tile. Only the color of the stream can be manipulated, as the other values 
	 * are overridden immediately after execution.<br>
	 * Defaults to do nothing.
	 * 
	 * @param {Direction} exit The direction in which the stream is exiting
	 * @param {Stream} stream The stream containing the color and direction data
	 * @returns {boolean} Whether the event was handled without an error
	 */
	Tile.prototype.preExit = function (exit, stream) {
		// Do nothing
	};
	
	/**
	 * Event-handler method that is executed on each exit (opening that is not the entry) of the evaluated tile,
	 * after the stream (cursor) has been moved to the next tile.<br>
	 * Defaults to do nothing.
	 * 
	 * @param {Direction} exit The direction in which the stream is exiting
	 * @param {Stream} stream The stream containing the color and direction data, can be manipulated
	 * @returns {boolean} Whether the event was handled without an error
	 */
	Tile.prototype.postExit = function (exit, stream) {
		// Do nothing
	};
	
	/* ****************** */
	/*  Abstract Methods  */
	/* ****************** */
	
	/**
	 * Abstract method that should be overridden by sub classes to return a unique identifier (e.g. the class name).
	 * 
	 * @returns {string}
	 */
	Tile.prototype.getTileTypeId = function () {
		throw new Error("Abstract method 'getTileTypeId' is not implemented");
	};
	
	/**
	 * Abstract method that should be overridden by sub classes to return the TTD Script code that was used to generate this tile type.
	 * 
	 * <i>Warning:</i> This method might be used in a static context, so the 'this' variable
	 * is not guaranteed to be present.
	 * 
	 * @returns {string} The TileTypeDefinition Script code
	 */
	Tile.prototype.getTileTypeDefinitionCode = function () {
		throw new Error("Abstract method 'getTileTypeDefinitionCode' is not implemented");
	};

	// Return class definition to dependency container
	return Tile;
	
});