/**
 * Constructor for the Tjunction class, which inherits the Tile class.
 * 
 * @param {boolean} preplaced Defines whether the tile can be moved or rotated, immutable value
 * @param {Position} position Initial position of the tile
 * @param {Direction} [initialRotation] The initial rotation value for this tile
 * @author Konstantin Schaper
 * @deprecated Replaced by '.ttd' files
 */
function Tjunction (preplaced, position, initialRotation) {
	// Define base openings
	var baseOpenings = {};
	baseOpenings[Direction.UP] = Color.NONE;
	baseOpenings[Direction.LEFT] = Color.NONE;
	baseOpenings[Direction.DOWN] = Color.NONE;
	
	// Call super constructor
	Tjunction.prototype.parent.constructor.call(this, preplaced, position, baseOpenings, undefined, initialRotation);

};

/* ************************ */
/*  Inheritance Definition  */
/* ************************ */

Tjunction.prototype = Object.create(Tile.prototype);
Tjunction.prototype.constructor = Tjunction;
Tjunction.prototype.parent = Tile.prototype;

Tjunction.prototype.getTileTypeId = function () {
	return "Tjunction";
};

/**
 * Tjunctions are the only way to split or unite streams and are therefore very powerful.
 */
Tjunction.prototype.getPointValue = function () {
	return 30;
};