/**
 * Constructor for the Straight class, which inherits the Tile class.
 * 
 * @param {boolean} preplaced Defines whether the tile can be moved or rotated, immutable value
 * @param {Position} position Initial position of the tile
 * @param {Direction} [initialRotation] The initial rotation value for this tile
 * @author Konstantin Schaper
 * @deprecated Replaced by '.ttd' files
 */
function Straight (preplaced, position, initialRotation) {
	// Define base openings
	var baseOpenings = {};
	baseOpenings[Direction.LEFT] = Color.NONE;
	baseOpenings[Direction.RIGHT] = Color.NONE;
	
	// Call super constructor
	Straight.prototype.parent.constructor.call(this, preplaced, position, baseOpenings, undefined, initialRotation);

};

/* ************************ */
/*  Inheritance Definition  */
/* ************************ */

Straight.prototype = Object.create(Tile.prototype);
Straight.prototype.constructor = Straight;
Straight.prototype.parent = Tile.prototype;

Straight.prototype.getTileTypeId = function () {
	return "Straight";
};

/**
 * Straights are one of the two core tiles and therefore not worth that much.
 */
Straight.prototype.getPointValue = function () {
	return 10;
};