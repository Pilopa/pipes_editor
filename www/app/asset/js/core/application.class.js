/**
 * @module asset/js/core/application
 */
define(["jquery", "core/notification_manager", "promise", "pegjs", "loopprotect", "jszip", "core/application_state.class", "core/event_manager.class", "core/progress_overlay", "model/level.class", "model/playfield.class", "model/tile.class",
        "core/component.class", "component/helpSlidesDialog", "component/helpOverviewDialog", "component/tileTypeEditorDialog", "component/tileTypeOverviewDialog",
         "component/assetEditorDialog", "component/additionalInputDialog", "component/playfield",  "component/optionsDialog" ,"component/navigation"], 
		function ($, NotificationManager, Promise, peg, loopProtect, JSZip,
				ApplicationState, EventManager, ProgressOverlay,
				Level, Playfield, Tile, Component,
				HelpSlidesDialogComponent, HelpOverviewDialogComponent, 
				TileTypeEditorDialogComponent,  TileTypeOverviewDialogComponent,
				AssetEditorDialogComponent, AdditionalInputDialogComponent,
				PlayfieldComponent, OptionsDialogComponent, NavigationComponent) {

	/**
	 * The core class which does all initial setup and which performs dependency injection.
	 * Takes care of filling in the view parts and maintains an event system for different parts of the 
	 * application to communicate with each other.
	 * 
	 * @constructor
	 * @alias module:asset/js/core/application
	 * @author Konstantin Schaper
	 */
	function Application(progressDisplay) {
		
		/* ****************** */
		/* 	 Private Members  */
		/* ****************** */

		/**
		 * The current state the application is in.
		 * Has impact on the functionality of the user interface.
		 * 
		 * @type {ApplicationState}
		 */
		var appState = ApplicationState.NONE;
		
		/**
		 * Contains all tile type classes for default tile types.
		 * 
		 * @type {object}
		 */
		var defaultTileTypes = {};
		
		/* ****************** */
		/* Define Event Types */
		/* ****************** */
		
		var eventTypes = [];
		
		/**
		 * @event Application#onStateChange
		 * @type {object}
		 * @property {ApplicationState} newState
		 */
		eventTypes.push("onStateChange");
		
		/**
		 * @event Application#onTilePlaced
		 * @type {object}
		 * @property {Tile} tilePlaced
		 */
		eventTypes.push("onTilePlaced");

		/**
		 * @event Application#onTileRemoved
		 * @type {object}
		 * @property {Tile} tileRemoved
		 */
		eventTypes.push("onTileRemoved");
		
		/**
		 * @event Application#onLevelLoaded
		 * @type {object}
		 * @property {Error|undefined} error
		 * @property {Level|undefined} levelLoaded
		 */
		eventTypes.push("onLevelLoaded");
		
		/**
		 * @event Application#onSavableLevelBlobCreated
		 * @type {object}
		 * @property {Error|undefined} error, {blob|undefined} blob
		 * @property {string} proposedFileName
		 */
		eventTypes.push("onSavableLevelBlobCreated");
		
		/**
		 * @event Application#onTileTypeAdded
		 * @type {object}
		 * @property {function} tileTypeClass
		 */
		eventTypes.push("onTileTypeAdded");
		
		/**
		 * @event Application#onTileTypeRemoved
		 * @type {object}
		 * @property {string} tileTypeId
		 */
		eventTypes.push("onTileTypeRemoved");
		
		/**
		 * @event Application#onTileTypeUpdated
		 * @type {object}
		 * @property {function} tileTypeClass
		 */
		eventTypes.push("onTileTypeUpdated");
		
		/**
		 * @event Application#onAssetAdded
		 * @type {object}
		 * @property {string} assetName
		 * @property {string} assetBlobUrl
		 */
		eventTypes.push("onAssetAdded");
		
		/**
		 * @event Application#onAssetRemoved
		 * @type {object}
		 * @property {string} assetName
		 */
		eventTypes.push("onAssetRemoved");
		
		/**
		 * @event Application#onAssetUpdated
		 * @type {object}
		 * @property {string} oldAssetName
		 * @property {string} newAssetName
		 * @property {string} assetBlobUrl
		 */
		eventTypes.push("onAssetUpdated");
		
		/**
		 * Responsible for taking care of the event system.
		 * 
		 * @type {EventManager}
		 */
		var eventManager = new EventManager(eventTypes);
		
		/* ****************** */
		/*   Public Members   */
		/* ****************** */
		
		/**
		 * @type {Level}
		 */
		this.activeLevel = undefined;
		
		/**
		 * Backup of the level during play mode.
		 * 
		 * @type {Level}
		 */
		this.editorLevel = undefined;
		
		/**
		 * PegJS parser to parse .ttd files into JavaScript code.
		 * 
		 * @type {Peg}
		 */
		this.parser = undefined;
		
		/* ****************** */
		/*  Private methods   */
		/* ****************** */
		
		/**
		 * Internal method that takes care of the immediate results of app state changes.
		 * 
		 * @param {ApplicationState} newAppState The new application state, for convenience
		 */
		var onAppStateChange = function (newAppState) {
			if (newAppState === ApplicationState.EDITOR && this.editorLevel instanceof Level) {
				this.activeLevel = this.editorLevel;
				delete this.editorLevel;
			} else if (newAppState === ApplicationState.PLAY && this.activeLevel instanceof Level) {
				this.editorLevel = this.activeLevel;
				this.activeLevel = this.activeLevel.createPlayModeClone();
			}
		};
		
		/* ****************** */
		/* Privileged methods */
		/* ****************** */
		
		/**
		 * Delegated convenience method for backwards compatibility.
		 * @see EventManager#registerListener
		 */
		this.registerListener = function (event, listener) {
			eventManager.registerListener(event, listener);
		};
		
		/**
		 * Delegated convenience method for backwards compatibility.
		 * @see EventManager#deregisterListener
		 */
		this.deregisterListener = function (event, listener) {
			eventManager.registerListener(event, listener);
		};
		
		/**
		 * Delegated convenience method for backwards compatibility.
		 * @see EventManager#fireEvent
		 */
		this.fireEvent = function (eventType, args) {
			eventManager.fireEvent(eventType, args);
		};
		
		/**
		 * @returns The current ApplicationState
		 * @see {ApplicationState}
		 */
		this.getAppState = function () {
			return appState;
		};
		
		/**
		 * Changes the state this application is in, firing a state change event.
		 * 
		 * @param {ApplicationState} newAppState The state to change to
		 * @throws {Error} When the input value is of incorrect type
		 */
		this.setAppState = function (newAppState) {
			if (!ApplicationState.verify(newAppState)) {
				throw new Error("Parameter 'newAppState' has to be of type 'ApplicationState', provided: " + typeof(newAppState));
			}
			
			appState = newAppState;
			onAppStateChange.call(this, newAppState);
			this.fireEvent("onStateChange", [newAppState]);
		};
		
		/**
		 * Attempts to find the Tile class for the given tile type id in both
		 * this application context as well as the application's active level.
		 * 
		 * @param {string} tileTypeId The tile type id to get the Tile class for
		 * @returns {function|undefined} The tile type class for the given id or undefined, if it does not exist 
		 */
		this.getTileType = function (tileTypeId) {
			if (this.isDefaultTileType(tileTypeId)) {
				return this.getDefaultTileType(tileTypeId);
			} else if (this.activeLevel) {
				return this.activeLevel.getTileTypes()[tileTypeId];
			} else 
				return undefined;
		};
		
		/**
		 * @param {Tile} tile The tile to get the image url for
		 * @param {Level} level The level to load non-default tile images from
		 * @returns {string} The css url string to the image
		 */
		this.getTileImageUrl = function (tile, level) {
			var tileTypeId = tile.getTileTypeId();
			var imageName;
			if (this.isDefaultTileType(tileTypeId)) {
				imageName = this.config["default_tile_types_image_location_url"] + tile.getBaseImageURL() + "." + this.config["tile_types_image_type"];
				return "url(" + imageName + ")";
			} else {
				imageName = level.getTileTypesImageLocationURL() + tile.getBaseImageURL() + "." + this.config["tile_types_image_type"];
				var base64ImageString = level.getCachedTileTypeImage(imageName);
				if (base64ImageString)
					return "url(data:image/png;base64," + base64ImageString + ")";
				else
					return "url(app/asset/img/undefined.png)";
			}
		};
		
		/**
		 * @param {string} tileTypeId The tileTypeId to get the image url for
		 * @param {Level} level The level to load non-default tile images from
		 * @returns {string} The css url string to the image
		 */
		this.getTileTypeImageUrl = function (tileTypeId, level) {
			var imageName;
			if (this.isDefaultTileType(tileTypeId)) {
				imageName = this.config["default_tile_types_image_location_url"] + tileTypeId + "." + this.config["tile_types_image_type"];
				return "url(" + imageName + ")";
			} else {
				imageName = level.getTileTypesImageLocationURL() + tileTypeId + "." + this.config["tile_types_image_type"];
				var base64ImageString = level.getCachedTileTypeImage(imageName);
				if (base64ImageString)
					return "url(data:image/png;base64," + base64ImageString + ")";
				else
					return "url(app/asset/img/undefined.png)";
			}
		};
		
		/**
		 * Registers the given default Tile class with this application context.
		 * 
		 * @param {Function} tileTypeClass The Tile sub-class to register
		 * @throws {Error} If the given tile type class is not default
		 * @fires Application#onTileTypeAdded
		 * @fires Application#onTileTypeUpdated
		 */
		this.addDefaultTileType = function (tileTypeClass, error) {
			if (error) throw error;
			if (!this.isDefaultTileType(tileTypeClass)) throw new Error("Non-default tile type class cannot be added as default.");
			var existed = typeof defaultTileTypes[tileTypeClass.name] !== "undefined";
			defaultTileTypes[tileTypeClass.name.toLowerCase()] = tileTypeClass;
			if (!existed) this.fireEvent("onTileTypeAdded", [tileTypeClass]);
			else this.fireEvent("onTileTypeUpdated", [tileTypeClass]);
		};
		
		/**
		 * Loads the Tile class for the given default tile type id.<br>
		 * This only loads default Tile classes registered in this application context.
		 * 
		 * @param {string} tileTypeId The default tile type id to load the Tile class for
		 * @returns {function|undefined} The Tile class or undefined, if it does not exist
		 */
		this.getDefaultTileType = function (tileTypeId) {
			return defaultTileTypes[tileTypeId];
		};
		
		/**
		 * The map has the following layout:
		 * {string} tileTypeId => {function} tileClass
		 * 
		 * @returns {object<string,function>} A map containing all default Tile classes
		 */
		this.getDefaultTileTypes = function () {
			return defaultTileTypes;
		};
		
		/**
		 * Combines the given levels tile types with the default tile types defined
		 * by this application context and returns the result. If no level is given,
		 * the application's active level is used.
		 * 
		 * @param {Level} [level] Optional level which's tile types to add
		 * @returns {object<string,function>} A map of the layout: {string} tileTypeId => {function} tileClass.
		 */
		this.getTileTypes = function (level) {
			return Object.assign({}, defaultTileTypes, typeof level === "undefined" ? this.activeLevel.getTileTypes() : level.getTileTypes());
		};
		
	}

	/* ****************** */
	/*   Public Methods   */
	/* ****************** */
	
	/**
	 * Asynchronously generates a blob out of the given level (or the active level, if no level is provided).
	 * Although the promise of this process is returned, the preferable way of interacting with the result is through
	 * the application event "onSavableLevelBlobCreated", which is fired on either success or failure.
	 * 
	 * @param {level} [level] Optional level to create a persistable blob for, takes the application's active level if undefined
	 * @returns {Promise} A promise for the generated blob
	 */
	Application.prototype.saveLevel = function (level) {
		// Check for input argument
		if (typeof level === "undefined") level = this.activeLevel;
		
		// Remember "this" variable
		var that = this;
		
		// Create Zip file
		var zipFileName = level.getTitle();
		var zip = new JSZip();
		zip.file("level.json", JSON.stringify(level, null, "\t"));
		
		// Create Subfolder for Tile types
		zip.folder(level.getTileTypesLocationURL());
		
		// Fill in non-default tile types
		var nonDefaultTileTypeIds = level.getNonDefaultTileTypeIds(this);
		for (var index = 0; index < nonDefaultTileTypeIds.length; index++) {
			var tileTypeId = nonDefaultTileTypeIds[index];
			var tileType = this.getTileType(tileTypeId);
			var tileTypeCode = tileType.prototype.getTileTypeDefinitionCode();
			zip.file(level.getTileTypesLocationURL() + tileTypeId + ".ttd", tileTypeCode);
		};
		
		// Create Subfolder for Tile images
		zip.folder(level.getTileTypesImageLocationURL());
		var imageCache = level.getTileTypeImageCache();
		for (var fileName in imageCache) {
			var base64ImageString = imageCache[fileName];
			zip.file(fileName, base64ImageString, {base64: true});
		};
		
		// Generate the blob from the in-memory zip file and return the Promise
		return zip.generateAsync({type:"blob"}).then(
			// Success
			function (blob){
				that.fireEvent("onSavableLevelBlobCreated", [undefined, blob, zipFileName]);
			},
			// Failure
			function (error) {
				that.fireEvent("onSavableLevelBlobCreated", [error, undefined, zipFileName]);
			}
		);
	};

	/**
	 * Attempts to generate a {Level} object from the given ArrayBuffer data.
	 * Fires an onLevelLoaded event on either success or failure.
	 * 
	 * @param {ArrayBuffer} The data from which to construct a level
	 * @returns nothing
	 */
	Application.prototype.loadLevel = function(data) {
		
		// Remember "this" variable
		var that = this;

		try {
			
			JSZip.loadAsync(data).then(function (zip) {
				zip.file("level.json").async("string").then(function (jsonString) {
					var levelLoadResult = Level.fromJSON(this, jsonString);
					var loadedLevel = levelLoadResult.level;

						// Create promises array
						var promises = [];
						
						// Load non-default tile types
						var nonDefaultTileTypeIds = loadedLevel.getNonDefaultTileTypeIds(that);
						for (var index = 0; index < nonDefaultTileTypeIds.length; index++) {
							var tileTypeId = nonDefaultTileTypeIds[index];
							
							// Asynchronously load all non-default tile types from the zip file
							var tileTypeDefinitionFile = zip.file(loadedLevel.getTileTypesLocationURL() + tileTypeId + ".ttd");
							var promise = tileTypeDefinitionFile.async("string").then(
							function (tileTypeDefinitionCode) {
								// Create the tile type from the code and store it in the level
								that.createTileType(tileTypeDefinitionCode, (loadedLevel.addTileType).bind(loadedLevel));
							});
							
							// Add the asynchronous promise to the list of promises to wait on
							promises.push(promise);
						}
						
						// Asynchronously load all images for non-default tile types from the zip file
						var imageFileType = that.config["tile_types_image_type"];
						
						// - Recursively filter all files under the image folder for valid image files
						var imageFiles = zip.folder(loadedLevel.getTileTypesImageLocationURL()).filter(
							function (path, file) {
								return path.endsWith(imageFileType);
							}
						);
						
						// - Iterate over the images found and load them into the level cache
						for (var index = 0; index < imageFiles.length; index++) {
							var imageFile = imageFiles[index];
							
							// Asynchrounsly load the image from the zip file
							var promise = imageFile.async("base64").then(
							function (imageString) {
								// Create the tile type from the code and store it in the level
								loadedLevel.cacheTileTypeImage(imageFile.name, imageString);
							});
							
							// Add the asynchronous promise to the list of promises to wait on
							promises.push(promise);
						}
							
						// Wait for all data load to be done
						Promise.all(promises)
						
						// All necessary files within zip are loaded
						.then((function () {
							
							// Load playfield
							var playfield = Playfield.fromJSON(that.getTileTypes(loadedLevel), levelLoadResult.playfield);
							loadedLevel.setPlayfield(playfield);
							
							// Assign the loaded level
							that.activeLevel = loadedLevel;
							
							// Fire successful onLevelLoaded event
							that.fireEvent("onLevelLoaded", [undefined, loadedLevel]);
							
							console.info("Load > level loaded ");
							
						}).bind(this), 
						
						// One of the promises failed
						function (exception) {
							console.error("Level file could not be unpacked:");
							console.dir(exception);
							that.fireEvent("onLevelLoaded", [exception, undefined]);
						});

				},
				// level.json could not be loaded
				function (exception) {
					console.error("The description file 'level.json' could not be read:");
					console.dir(exception);
					that.fireEvent("onLevelLoaded", [exception, undefined]);
				});

			},
			
			// ZIP could not be created from file
			function (exception) {
				console.error("Level file malformed:");
				console.dir(exception);
				that.fireEvent("onLevelLoaded", [exception, undefined]);
			}
			
			);
			
		} catch (exception) {
			// A general error occurred
			console.error("An error occurred while loading the level: ");
			console.dir(exception);
			that.fireEvent("onLevelLoaded", [exception, undefined]);
		}
	};

	/**
	 * Defines all application wide events.
	 * Multiple calls to this method should be avoided and can
	 * cause unwanted behavior. 
	 */
	Application.prototype.bindEvents = function() {
		// Create custom resize event which can be used by multiple handlers, instead of just one (which is the default functionality)
		$(window).resize(function () {
	       $(window).trigger("window::resize");
	    });
		
		// Enable file drag & drop
		$.event.props.push('dataTransfer');
	};

	/**
	 * Asynchronously loads the script at the given url.
	 * 
	 * @param {String} url The url where to load the script from
	 * @returns {Deferred} The deferred object representing this ajax request
	 * 
	 * @see https://api.jquery.com/jquery.getscript/
	 */
	Application.prototype.loadScript = function(url) {
		return $.getScript(url);
	};

	/**
	 * Asynchronously loads all requested scripts in order they are provided, calls the successCallback
	 * on finish or the failCallback as soon as one script fails to load.
	 * 
	 * @param {string[]} scripts
	 * @param {function} successCallback The handler to be called after all scripts have been successfully loaded
	 * @param {function} failCallback The handler to be called once a script failed to be loaded
	 * @see http://stackoverflow.com/questions/9711160/jquery-load-scripts-in-order
	 * @see https://api.jquery.com/jquery.getscript/
	 */
	Application.prototype.loadScripts = function(scripts, successCallback, failCallback) {
		var index = 0;
		var that = this;
		this.loadScript(scripts[index])
		.done(function(script, textStatus) {
			if (++index < scripts.length) {
				that.loadScript(scripts[index]) // Get deferred script loading object
				.done(arguments.callee) // Recursive call to anonymous function
				.fail(failCallback); // Call provided error handler
			} else {
				successCallback(); // At the end of the recursive loop, call provided success handler
			}
		})
		.fail(failCallback); // Call provided error handler
	};
	
	/**
	 * Registers the given Tile class with the given level in this application context.
	 * 
	 * @param {function} tileTypeClass The Tile sub-class to register
	 * @param {Level} [level=activeLevel] The level to add the tile type to
	 * @fires Application#onTileTypeAdded
	 * @fires Application#onTileTypeUpdated
	 */
	Application.prototype.addTileType = function (tileTypeClass, level) {
		if (!level) level = this.activeLevel;
		
		var existed = typeof level.getTileTypes()[tileTypeClass.name] !== "undefined";
		level.addTileType(tileTypeClass);
		
		if (existed) 
			this.fireEvent("onTileTypeUpdated", [tileTypeClass]);
		else {
			level.setBaseToolAvailabilityCount(tileTypeClass.name, 0);
			this.fireEvent("onTileTypeAdded", [tileTypeClass]);
		}
	};
	
	/**
	 * De-registers the Tile class associated with the given tile type id from the active level in this application context.
	 * 
	 * @param {string} tileTypeId The tile type to delete
	 * @fires Application#onTileTypeRemoved
	 */
	Application.prototype.removeTileType = function (tileTypeId) {
		this.activeLevel.removeTileType(tileTypeId);
		this.fireEvent("onTileTypeRemoved", [tileTypeId]);
	};

	/**
	 * @param {function|string} tileType
	 * @returns {boolean} Whether the given tileType is configured to be default.
	 */
	Application.prototype.isDefaultTileType = function (tileType) {
		if (typeof tileType === "function")
			return typeof this.config['default_tile_types'][tileType.name] !== "undefined";
		else if (typeof tileType === "string")
			return typeof this.config['default_tile_types'][tileType] !== "undefined";
		else 
			throw "Parameter 'tileType' has to be of type 'function' or 'string', '" + typeof(tileType) + "' given.";
	};

	/**
	 * @param {string[]} tileTypeIds An array containing the ids of all Tile class types to load.
	 * @param {string} path The path from where to load the tileTypeIds
	 * @param {function} [callback] Optional function being executed when the Tile class for the given tile type has been successfully loaded,
	 * the created Tile class is provided as the first and only parameter.
	 * @returns {Deferred[]} An array of deferred objects which are loading the tile types asynchronously
	 */
	Application.prototype.loadTileTypes = function (tileTypeIds, path, callback) {
		// Construct script urls array
		var scripts = [];
		tileTypeIds.forEach(function (tileTypeId, index, array) {
			scripts.push($.get(path + tileTypeId + ".ttd", (function (codeString) {
				this.createTileType(codeString, callback);
			}).bind(this), "text"));
		}, this);
		
		return scripts;
	};
	
	/**
	 * Handles the result of a tile type class creation process.
	 * 
	 * @callback Application~createTileTypeCallback
	 * @param {function|undefined} tileTypeClass The freshly created tile type class constructor, undefined if an error occurred
	 * @param {error|undefined} error The error which prevented the tile type class from being created, undefined on success
	 */

	/**
	 * Attempts to load the given tile type from the given code string.<br>
	 * The callback parameter is a function that takes the generated Tile class as a single argument.
	 * 
	 * @param {string} tileTypeCode The code from which to generate the tile type class.
	 * @param {Application~createTileTypeCallback} callback The callback to call once the code has been evaluated and the class created
	 */
	Application.prototype.createTileType = function (ttdCode, callback) {
		try {
			
			// Throw error when parser not present
			if (!this.parser) callback(undefined, new Error("Parser not initialized within this application context"));
			
			// Parse .ttd into JavaScript
			var javaScriptCode = this.parser.parse(ttdCode);
			
			// Protect against simple infinite loops
			var loopProtectedCode = loopProtect(javaScriptCode);
			
			// Interpret Code in local scope
			// # This could be improved by executing the code in a different mod
			var tileTypeClass = eval("(function(){" + loopProtectedCode + "}())");
			
			// Execute callback if present
			if (typeof callback === "function") 
				callback(tileTypeClass, undefined);
		
		} catch (exception) {
			callback(undefined, exception);
		}
	};

	/**
	 * Loads all scripts of all configured default tile types.
	 * 
	 * @returns {Deferred[]} An array of deferred objects which are loading the tile types asynchronously
	 */
	Application.prototype.loadDefaultTileTypes = function () {
		return this.loadTileTypes(this.getDefaultTileTypeIds(), this.config['default_tile_types_location_url'], (this.addDefaultTileType).bind(this));
	};

	/**
	 * @returns {String[]} An array containing all configured IDs of default tile types.
	 */
	Application.prototype.getDefaultTileTypeIds = function () {
		return Object.keys(this.config['default_tile_types']);
	};

	/**
	 * Initializes the application by loading all necessary scripts,
	 * the application configuration, injecting all view components
	 * and finally defining the initial application state.
	 * 
	 * @param {string} configUrl The url on the server to retrieve the configuration file from
	 * @param {string} parserGrammarUrl The url on the server to retrieve the parser grammar file from
	 */
	Application.prototype.start = function (configUrl, parserGrammarUrl) {
		// Dont start the application if it is already running
		if (this.getAppState() !== ApplicationState.NONE) return;
		
		console.info("Attempting to load application configuration ...");
			
		// Load config
		$.getJSON(configUrl).done((function (configData) {
			
			console.info("Attempting to load application parser grammar ...");
			
			// Load parser grammar
			$.get(parserGrammarUrl, (function (parserGrammar) {
				
				console.info("Attempting to create application parser ...");
				
				// Create parser
				this.parser = peg.generate(parserGrammar);
				
				console.info("Attempting to apply application default configuration ...");
				
				// Perform standard configuration
				this.config = configData;
				this.defaultPlayfield = new Playfield(this.config['initial_level_dimensions']['rows'],
						this.config['initial_level_dimensions']['cols']);
				this.activeLevel = new Level(this.config['initial_level_name'], this.defaultPlayfield, this.config['default_tile_types'],
						this.config['tile_types_location_url'], this.config['tile_types_image_location_url']);
				
				console.info("Attempting to load default tile types ...");
				
				// Bind events
				this.bindEvents();
				
				console.info("Attempting to create view components ...");
				
				// Create view components
				// # Order of creation is important because of dependencies
				
				this.helpSlidesDialog =
					new HelpSlidesDialogComponent();
				
				this.helpOverviewDialog =
					new HelpOverviewDialogComponent(this.helpSlidesDialog);
				
				this.tileTypeEditorDialog =
					new TileTypeEditorDialogComponent(this);
				
				this.tileTypeOverviewDialog =
					new TileTypeOverviewDialogComponent(this, this.tileTypeEditorDialog);
				
				this.assetEditorDialog = 
					new AssetEditorDialogComponent(this);
				
				this.additionalInputDialog =
					new AdditionalInputDialogComponent(this);
				
				this.playfieldComponent = 
					new PlayfieldComponent(this, this.additionalInputDialog);
				
				this.optionsDialog =
					new OptionsDialogComponent(this, this.playfieldComponent);
				
				this.navigationComponent =
					new NavigationComponent(this, this.playfieldComponent, this.optionsDialog, this.helpOverviewDialog, this.tileTypeOverviewDialog, this.assetEditorDialog);
				
				console.info("Attempting to inject view components ...");

				// Inject all views in order and then initialize the application state
				Component.injectAll([
				             this.helpSlidesDialog,
				             this.helpOverviewDialog,
				             this.tileTypeEditorDialog,
				             this.tileTypeOverviewDialog,
				             this.additionalInputDialog,
				             this.playfieldComponent,
				             this.optionsDialog,
				             this.assetEditorDialog,
				             this.navigationComponent
				             ],
				             ["#dialogs", "#dialogs", "#dialogs", "#dialogs", "#dialogs", "#playfield-container", "#dialogs", "#dialogs", "#navigation-container"],
				             [true, true, true, true, true, false, true, true, false])
				             
				.then((function () {
					
					console.info("View components successfully injected!");
					ProgressOverlay.setTitle("Loading default tile types...");
					
					// Initialize app state
					this.setAppState(ApplicationState.EDITOR);
					
					// Load standard tile types
					$.when.apply($, this.loadDefaultTileTypes())
					.fail(function (jqxhr, settings, exception) {
						console.error("Tile type could not be loaded:");
						console.dir(exception);
					})
					.done((function () {
						// Hide the progress overlay
						ProgressOverlay.setVisible(false);
						NotificationManager.showGlobalSuccessNotification("Application started!");
						
						console.info("Default tile types successfully loaded!");
					}).bind(this));
					
					
					
				}).bind(this)).then(null, function (error) {
					console.error(new Error("One of the views could not be injected: " + error));
				});

			}).bind(this), "text")
			.fail(function (jqxhr, settings, exception) {
				throw new Error("Parser grammar could not be loaded: " + exception);
			});
			
		}).bind(this))
		.fail(function(jqxhr, settings, exception) {
			throw new Error("Config could not be loaded: " + exception);
		});
		
	};
	
	// Return class definition to dependency container
	return Application;
	
});
