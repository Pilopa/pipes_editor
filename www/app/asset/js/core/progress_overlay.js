/**
 * @module asset/js/core/progress_overlay
 */
define (["jquery"], function ($) {
	
	function getPercentWidth(element) {
		var width = $(element).width();
		var parentWidth = $(element).offsetParent().width();
		var percent = 100*width/parentWidth;
		return percent;
	}
	
	/**
	    * A module representing the global progress display overlay.
	    * @exports asset/js/core/progress_overlay
	    * @since 1.0.2
	    * @author Konstantin Schaper
	    */
	var progressOverlay = {
		
		/**
		 * Updates the display value for the progress bar.
		 * 
		 * @param {number} newProgress The new percentage value to display (Between 0 and 100; -1 is indeterminate).
		 * @param {boolean} [animate=false] Whether to 
		 */
		setProgress: function (newProgress, animate) {
			// Clamp Value between -1 and 100
			newProgress = Math.max(-1, Math.min(100, newProgress));
			
			// Get the display
			var progressBar = $(".progress-container .progress-bar");
			var progressValue = $(".progress-container .progress-bar .progress-value");
			var progressLabel = $(".progress-container .progress-bar .progress-label");
			
			// Update display depending on mode
			if (newProgress === -1) {
				
				progressBar.addClass("indeterminate");
				
				progressValue.css({
					width: "0%"
				});
				
				progressLabel.text("");
				
			} else {
				progressBar.removeClass("indeterminate");
				
				if (animate) {
					progressValue.animate({
						width: newProgress + "%"
					}, {
						duration: 400,//Math.abs(getPercentWidth(progressValue)-newProgress) * 10,
						progress: function (animation, progress, remaining) {
							progressLabel.text(Math.round(getPercentWidth(this)) + "%");
						}
					});
				} else {
					progressValue.css({
						width: newProgress + "%"
					});
					
					progressLabel.text(newProgress + "%");
				}
			}
			
			
		},
		
		/**
		 * Updates the title text displayed above the progress bar.
		 * 
		 * @param {string} newTitle
		 */
		setTitle: function (newTitle) {
			var progressTitle = $(".progress-container .progress-title");
			progressTitle.text(newTitle);
		},
		
		/**
		 * Changes the visibility state of the progress overlay.
		 * 
		 * @param {boolean} visible
		 */
		setVisible: function (visible) {
			if (visible) $(".progress-container").show();
			else $(".progress-container").hide();
		}
		
	};
	
	// Initialize default behaviour
	progressOverlay.setVisible(true);
	progressOverlay.setProgress(-1);
	
	// Return object to dependency container
	return progressOverlay;
	
});