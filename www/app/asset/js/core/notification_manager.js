/**
 * @module asset/js/core/notification_manager
 */
define(["jquery", "jquery.notify"], function ($) {

	/**
	 * Singleton class which abstracts the notification system to streamline the API.
	 * Currently utilizes the JQuery plug-in notifyjs.
	 * 
	 * @constructor
	 * @alias module:asset/js/core/notification_manager
	 * 
	 * @author Konstantin Schaper
	 * @since 1.0.3
	 */
	var NotificationManager = {
			
		globalPosition: "top center",
		
		showGlobalSuccessNotification: function (text) {
			$.notify(text, {
				className: "success",
				position: this.globalPosition
			});
		}
		
	};
	
	// Return class definition to dependency container
	return NotificationManager;

});