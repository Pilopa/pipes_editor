define (["jquery", "core/dialog_component.class"], function ($, DialogComponent) {	

	QUnit.module( "DialogComponent.class", function () {
		
		QUnit.test("addBackButton", function (assert) {
			
			// Arrange
			var dialogComponentObject = new DialogComponent();
			dialogComponentObject.getViewURL = function () { return "mock/mockDialogComponentView.html"; };
			
			var target = $("#testViewDummy");
			
			// Act
			var promise = dialogComponentObject.injectInto(target);
			
			// Assert
			return promise.then(function (componentResult) {
				
				dialogComponentObject.addBackButton();
				assert.ok(dialogComponentObject.getView().find(".back").length, "A UI element with class 'back' has to be present in a dialog component with a back button.");
			});
			
		});
		
		QUnit.test("switchFrom", function (assert) {
			
			// Arrange
			var dialogComponentObjectFrom = new DialogComponent();
			dialogComponentObjectFrom.getViewURL = function () { return "mock/mockDialogComponentView.html"; };
			
			var dialogComponentObjectTo = new DialogComponent();
			dialogComponentObjectTo.getViewURL = function () { return "mock/mockDialogComponentView.html"; };
			
			var target = $("#testViewDummy");
			target.empty();
			
			// Act
			var promiseFrom = dialogComponentObjectFrom.injectInto(target, false);
			var promiseTo = dialogComponentObjectTo.injectInto(target, false);
			
			// Assert
			return Promise.all([promiseFrom, promiseTo]).then(function (componentResults) {
				
				dialogComponentObjectTo.switchFrom(dialogComponentObjectFrom);
				
				assert.ok(dialogComponentObjectTo.getView().find(".back").length, "After switching to a dialog component from another one," +
						" the 'back' has to be present in the 'to' dialog.");
				
				assert.equal(dialogComponentObjectTo.previous, dialogComponentObjectFrom, "After switching to a dialog component from another one, " +
						"the previous one has to be defined in the actual one for backtracking.");
			});
			
		});
		
	});
	
});