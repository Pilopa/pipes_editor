﻿/**
 * Defines the global 'requireConfig' variable to setup RequireJS with.
 * 
 * @author Konstantin Schaper
 */
var requireConfig =	{
    // Default module directory
    baseUrl: 'app',
    
    // Non-default paths relative to default directory
    paths: {
    	
    	// Folders
        core: 'asset/js/core',
        model: 'asset/js/model',
        lib: 'asset/lib',
        
        // Lib files
        jquery: 'asset/lib/jquery/jquery-2.1.4.min',
        mcustomscrollbar: 'asset/lib/jquery.mcustomscrollbar/jquery.mCustomScrollbar.concat.min',
        jquerymobile: 'asset/lib/jquery.mobile/jquery.mobile-1.4.5.min',
        mousewheel: 'asset/lib/jquery.mousewheel/jquery.mousewheel',
        mwheelintent: 'asset/lib/jquery.mwheelintent/mwheelIntent',
        slick: 'asset/lib/jquery.slick/slick-1.6.0.min',
        jqueryui: 'asset/lib/jquery.ui/jquery-ui.min',
        touchpunch: 'asset/lib/jquery.ui.touchpunch/jquery.ui.touch-punch.min',
        loopprotect: 'asset/lib/jsbin.loopprotect/loop-protect.min',
        jszip: 'asset/lib/jszip/jszip-3.1.3.min',
        pegjs: 'asset/lib/pegjs/peg-0.10.0.min',
        filesaver: 'asset/lib/filesaver/fileSaver.min',
        promise: 'asset/lib/es6promise/es6-promise.min', // Android 4.4 does not support Promises
        "jquery-private": 'asset/lib/jquery.private/jquery.private', // Pseudo library which is intended to remove jquery from global scope
        	
        // Component files
        "component/additionalInputDialog": 'component/additional_input_dialog/additional_input_dialog_component.class',
        "component/helpOverviewDialog": 'component/help_overview_dialog/help_overview_dialog_component.class',
        "component/helpSlide": 'component/help_slide/help_slide_component.class',
        "component/helpSlidesDialog": 'component/help_slides_dialog/help_slides_dialog_component.class',
        "component/menu": 'component/menu/menu_component.class',
        "component/navigation": 'component/navigation/navigation_component.class',
        "component/optionsDialog": 'component/options_dialog/options_dialog_component.class',
        "component/playfield": 'component/playfield/playfield_component.class',
        "component/toolbox": 'component/toolbox/toolbox_component.class',
        
    },
    
    map: {
    	
    	// Swap out jquery for the jquery private
    	'*': {'jquery': 'jquery-private'},
    	// Prevent circular dependency
    	'jquery-private': {'jquery': 'jquery'},
    	
    }
};