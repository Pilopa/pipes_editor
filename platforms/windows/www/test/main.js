﻿(function () {

	// !!! WARNING: The executed tests do not have access to cordova functions and plugins !!!
	
	// Adjust the RequireJS configuration to point to the source directory
	requireConfig.baseUrl = '../app';
	
	// Setup RequireJS
	requirejs.config(requireConfig);
	
	// Define test modules with '.js' ending to load them from paths relative to the 'index.html' file
	// instead to loading them relative to the defined base URL
	var tests = [
		"unit/asset/js/core/application_state.class.test.js",
		"unit/asset/js/core/application.class.test.js",
		"unit/asset/js/core/component.class.test.js",
		"unit/asset/js/core/dialog_component.class.test.js",
		"unit/asset/js/model/playfield.class.test.js",
		"unit/asset/js/model/level.class.test.js"
	];
	
	// Load all test modules and start QUnit
	requirejs(tests, QUnit.start);

})();