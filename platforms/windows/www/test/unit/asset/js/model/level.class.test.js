﻿define(["model/level.class", "model/playfield.class"], function (Level, Playfield) {

	QUnit.module( "Level.class", function () {
		
		QUnit.test("Constructor", function (assert) {
			// Arrange
			var testLevel = undefined;
			var act = undefined;
			
			// Act
			act = function () { testLevel = new Level(); };
	
			// Assert
			assert.raises(act, "Creating a level without providing parameters must throw error to pass.");
			
			// Act
			act = function () { testLevel = new Level(undefined, null, {}, "", ""); };
	
			// Assert
			assert.raises(act, "Creating a level without a title must throw error to pass.");
			
			// Act
			act = function () { testLevel = new Level("", null, {}, "", ""); };
	
			// Assert
			assert.raises(act, "Creating a level with a too short title must throw error to pass.");
			
			// Act
			testLevel = new Level("T");
	
			// Assert
			assert.ok(testLevel instanceof Level, "Creating a level (with minimum settings) should result in a new instance to be created, without errors.");
			assert.ok(testLevel.getPlayfield() instanceof Playfield, "The playfield of a freshly created level (with minimum settings) should be an instance of Playfield.");
			assert.equal(testLevel.getTools().length, 0, "The number of tools in a freshly created level (with minimum settings) should be zero.");
			assert.equal(testLevel.getTitle(), "T", "The title of the created level should be the same as the one inserted into the constructor");
		});
		
	});
	
});