﻿define (["jquery", "core/component.class"], function ($, Component) {	

	QUnit.module( "Component.class", function () {
		
		QUnit.test("Constructor", function (assert) {
			
			// Arrange
			var componentObject;
			
			// Act
			componentObject = new Component();
			
			// Assert
			assert.ok(typeof componentObject !== "undefined", "Result object not undefined");
		});
		
		// See https://stackoverflow.com/questions/28686906/how-to-run-qunit-assertions-on-resolving-a-promise
		QUnit.test("injectInto", function (assert) {
			// Arrange
			var bindEventsCalled = false;
			var initializeCalled = false;
			var initializeCalledBeforeBindEvents = false;
			
			var componentObject = new Component();
			componentObject.getViewURL = function () { return "mock/mockComponentView.html"; };
			componentObject.initialize = function () {
				if (!bindEventsCalled) initializeCalledBeforeBindEvents = true;
				initializeCalled = true;
			};
			componentObject.bindEvents = function () { bindEventsCalled = true; };
			
			var target = $("#testViewDummy");
			
			// Act
			var promise = componentObject.injectInto(target);
			
			// Assert
			return promise.then(function (componentResult) {
				assert.ok(componentResult.getView(), "The view of a freshly injected component has to be present.");
				assert.equal(target.children().length, 1, "The target view must only have one child (the component's view) when the replace flag is set.");
				assert.equal(target.children(".mockComponentViewId").length, 1, "The view has to be present in the HTML dom.");
				assert.notOk(initializeCalledBeforeBindEvents, "The 'bindEvents' method has to be called before the 'initialize' method.");
				assert.ok(bindEventsCalled, "The 'bindEvents' method has to called prior to the resolution of the promise.");
				assert.ok(initializeCalled, "The 'initialize' method has to called prior to the resolution of the promise.");
			});
		});
		
	});
	
});