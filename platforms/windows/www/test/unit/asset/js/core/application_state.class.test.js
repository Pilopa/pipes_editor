﻿define (["core/application_state.class"], function(ApplicationState){
	
	QUnit.module( "ApplicationState", function () {
		
		QUnit.test("verify", function (assert) {
			
			assert.ok(ApplicationState.verify(ApplicationState.NONE), "ApplicationState.NONE => true");
			assert.ok(ApplicationState.verify(ApplicationState.EDITOR), "ApplicationState.EDITOR => true");
			assert.ok(ApplicationState.verify(ApplicationState.PLAY), "ApplicationState.PLAY => true");
			assert.ok(ApplicationState.verify('0'), "'0' => true");
			assert.notOk(ApplicationState.verify(undefined), "undefined => false");
			assert.notOk(ApplicationState.verify(null), "null => false");
			assert.notOk(ApplicationState.verify(NaN), "NaN => false");
			assert.notOk(ApplicationState.verify(-1), "-1 => false");
			assert.notOk(ApplicationState.verify(3), "3 => false");
			
		});
			
	});
	
});