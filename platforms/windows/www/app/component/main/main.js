﻿/**
 * Entrance point to the web application,
 * waits for Cordova to load, then loads RequireJS, initializes the
 * basic view layout and ultimately starts the main application.
 *
 * @author Konstantin Schaper
 */

document.addEventListener("deviceready", function () {
	/* Cordova: Now safe to use device APIs */
	
	// Setup RequireJS
	requirejs.config(requireConfig);

	// Start the main app logic.
	requirejs(['jquery', 'core/application.class'],
	function ($, Application) {
		
		// Wait for the rest of the page to load
		$(function () {
		
			// Load view
			$("div[data-role='page']").load("app/component/main/main_view.html", function () {
				
				// Load style sheet
				$("head").append('<link rel="stylesheet" href="app/component/main/main_style.css" />');
				
				try {
					
					console.info("Attempting to start application ...");
				
					// Start Application
					new Application().start("app/asset/json/config.json", "app/asset/txt/parser_grammar.txt");
					
					console.info("Application successfully started!");
				
				} catch (exception) {
					
					// Log the error
					console.info("An unexpected error occurred during application start!");
					console.error(exception);
					
				}
				
			});
		
		});
		
	});

});
