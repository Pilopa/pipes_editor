﻿/**
 * @module component/menu
 */
define (["jquery", "mcustomscrollbar", "core/component.class", "core/application_state.class"], function ($, mCustomScrollbar, Component, ApplicationState) {
	
	/**
	 * Second level navigation that presents essential actions to the user.
	 *
	 * @param {Application} application The application context used for the save and load processes
	 * @param {OptionsDialogComponent} optionsDialogComponent The dialog to open when the options button is clicked
	 * @param {HelpOverviewDialogComponent} helpOverviewDialogComponent The dialog to open when the help button is clicked
	 * 
	 * @constructor
	 * @alias module:component/menu
	 *
	 * @author Konstantin Schaper
	 */
	function MenuComponent (application, optionsDialogComponent, helpOverviewDialogComponent) {
		// Super constructor call
		MenuComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		this.application = application;
		this.optionsDialogComponent = optionsDialogComponent;
		this.helpOverviewDialogComponent = helpOverviewDialogComponent;
	}
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	MenuComponent.prototype = new Component;
	MenuComponent.prototype.constructor = MenuComponent;
	MenuComponent.prototype.parent = Component.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	MenuComponent.prototype.getViewURL = function() {
		return "app/component/menu/menu_view.html";
	};
	
	MenuComponent.prototype.getStyleURL = function() {
		return "app/component/menu/menu_style.css";
	};
	
	MenuComponent.prototype.bindEvents = function() {
		this.getView().find(".switch-mode").click((this.onSwitchModeButtonClick).bind(this));
		this.getView().find(".save").click((this.onSaveButtonClick).bind(this));
		this.getView().find(".load").click((this.onLoadButtonClick).bind(this));
		this.getView().find(".help").click((this.onHelpButtonClick).bind(this));
		this.getView().find(".survey").click((this.onSurveyButtonClick).bind(this));
		this.getView().find(".options").click((this.onOptionsButtonClick).bind(this));
		this.getView().find('.file-input').change((this.load).bind(this));
		this.application.registerListener("onStateChange", (this.onApplicationStateChange).bind(this));
		this.application.registerListener("onSavableLevelBlobCreated", (this.onSavableLevelBlobCreated).bind(this));
	};
	
	MenuComponent.prototype.initialize = function () {
		// Create scroll bar
		this.getView().find(".scroller").mCustomScrollbar({
			autoHideScrollbar: true,
			autoExpandScrollbar: true,
			scrollInertia: 100
		});
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method that is called whenever the state of the defined application context
	 * changes. Updates the availability of the menu's actions accordingly.
	 */
	MenuComponent.prototype.onApplicationStateChange = function (newState) {
		if (newState === ApplicationState.EDITOR) {
			this.getView().find(".load, .save").show();
		} else if (newState === ApplicationState.PLAY) {
			this.getView().find(".load, .save").hide();
		}
	};
	
	/**
	 * Event handler method that is called when the "SwitchMode" button is clicked.
	 * Toggles the different application modes of the defined application context.
	 */
	MenuComponent.prototype.onSwitchModeButtonClick = function (event) {
		if (this.application.getAppState() === ApplicationState.EDITOR) {
			this.application.setAppState(ApplicationState.PLAY);
		} else if (this.application.getAppState() === ApplicationState.PLAY) {
			this.application.setAppState(ApplicationState.EDITOR);
		}
	};
	
	/**
	 * Event handler method that is called whenever the defined application context
	 * finished the process of creating a savable blob for a level, both on success and failure.
	 * Causes the application device to download the created blob on success, or throws the 
	 * occurred error on failure.
	 * 
	 * @param {Error|undefined} error The error that occurred, or undefined on success
	 * @param {blob|undefined} blob The created blob, or undefined on failure
	 * @param {string} proposedFileName The title of the level to be saved
	 * @throws {Error} Throws the error parameter if it is not undefined
	 */
	MenuComponent.prototype.onSavableLevelBlobCreated = function (error, blob, proposedFileName) {
		try {
			// Check for error
			if (error) throw error;
			
			// Check for cordova device plugins to determine platform
			if (device) {
				
				// # Choose platform specific storage solution
				
				if (device.platform === "browser") {
										
					// Save via HTML5
					window.URL = window.webkitURL || window.URL;
					var e = document.createEvent('MouseEvents'),
					a = document.createElement('a');
					a.download = proposedFileName + ".pipes";
					a.href = window.URL.createObjectURL(blob);
					a.dataset.downloadurl = ['application/json', a.download, a.href].join(':');
					e.initEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
					a.dispatchEvent(e);
					
				} else if (device.platform === "windows") {
					
					// The file plugin does not support saving blobs under windows
					// Check if WinJS is present
					// @see http://news.ebscer.com/2016/01/writing-to-a-file-in-a-windows-10-cordova-app/
					// @see https://social.msdn.microsoft.com/Forums/sqlserver/en-US/68880fa3-7a6b-407c-8ed3-566150d34ac0/uwp-winjs-writing-a-blob-to-a-file-in-a-uwp-js-app?forum=wpdevelop
					if (Windows && Windows.Storage) {

						
						// Initiate the writer
						var savePicker = new Windows.Storage.Pickers.FileSavePicker();
						
						// Set the destination
						savePicker.suggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.downloads;
						
						// Set file name
						savePicker.suggestedFileName = proposedFileName;
						
						// Set file type choice for save dialog
						savePicker.fileTypeChoices.insert("PIPES", [".pipes"]);
						
						// Get the file handle
						savePicker.pickSaveFileAsync().then(function (file) {
							
							// Write to input file chosen by user
							if (file) {
								// Open the returned file in order to copy the data 
								file.openAsync(Windows.Storage.FileAccessMode.readWrite).then(function (output) { 
									 
									// Get the IInputStream stream from the blob object 
									var input = blob.msDetachStream(); 
					 
									// Copy the stream from the blob to the File stream 
									Windows.Storage.Streams.RandomAccessStream.copyAsync(input, output).then(function () { 
										output.flushAsync().then(function () { 
											input.close(); 
											output.close(); 
										}, function (exception) {
											throw exception;
										}); 
									}, function (exception) {
										throw exception;
									}); 
								});
							} else {
								// User cancelled the file save dialog
							}
							
						}, function (exception) {
							throw exception;
						});
						
					} else {
						throw new Error("WinJS not found");
					}
					
				} else {
					
					// Check for essential file plugin
					if (cordova.file) {
						
						// Get the correct directory
						window.resolveLocalFileSystemURL(cordova.file.dataDirectory, 
						function (directoryEntry) {
							
							// Create the pipes directory if necessary
							directoryEntry.getDirectory("Pipes", {create: true}, function (pipesDirectoryEntry) {
								
								// Get the file entry
								pipesDirectoryEntry.getFile(proposedFileName + ".pipes", {create: true, exclusive: false}, function (fileEntry) {
									
									// Write to the file entry
									fileEntry.createWriter(function (fileWriter) {
										
										fileWriter.onerror = function (error) {
											// Writing error
											console.info("FileWriterError");
											throw error;
										};
										
										fileWriter.write(blob);
										
									});
									
								}, 
								// File error
								function (error) {
									console.info("FileError");
									throw error;
								});
								
							}, // Pipes directory error
							function (error) {
								console.info("PipesDirectoryError");
								throw error;
							});
						},
						
						// Directory error
						function (error) {
							console.info("DataDirectoryError");
							throw error;
						});
						
					} else {
						throw new Error("File plugin not found");
					}
				}
			} else {
				throw new Error("Device plugin not found");
			}
			
		} catch (exception) {
			// Log exception
			console.error(exception);
		}
	};
	
	/**
	 * Event handler method that is called when the "Save" button is clicked.
	 * Calls the defined application context to start the creation of a savable blob for the active level.
	 * The result is handled by the event handler MenuComponent.prototype.onSavableLevelBlobCreated.
	 */
	MenuComponent.prototype.onSaveButtonClick = function (event) {
		if (this.application.getAppState() === ApplicationState.EDITOR) {
			// Start the blob generation of the active level.
			this.application.saveLevel();
		}
	};
	
	/**
	 * Event handler method that is called when the "Load" button is clicked.
	 * Opens a file input dialog for the user to provide the destination URL.
	 * The result is handled by the event handler MenuComponent.load.
	 */
	MenuComponent.prototype.onLoadButtonClick = function (event) {
		this.getView().find('.file-input').trigger('click');
	};
	
	/**
	 * Event handler method that is called when the "Help" button is clicked.
	 * Opens the HelpOverviewDialogComponent associated with this menu component.
	 */
	MenuComponent.prototype.onHelpButtonClick = function (event) {
		this.helpOverviewDialogComponent.open();
	};
	
	/**
	 * Event handler method that is called when the "Options" button is clicked.
	 * Opens the OptionsDialogComponent associated with this menu component.
	 */
	MenuComponent.prototype.onOptionsButtonClick = function (event) {
		this.optionsDialogComponent.open();
	};
	
	/**
	 * Event handler method that is called when the "Survey" button is clicked.
	 * Opens the user survey of this application in a new browser tab.
	 */
	MenuComponent.prototype.onSurveyButtonClick = function (event) {
		window.open("http://surveys.konstantinschaper.info/index.php/718162?lang=en");
	};
	
	/**
	 * Event handler method that is called when the load file selection dialog is 
	 * submitted. Reads the submitted file and forwards an array buffer to the associated
	 * application class to eventually attempt to deserialize the level file.
	 */
	MenuComponent.prototype.load = function (event) {
		// Check for the various File API support.
		if (window.File && window.FileReader && window.FileList && window.Blob) {
			var file = event.target.files[0];
			
			if (typeof file === "undefined") return;
			
			var reader = new FileReader();
			
			reader.onload = (function(e){
				// Retrieve the array buffer data from the onLoad event
				var data = e.target.result;
				
				// Asynchronously construct the level
				this.application.loadLevel(data);
			}).bind(this);
			
			reader.readAsArrayBuffer(file);	
		} else {
			throw new Error('The File APIs are not fully supported in this browser.');
		}
	};
	
	// Return class definition to dependency container
	return MenuComponent;
	
});