﻿/**
 * @module component/helpSlide
 */
define (["jquery", "mcustomscrollbar", "core/component.class"], function ($, mCustomScrollbar, Component) {
	
	/**
	 * Simple embeddable Component which represents a single help slide in a HelpSlidesDialogComponent slideshow.
	 * 
	 * @param {Slick} carousel
	 * @param {number} index The slide index
	 * @param {string} title
	 * @param {string} description
	 * @param {string|undefined} imagePath The image to display under the description, undefined means no image is displayed
	 * 
	 * @constructor
	 * @alias module:component/helpSlide
	 * 
	 * @author Konstantin Schaper
	 */
	function HelpSlideComponent (carousel, index, title, description, imagePath) {
		// Super constructor call
		HelpSlideComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * @type {Slick}
		 */
		this.carousel = carousel;
		
		/**
		 * @type {string}
		 */
		this.title = title;
		
		/**
		 * @type {string}
		 */
		this.description = description;
		
		/**
		 * @type {string}
		 */
		this.imagePath = imagePath;
		
		/**
		 * @type {number}
		 */
		this.index = index;
		
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	HelpSlideComponent.prototype = new Component;
	HelpSlideComponent.prototype.constructor = HelpSlideComponent;
	HelpSlideComponent.prototype.parent = Component.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	HelpSlideComponent.prototype.getViewURL = function() {
		return "app/component/help_slide/help_slide_view.html";
	};
	
	HelpSlideComponent.prototype.getStyleURL = function() {
		return "app/component/help_slide/help_slide_style.css";
	};
	
	HelpSlideComponent.prototype.initialize = function () {
		// Create scroll bar	
		this.getView().find(".scroller").mCustomScrollbar({
			autoHideScrollbar: true,
			autoExpandScrollbar: true,
			scrollInertia: 100
		});
		
		// Fill in data
		this.getView().data("title", this.title);
		this.getView().find("p").html(this.description);
		this.getView().find("img").attr("src", this.imagePath);
		this.carousel.slick("slickAdd", this.getView());
		
		// Activate links
		$(".link").click(this, this.onLinkClick);
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method being called when a link is clicked.
	 * Changes the current slide to the one defined by the clicked link.
	 */
	HelpSlideComponent.prototype.onLinkClick = function (event) {
		event.data.carousel.slick("slickGoTo", $(this).data("keywordid"), true);
	};
	
	// Return class definition to dependency container
	return HelpSlideComponent;
	
});