﻿/**
 * @module component/playfield
 */
define (["jquery", "jqueryui", "touchpunch", "core/component.class", "model/tile.class", "model/position.class"], function ($, $ui, touchpunch, Component, Tile, Position) {

	/**
	 * Component responsible for displaying a {Playfield} object and letting the player/editor modify it.
	 * 
	 * @param {Application} application The application context to register event listeners on
	 * @param {AdditionalInputDialogComponent} additionalInputDialogComponent Called when a new tile is placed on the playfield which requires additional user input
	 * 
	 * @constructor
	 * @alias module:component/playfield
	 * 
	 * @author Konstantin Schaper
	 */
	function PlayfieldComponent(application, additionalInputDialogComponent) {
		// Super constructor call
		PlayfieldComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * @type {Application}
		 */
		this.application = application;
		
		/**
		 * @type {AdditionalInputDialogComponent}
		 */
		this.additionalInputDialogComponent = additionalInputDialogComponent;
		
		/**
		 * playfield cols / rows
		 * @type {Number}
		 */
		this.colRowRatio = 1.0;
		
		/**
		 * playfield width / height
		 * @type {Number}
		 */
		this.aspectRatio = 1.0;
		
	}
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	PlayfieldComponent.prototype = new Component;
	PlayfieldComponent.prototype.constructor = PlayfieldComponent;
	PlayfieldComponent.prototype.parent = Component.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	PlayfieldComponent.prototype.getViewURL = function() {
		return "app/component/playfield/playfield_view.html";
	};
	
	PlayfieldComponent.prototype.getStyleURL = function() {
		return "app/component/playfield/playfield_style.css";
	};
	
	PlayfieldComponent.prototype.bindEvents = function() {
		$(window).on("window::resize", (this.reset).bind(this));
		
		this.application.registerListener("onStateChange", (this.onApplicationStateChange).bind(this));
		this.application.registerListener("onLevelLoaded", (this.onLevelLoaded).bind(this));
	};
	
	PlayfieldComponent.prototype.initialize = function () {
		// Initially update the view
		this.reset();
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method that is called when the associated application's state changes.
	 * Updates the view to reflect this change.
	 */
	PlayfieldComponent.prototype.onApplicationStateChange = function (newState) {
		this.reset();
	};
	
	/**
	 * Event handler method that is called whenever a new level is loaded.
	 * Updates the view to display the new level.
	 */
	PlayfieldComponent.prototype.onLevelLoaded = function (error, level) {
		this.reset();
	};
	
	/**
	 * Recreates all tile display elements and recalculates the playfield size as well as the size of each tile.
	 */
	PlayfieldComponent.prototype.reset = function () {
		this.resetCells();
		this.updateBounds();
	};

	
	/**
	 * @returns {Playfield} The playfield of the associated application's active level
	 */
	PlayfieldComponent.prototype.getPlayfieldData = function () {
		return this.application.activeLevel.getPlayfield();
	};
	
	/**
	 * Recalculates the size of the visual representation of this playfield component and
	 * its tile displays.
	 */
	PlayfieldComponent.prototype.updateBounds = function() {
		
		// Setup variables
		
		var playfieldContainer = this.getView().parent();
		
		var rowCount = this.getPlayfieldData().getRowCount();
		var columnCount = this.getPlayfieldData().getColumnCount();
		
		var newWidth = "100%";
		var newHeight = "100%";
		
		// Calculate colrowratio
		this.colRowRatio = (columnCount / rowCount);
		
		// Calculate aspect ratio
		this.aspectRatio = playfieldContainer.width() / playfieldContainer.height();
		
		// Calculate new dimensions
		if (this.colRowRatio === 1.0) {
			if (this.aspectRatio < 1.0) {
				newWidth = playfieldContainer.width();
				newHeight = newWidth / this.colRowRatio;
			} else {
				newHeight = playfieldContainer.height();
				newWidth = newHeight * this.colRowRatio;
			}
		} else if (this.colRowRatio < 1.0) {
			if (this.aspectRatio > this.colRowRatio) {
				newHeight = playfieldContainer.height();
				newWidth = newHeight * this.colRowRatio;
			} else {
				newWidth = playfieldContainer.width();
				newHeight = newWidth / this.colRowRatio;
			}
		} else { // this.colRowRatio >= 1.0
			if (this.aspectRatio < this.colRowRatio) {
				newWidth = playfieldContainer.width();
				newHeight = newWidth / this.colRowRatio;
			} else {
				newHeight = playfieldContainer.height();
				newWidth = newHeight * this.colRowRatio;
			}
			
		}
		
		// Resize table
		this.getView().css({
			"height" : newHeight + "px",
			"width" : newWidth + "px"
		});
		
		// Redefine cells
		this.updateCellSize();
		
	};
	
	/**
	 * Recreates the tile display elements from the playfield data.
	 */
	PlayfieldComponent.prototype.resetCells = function() {
		// Clear playfield
		this.getView().empty();
		var tbody = $("<tbody></tbody>").appendTo(this.getView());
		
		// Remember this
		var that = this;
		
		// Create new tile view elements
		for (var y = 0; y < this.getPlayfieldData().getRowCount(); y++) {
			tbody.append("<tr>");
			for (var x = 0; x < this.getPlayfieldData().getColumnCount(); x++) {
				$("<td data-cr=" + x + "," + y + " class='tile'></td>")
				.appendTo(tbody)
				.draggable({
					delay: 150,
					distance: 20,
					scroll: false,
					containment: "window",
					opacity: 0.75,
					cancel: ".playfield.disabled, .immovable",
					revert: function (droppable) {
						
						if (!droppable) $(this).draggable("option", "revertDuration", 150);
						else $(this).draggable("option", "revertDuration", 0);
						
						return true;
					},
					zIndex: 99,
					start: function (event, ui) {
						// Center the helper to the cursor
						$(this).draggable('instance').offset.click = {
				            left: Math.floor(ui.helper.width() / 2),
				            top: Math.floor(ui.helper.height() / 2)
				        }; 
					}
				})
				.droppable({
					accept: function (draggable) {
						return ($(this).hasClass("empty") || !$(this).hasClass("immovable")) &&
						(draggable.hasClass("tool-drag") 
						|| draggable.hasClass("tile"));
					},
					drop: function (event, ui) {
						var dropCR = $(this).data("cr");
						var dropSplit = dropCR.split(",");
						var dropCol = dropSplit[0];
						var dropRow = dropSplit[1];
						if (ui.draggable.hasClass('tool-drag')) {
							var dragTileTypeId = ui.helper.data("ttid");
							if (that.application.getTileTypes()[dragTileTypeId].additionalValues === undefined)
								that.place(dropCol, dropRow, dragTileTypeId);
							else {
								// Open the dialog with the gathered data
								that.additionalInputDialogComponent.open({
									tileTypeId: dragTileTypeId,
									row: dropRow,
									column: dropCol
								}, that);
							}
						} else if (ui.draggable.hasClass('tile')) { 
							var dragCR = ui.helper.data("cr");
							var dragSplit = dragCR.split(",");
							var dragCol = dragSplit[0];
							var dragRow = dragSplit[1];
							that.swap(dragCol, dragRow, dropCol, dropRow);
						}
					}
				})
				.on("click", function() {
					var cr = $(this).data("cr");
					var split = cr.split(",");
					var x = split[0];
					var y = split[1];
					if (!that.getView().hasClass("disabled"))
						that.rotate(x, y);
				});
				this.updateTileInformation(x,y);
			}
			tbody.append("</tr>");
		}
	};
	
	/**
	 * @param {Position} position The position for which to get the tile display
	 * @returns {JQuery|undefined} The tile display element at the given position
	 */
	PlayfieldComponent.prototype.getCell = function(position) {
		return this.getView().find(".tile[data-cr='" + position.getColumn() + "," + position.getRow() + "']");
	};
	
	/**
	 * Iteratively calls PlayfieldComponent.updateTileInformation for each position on the associated application's active level.
	 */
	PlayfieldComponent.prototype.updateTilesInformation = function () {
		for (var y = 0; y < this.getPlayfieldData().getRowCount(); y++) {
			for (var x = 0; x < this.getPlayfieldData().getColumnCount(); x++) {
				this.updateTileInformation(x, y);
			}
		}
	};
	
	/**
	 * Updates the visual representation of the tile display at the given position.
	 * 
	 * @param {integer|Position} x The x-coordinate or a complete Position object
	 * @param {integer} [y] The y-coordinate
	 */
	PlayfieldComponent.prototype.updateTileInformation = function (x,y) {
		var pos;
		if (x instanceof Position) {
			pos = x;
		} else {
			pos = new Position(y, x);
		}
		
		var cell = this.getCell(pos);
		var tile = this.getPlayfieldData().getTile(pos);
		
		if (tile === undefined) {
			cell
			.css({
				"background-image": "url(app/asset/img/tile_type/Empty.png)"
			})
			.addClass("empty immovable unrotatable");
		} else {
			// Appearence
			cell
			.css({
				"background-image": this.application.getTileImageUrl(tile, this.application.activeLevel),
				"transform": "rotate(" + ( tile.getRotation() * 90 ) + "deg)"
			}).removeClass("empty");
			
			// Movable
			if (tile.isMovable())
				cell.removeClass("immovable");
			else
				cell.addClass("immovable");
			
			// Rotatable
			if (tile.isRotatable()) 
				cell.removeClass("unrotatable");
			else
				cell.addClass("unrotatable");
		}
	};
	
	/**
	 * Updates the cell size that is used for all tile displays and automatically applies it to
	 * all tiles in the visual representation of this playfield component.
	 */
	PlayfieldComponent.prototype.updateCellSize = function () {	
		var newCellWidth = "0";
		var newCellHeight = "0";
		
		// Calculate new dimensions
		if (this.colRowRatio === 1.0) {
			if (this.aspectRatio < 1.0) {
				newCellHeight = this.getView().height() / this.getPlayfieldData().getRowCount();
				newCellWidth = newCellHeight;
			} else {
				newCellWidth = this.getView().width() / this.getPlayfieldData().getColumnCount();
				newCellHeight = newCellWidth;
			}
		} else if (this.colRowRatio < 1.0) {
			if (this.aspectRatio > this.colRowRatio) {
				newCellWidth = this.getView().width() / this.getPlayfieldData().getColumnCount();
				newCellHeight = newCellWidth;
			} else {
				newCellHeight = this.getView().height() / this.getPlayfieldData().getRowCount();
				newCellWidth = newCellHeight;
			}
		} else { // this.colRowRatio >= 1.0
			if (this.aspectRatio < this.colRowRatio) {
				newCellHeight = this.getView().height() / this.getPlayfieldData().getRowCount();
				newCellWidth = newCellHeight;
			} else {
				newCellWidth = this.getView().width() / this.getPlayfieldData().getColumnCount();
				newCellHeight = newCellWidth;
			}
		}
		
		this.cellWidth = newCellWidth;
		this.cellHeight = newCellHeight;
		
		$(".playfield td").css({
			"width": Math.floor(newCellWidth) + "px",
			"height": Math.floor(newCellHeight) + "px"
		});
	};
	
	/**
	 * Makes the tiles at the given positions swap places,
	 * both in the model and in the visual representation.
	 * 
	 * @param xa The x-coordinate of the first position
	 * @param ya The y-coordinate of the first position
	 * @param xb The x-coordinate of the second position
	 * @param yb The y-coordinate of the second position
	 */
	PlayfieldComponent.prototype.swap = function (xa, ya, xb, yb) {
		var posA = new Position(ya, xa);
		var posB = new Position(yb, xb);
		this.getPlayfieldData().moveTile(posA, posB);
		this.updateTileInformation(posA);
		this.updateTileInformation(posB);
	};
	
	/**
	 * Removes the tile at the given position,
	 * both in the model and in the visual representation.
	 * 
	 * @param x The x-coordinate
	 * @param y The y-coordinate
	 */
	PlayfieldComponent.prototype.remove = function (x,y) {
		var pos = new Position(y,x);
		var tile = this.getPlayfieldData().getTile(pos);
		this.getPlayfieldData().removeTile(pos);
		this.updateTileInformation(pos);
		
		// Fire tile removed event on application
		this.application.fireEvent("onTileRemoved", [tile]);
	};
	
	/**
	 * Rotates the tile at the given position,
	 * both in the model and in the visual representation.
	 * 
	 * @param x The x-coordinate
	 * @param y The y-coordinate
	 */
	PlayfieldComponent.prototype.rotate = function (x,y) {
		var pos = new Position(y,x);
		var tile = this.getPlayfieldData().getTile(pos);
		if (tile !== undefined && tile.isRotatable()) {
			tile.rotate();
			this.updateTileInformation(pos);
		}
	};
	
	/**
	 * Prevents any further interaction with this playfield component.
	 */
	PlayfieldComponent.prototype.disableInteraction = function () {
		this.getView().addClass("disabled");
	};
	
	/**
	 * Reenables interaction with this playfield component.
	 */
	PlayfieldComponent.prototype.enableInteraction = function () {
		this.getView().removeClass("disabled");
	};
	
	/**
	 * Places a tile of the given tile type (id parameter) at the given location,
	 * using the additional values provided.
	 * 
	 * @param {integer} x The x-coordinate
	 * @param {integer} y The y-coordinate
	 * @param {string} id The tile type id of which to create a new tile
	 * @param {object} additionalValues The additional values provided by the user
	 */
	PlayfieldComponent.prototype.place = function (x, y, id, additionalValues) {
		// Get the tile type class
		var tileType = this.application.getTileTypes()[id];
		
		// Create the tile
		var pos = new Position(y,x);
		var newTile = new tileType(false, pos, additionalValues);
		var previousTile = this.getPlayfieldData().getTile(pos);
		
		// Remove previous tile
		if (typeof previousTile !== "undefined") {
			if (previousTile.isMovable())
				this.remove(x, y);
			else
				return;
		}
		
		// Place the tile
		this.getPlayfieldData().placeTile(newTile);
		
		// Update the UI
		this.updateTileInformation(pos);
		
		// Fire tile placed event on application
		this.application.fireEvent("onTilePlaced", [newTile]);
	};
	
	/**
	 * Highlightes the tile displays on which errors ocurred.
	 * This is done through the css class "error".
	 * 
	 * @param {object[]} errors An array containing all errors
	 */
	PlayfieldComponent.prototype.showErrorHightlighting = function (errors) {
		errors.forEach(function (error, index, array) {
			if (typeof error["tile"] !== "undefined")
				this.getCell(error["tile"].getPosition()).addClass("error");
			else if (typeof error["position"] !== "undefined")
				this.getCell(error["position"]).addClass("error");
			else
				this.getView().addClass("error");
		}, this);
	};
	
	/**
	 * Disables the error highlighting again.
	 */
	PlayfieldComponent.prototype.resetErrorHighlighting = function () {
		this.getView().removeClass("error").find(".tile").removeClass("error");
	};
	
	// Return class definition to dependency container
	return PlayfieldComponent;
});