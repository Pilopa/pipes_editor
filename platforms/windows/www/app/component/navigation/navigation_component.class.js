﻿/**
 * @module component/navigation
 */
define (["jquery", "core/component.class", "core/application_state.class", "component/menu", "component/toolbox"],
		function ($, Component, ApplicationState, MenuComponent, ToolboxComponent) {
	
	/**
	 * Compound component that consists of a MenuComponent and a ToolboxComponent, which are both created internally during initialization.
	 * 
	 * @param {Application} application The associated application context used to register event listeners on
	 * @param {PlayfieldComponent} playfieldComponent Required by the ToolboxComponent constructor
	 * @param {OptionsDialogComponent} optionsDialogComponent Required by the MenuComponent constructor
	 * @param {HelpOverviewDialogComponent} helpOverviewDialogComponent Required by the MenuComponent constructor
	 * 
	 * @constructor
	 * @alias module:component/navigation
	 * 
	 * @author Konstantin Schaper
	 */
	function NavigationComponent (application, playfieldComponent, optionsDialogComponent, helpOverviewDialogComponent) {
		// Super constructor call
		NavigationComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		this.application = application;
		this.playfieldComponent = playfieldComponent;
		this.optionsDialogComponent = optionsDialogComponent;
		this.helpOverviewDialogComponent = helpOverviewDialogComponent;
		this.viewId = NavigationComponent.nextId++;
	}
	
	/* ****************** */
	/*  Static Variables  */
	/* ****************** */
	
	/**
	 * Integer value which gets incremented every time a new navigation component is created.
	 * This value is used to define and access the element ids for the navigation tabs.
	 * For further information see JQueryMobile documentation.
	 * 
	 * @type {number}
	 */
	NavigationComponent.nextId = 1;
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	NavigationComponent.prototype = new Component;
	NavigationComponent.prototype.constructor = NavigationComponent;
	NavigationComponent.prototype.parent = Component.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	NavigationComponent.prototype.getViewURL = function() {
		return "app/component/navigation/navigation_view.html";
	};
	
	NavigationComponent.prototype.getStyleURL = function() {
		return "app/component/navigation/navigation_style.css";
	};
	
	NavigationComponent.prototype.bindEvents = function() {
		this.application.registerListener("onStateChange", (this.onApplicationStateChange).bind(this));
	};
	
	NavigationComponent.prototype.initialize = function () {
		// Make navbar component specific
		this.getView().find(".menu-pane-tab").attr("href", "#menu-pane-" + this.viewId);
		this.getView().find(".menu-pane").attr("id", "menu-pane-" + this.viewId);
		
		this.getView().find(".toolbox-pane-tab").attr("href", "#toolbox-pane-" + this.viewId);
		this.getView().find(".toolbox-pane").attr("id", "toolbox-pane-" + this.viewId);
		
		this.getView().find(".ui-tabs").tabs("refresh");
		this.getView().find(".menu-pane-tab").trigger("click");
		
		// Update size of toolbox when is opened
		this.getView().find("a[href='#toolbox-pane-" + this.viewId + "']").click((function () {
			setTimeout((function() {
				this.toolboxComponent.adjustSizes();
			}).bind(this), 0);
		}).bind(this));
		
		// Create tab content
		this.toolboxComponent = new ToolboxComponent(this.application, this, this.playfieldComponent);
		this.menuComponent = new MenuComponent(this.application, this.optionsDialogComponent, this.helpOverviewDialogComponent);
		
		// Inject tab content
		this.toolboxComponent.injectInto(this.getView().find(".toolbox-pane"));
		this.menuComponent.injectInto(this.getView().find(".menu-pane"));
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method that is called whenever the state of the associated application changes.
	 * Updates the UI to display the new state's name.
	 */
	NavigationComponent.prototype.onApplicationStateChange = function (newState) {
		if (newState === ApplicationState.EDITOR) {
			this.getView().find(".state-display").text("Editor Mode");
		} else if (newState === ApplicationState.PLAY) {
			this.getView().find(".state-display").text("Play Mode");
		}
	};
	
	// Return class definition to dependency container
	return NavigationComponent;
	
});