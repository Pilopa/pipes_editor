﻿/**
 * @module asset/js/model/stream
 */
define (["model/color.class", "model/direction.class", "model/position.class"], function (Color, Direction, Position) {

	/**
	 * Class representing the data during tile evaluation.
	 * 
	 * @param {Color} color
	 * @param {Position} nextTilePosition
	 * @param {Tile} nextTile
	 * @param {Direction} nextEntryDirection
	 * 
	 * @constructor
	 * @alias module:asset/js/model/stream
	 * 
	 * @author Konstantin Schaper
	 */
	function Stream(color, nextTilePosition, nextTile, nextEntryDirection) {
		
		/* ****************** */
		/* 	   Constructor	  */
		/* ****************** */
		
		// Attempt to parse paremeters
		color = parseInt(color);
		nextEntryDirection = parseInt(nextEntryDirection);
		
		// Verify color parameter
		if (color !== undefined) {
			// Verify input type
			if (!Color.verify(color))
				throw "The parameter 'color' has to be of type 'Color'.";
		} else 
			throw "The parameter 'color' has to be provided. Given: " + color;
		
		// Verify nextEntryDirection parameter
		if (nextEntryDirection !== undefined) {
			// Verify input type
			if (!Direction.verify(nextEntryDirection))
				throw "The parameter 'nextEntryDirection' has to be of type 'Direction'.";
		} else 
			throw "The paremeter 'nextEntryDirection' has to be provided. Given: " + nextEntryDirection;
		
		// Verify nextEntryDirection parameter
		if (nextTilePosition !== undefined) {
			// Verify input type
			if (!(nextTilePosition instanceof Position))
				throw "The parameter 'nextTilePosition' has to be of type 'Position'.";
		} else 
			throw "The paremeter 'nextTilePosition' has to be provided. Given: " + nextTilePosition;
		
		/* ****************** */
		/* Privileged methods */
		/* ****************** */
		
		/**
		 * Returns the current color of this stream,
		 * returns Color.NONE if it is empty.
		 * 
		 * @returns {Color}
		 */
		this.getColor = function () {
			return color;
		};
		
		/**
		 * Overrides the active color of the stream with the specified one.
		 * 
		 * @param {Color} newColor
		 */
		this.setColor = function (newColor) {
			newColor = parseInt(newColor);
			// Verify newColor parameter
			if (newColor !== undefined) {
				// Verify input type
				if (!Color.verify(newColor))
					throw "The parameter 'newColor' has to be of type 'Color'.";
				else
					color = newColor;
			} else 
				throw "The parameter 'newColor' has to be provided. Given: " + newColor;
		};
		
		/**
		 * Returns the position of the next tile to be evaluated.<br>
		 * The value returned depends on the stage the evaluation process is in.
		 * For more information, look into the event methods of the Tile class.
		 * 
		 * @returns {Position}
		 */
		this.getNextTilePosition = function () {
			return nextTilePosition;
		};
		
		/**
		 * Defines the position of the next tile to be evaluated.
		 * 
		 * @param {Tile|undefined} newNextTile
		 */
		this.setNextTilePosition = function (newNextTilePosition) {
			// Verify input type
			if (!(newNextTilePosition instanceof Position))
				throw "The parameter 'newNextTilePosition' has to be of type 'Position'.";
			else
				nextTilePosition = newNextTilePosition;
		};
		
		/**
		 * Returns the next tile to be evaluated.<br>
		 * The value returned depends on the stage the evaluation process is in.
		 * For more information, look into the event methods of the Tile class.
		 * 
		 * @returns {Tile}
		 * @deprecated Use Stream.getNextTilePosition instead.
		 */
		this.getNextTile = function () {
			return nextTile;
		};
		
		/**
		 * Defines the next tile to be evaluated.
		 * 
		 * @param {Tile|undefined} newNextTile
		 * @deprecated Use Stream.setNextTilePosition instead.
		 */
		this.setNextTile = function (newNextTile) {
			// Verify input type
			if (!(newNextTile instanceof Tile) && typeof newNextTile !== "undefined")
				throw "The parameter 'newNextTile' has to be of type 'Tile' or 'undefined'.";
			else
				nextTile = newNextTile;
		};
		
		/**
		 * Returns the direction from which the nextTile is going to be evaluated.
		 * 
		 * @returns {Direction}
		 */
		this.getNextEntryDirection = function () {
			return nextEntryDirection;
		};
		
		/**
		 * Defines the direction from which the next tile is going to be evaluated.
		 * 
		 * @param {Direction} newNextEntryDirection
		 */
		this.setNextEntryDirection = function (newNextEntryDirection) {
			newNextEntryDirection = parseInt(newNextEntryDirection);
			// Verify newNextEntryDirection parameter
			if (newNextEntryDirection !== undefined) {
				// Verify input type
				if (!Direction.verify(newNextEntryDirection))
					throw "The parameter 'newNextEntryDirection' has to be of type 'Direction'.";
				else 
					nextEntryDirection = newNextEntryDirection;
			} else 
				throw "The paremeter 'newNextEntryDirection' has to be provided. Given: " + newNextEntryDirection;
		};
		
	}

	// Return class definition to dependency container
	return Stream;
});