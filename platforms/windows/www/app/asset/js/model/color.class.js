﻿define(function () {

	/**
	 * Enumeration representing the different types of colors a stream can be.
	 * 
	 * @author Konstantin Schaper
	 * @exports asset/js/model/color
	 */
	var Color = {
		NONE: 0,
		ALPHA: 1,
		BETA: 2,
		
		/**
		 * @param value The variable to verify
		 * @returns {boolean} Whether or not the given value is a color.
		 */
		verify: function (value) {
			return isInteger(value) && value >= 0 && value <= 2;
		},
		
		/**
		 * @param {Color} color The color to transform into a string
		 * @returns {string} The string representation of the given color value
		 */
		toString: function (color) {
			return (color == this.NONE) ? "NONE" :
					(color == this.ALPHA) ? "ALPHA" :
					(color == this.BETA) ? "BETA" : undefined;
		},
		
		/**
		 * Array containing all values defined by this enumeration.
		 * 
		 * @type {array}
		 */
		values: [this.NONE, this.ALPHA, this.BETA]
	};
	
	// Return class definition to dependency container
	return Color;

});