﻿define(function () {

	/**
	 * Enumeration representing the four different directions.
	 * 
	 * @author Konstantin Schaper
	 * @exports asset/js/model/direction
	 */
	var Direction = {
		UP: 0,
		RIGHT: 1,
		DOWN: 2,
		LEFT: 3,
		
		/**
		 * @param value The variable to verify
		 * @returns {boolean} Whether or not the given value is a direction.
		 */
		verify: function (value) {
			return isInteger(value) && value >= 0 && value <= 3;
		},
		
		/**
		 * @param {Direction} direction The direction to negate
		 * @returns {Direction} The opposite direction of the input direction
		 */
		negate: function (direction) {
			return this.rotated(direction, 2);
		},
		
		/**
		 * Rotates the given direction by the given rotation and returns the result.
		 * This method exclusively returns valid Direction values (0-3).
		 * 
		 * @param {Direction} direction The direction to rotate
		 * @param {Direction} rotation The amount by which to rotate the given direction
		 * @returns {Number} The rotated direction
		 */
		rotated: function (direction, rotation) {
			direction = parseInt(direction);
			rotation = parseInt(rotation);
			var rotatedDirection = direction + rotation;
			return ((rotatedDirection < 0) ? 4 + rotatedDirection : rotatedDirection ) % 4;
		},
		
		/**
		 * @param {Direction} direction The direction to transform into a string
		 * @returns {string} The string representation of the given direction value
		 */
		toString: function (direction) {
			return (direction == this.UP) ? "UP" :
					(direction == this.RIGHT) ? "RIGHT" :
					(direction == this.DOWN) ? "DOWN" :
					(direction == this.LEFT) ? "LEFT" : undefined;
		},
		
		/**
		 * Array containing all values defined by this enumeration.
		 * 
		 * @type {array}
		 */
		values: [this.UP, this.RIGHT, this.DOWN, this.LEFT]
	};
	
	// Return class definition to dependency container
	return Direction;

});