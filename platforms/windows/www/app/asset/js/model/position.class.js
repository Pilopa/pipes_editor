﻿/**
 * @module asset/js/model/position
 */
define(["model/direction.class"], function (Direction) {
	
	/**
	 * Immutable class representing a location on the play field square-grid.
	 * 
	 * @param {integer} row The x-coordinate
	 * @param {integer} column The y-coordinate
	 * 
	 * @constructor
	 * @alias module:asset/js/model/position
	 * 
	 * @author Konstantin Schaper
	 */
	function Position(row, column) {
		
		/* ****************** */
		/* 	   Constructor	  */
		/* ****************** */
		
		row = parseInt(row);
		column = parseInt(column);
		
		// Verify row parameter
		if (row !== undefined) {
			// Verify input type
			if (!isInteger(row))
				throw "The parameter 'row' has to be of type 'integer'.";
		} else 
			throw "The paremeter 'row' has to be provided. Given: " + row;
		
		// Verify column parameter
		if (column !== undefined) {
			// Verify input type
			if (!isInteger(column))
				throw "The parameter 'column' has to be of type 'integer'.";
		} else
			throw "The paremeter 'column' has to be provided. Given: " + column;
		
		/* ****************** */
		/* Privileged methods */
		/* ****************** */
		
		/**
		 * @returns {number} The x coordinate of this location
		 */
		this.getRow = function() {
			return row;
		};
		
		/**
		 * @returns {number} The y coordinate of this location
		 */
		this.getColumn = function() {
			return column;
		};
		
		/**
		 * @param {Direction}
		 * @returns {Position} The position directly next to this one in the given direction.
		 */
		this.getAdjacent = function (direction) {
			direction = parseInt(direction);
			
			if (direction === Direction.UP) 
				return new Position(this.getRow() - 1, this.getColumn());
			else if (direction === Direction.RIGHT) 
				return new Position(this.getRow(), this.getColumn() + 1);
			else if (direction === Direction.DOWN) 
				return new Position(this.getRow() + 1, this.getColumn());
			else if (direction === Direction.LEFT) 
				return new Position(this.getRow(), this.getColumn() - 1);
			else 
				throw "The parameter 'direction' has to be a valid 'Direction'";
			
		};
		
		/**
		 * @param {Position} other The Position object to compare with
		 * @returns {boolean} Whether this position's values equal the ones of the provided one 
		 */
		this.equals = function(other) {
			// Verify other parameter
			if (other !== undefined) {
				// Verify input type
				if (other instanceof Position)
					return other.getRow() === row && other.getColumn() === column;
				else
					throw "The parameter 'other' has to be of type 'Position'.";
			} else 
				throw "The paremeter 'other' has to be provided. Given: " + other;
		};
		
		this.toJSON = function() {
			return  { 
						"row": row,
						"column": column
					};
		};
		
		this.toString = function () {
			return JSON.stringify(this);
		};
		
	};
	
	/**
	 * Convenience method for fromJSON.
	 * 
	 * @param {String} string
	 * @returns {Position}
	 */
	Position.fromString = function(string) {
		return Position.fromJSON(string);
	};
	
	/**
	 * @param {string} string The string to transform into a Position object
	 * @returns {Position} The immutable Position object de-serialized from the given string
	 */
	Position.fromJSON = function(string) {
		// Verify string parameter
		if (string !== undefined) {
			// Verify input type
			if (typeof(string) === "string") {
				var obj = JSON.parse(string);
				return new Position(obj['row'], obj['column']);
			} else
				throw "The parameter 'string' has to be of type 'String'.";
		} else 
			throw "The paremeter 'string' has to be provided. Given: " + string;
	};

	// Return class definition to dependency container
	return Position;
});