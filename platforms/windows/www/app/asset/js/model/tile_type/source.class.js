﻿/**
 * Constructor for the Source class, which inherits the Tile class.
 * 
 * @param {boolean} preplaced Defines whether the tile can be moved or rotated, immutable value
 * @param {Position} position Initial position of the tile
 * @param {Object} additionalValues Object containing additional input values
 * @param {Direction} [initialRotation] The initial rotation value for this tile
 * @author Konstantin Schaper
 * @deprecated Replaced by '.ttd' files
 */
function Source (preplaced, position, additionalValues, initialRotation) {
	
	/* ****************** */
	/* 	   Constructor	  */
	/* ****************** */
	
	// Verify color parameter
	if (additionalValues['color'] !== undefined) {
		// Verify input type
		if (!Color.verify(additionalValues['color']))
			throw "The parameter 'additionalValues['color']' has to be of type 'Color'.";
	} else 
		throw "The paremeter 'additionalValues['color']' has to be provided. Given: " + additionalValues['color'];
	
	// Define base openings
	var baseOpenings = {};
	baseOpenings[Direction.RIGHT] = additionalValues['color'];
	
	// Call super constructor
	Source.prototype.parent.constructor.call(this, preplaced, position, baseOpenings, additionalValues, initialRotation);
	
	/* ****************** */
	/* Privileged methods */
	/* ****************** */
	
	/**
	 * Returns the color of this source.
	 * 
	 * @returns {Color}
	 */
	this.getColor = function() {
		return additionalValues['color'];
	};
	
};

/* ************************ */
/*  Inheritance Definition  */
/* ************************ */

Source.prototype = Object.create(Tile.prototype);
Source.prototype.constructor = Source;
Source.prototype.parent = Tile.prototype;

Source.prototype.getTileTypeId = function () {
	return "Source";
};

Source.prototype.getBaseImageURL = function () {
	var color = this.getColor();
	
	if (color == Color.NONE)
		return "Source";
	else if (color == Color.ALPHA)
		return "Source_Alpha";
	else if (color == Color.BETA)
		return "Source_Beta";
	else
		return undefined;
};

Source.prototype.isSource = function () {
	return true;
};

/**
 * @see Tile.additionalValues
 */
Source.additionalValues = {
	"color": {
		type: "Color"
	}
};