﻿/**
 * Constructor for the Destination class, which inherits the Tile class.
 * 
 * @param {boolean} preplaced Defines whether the tile can be moved or rotated, immutable value
 * @param {Position} position Initial position of the tile
 * @param {Object} additionalValues Object containing additional input values
 * @param {Direction} [initialRotation] The initial rotation value for this tile
 * @author Konstantin Schaper
 * @deprecated Replaced by '.ttd' files
 */
function Destination (preplaced, position, additionalValues, initialRotation) {
	
	/* ****************** */
	/* 	   Constructor	  */
	/* ****************** */
	
	// Verify color parameter
	if (additionalValues['color'] !== undefined) {
		// Verify input type
		if (!Color.verify(additionalValues['color']))
			throw "The parameter 'color' has to be of type 'Color'.";
	} else 
		throw "The paremeter 'color' has to be provided. Given: " + additionalValues['color'];
	
	// Define base openings
	var baseOpenings = {};
	baseOpenings[Direction.LEFT] = additionalValues['color'];
	
	// Call super constructor
	Destination.prototype.parent.constructor.call(this, preplaced, position, baseOpenings, additionalValues, initialRotation);
	
	/* ****************** */
	/* Privileged methods */
	/* ****************** */
	
	/**
	 * Returns the color of this source.
	 * 
	 * @returns {Color}
	 */
	this.getColor = function() {
		return additionalValues['color'];
	};
};

/* ************************ */
/*  Inheritance Definition  */
/* ************************ */

Destination.prototype = Object.create(Tile.prototype);
Destination.prototype.constructor = Destination;
Destination.prototype.parent = Tile.prototype;

Destination.prototype.getTileTypeId = function () {
	return "Destination";
};

Destination.prototype.getBaseImageURL = function () {
	var color = this.getColor();
	
	if (color == Color.NONE)
		return "Destination";
	else if (color == Color.ALPHA)
		return "Destination_Alpha";
	else if (color == Color.BETA)
		return "Destination_Beta";
	else
		return undefined;
};

Destination.prototype.isDestination = function () {
	return true;
};

/**
 * @see Tile.additionalValues
 */
Destination.additionalValues = {
	"color": {
		type: "Color"
	}
};