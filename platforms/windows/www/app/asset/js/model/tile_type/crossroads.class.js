﻿/**
 * Constructor for the Crossroads class, which inherits the Tile class.
 * 
 * @param {boolean} preplaced Defines whether the tile can be moved or rotated, immutable value
 * @param {Position} position Initial position of the tile
 * @param {Direction} [initialRotation] The initial rotation value for this tile
 * @author Konstantin Schaper
 * @deprecated Replaced by '.ttd' files
 */
function Crossroads (preplaced, position, initialRotation) {
	// Define base openings
	var baseOpenings = {};
	baseOpenings[Direction.UP] = Color.NONE;
	baseOpenings[Direction.RIGHT] = Color.NONE;
	baseOpenings[Direction.DOWN] = Color.NONE;
	baseOpenings[Direction.LEFT] = Color.NONE;
	
	// Call super constructor
	Crossroads.prototype.parent.constructor.call(this, preplaced, position, baseOpenings, undefined, initialRotation);
};

/* ************************ */
/*  Inheritance Definition  */
/* ************************ */

Crossroads.prototype = Object.create(Tile.prototype);
Crossroads.prototype.constructor = Crossroads;
Crossroads.prototype.parent = Tile.prototype;

Crossroads.prototype.getTileTypeId = function () {
	return "Crossroads";
};

Crossroads.prototype.isRotatable = function () {
	return false;
};

/**
 * Crossroads are the only way to make streams cross without colliding and are therefore very powerful.
 */
Crossroads.prototype.getPointValue = function () {
	return 40;
};

/**
 * Gets all exits for the given entry direction.
 * 
 * @param {Direction} entryDirection
 * @returns {Direction[]} The exit directions for this entry
 * @see Tile.prototype.getOpenings
 */
Crossroads.prototype.getExits = function(entryDirection) {
	var exits = [];
	var baseEntryDirection = this.getBaseDirection(entryDirection);
	
	if (baseEntryDirection == Direction.UP) exits.push(this.getRotatedDirection(Direction.DOWN));
	if (baseEntryDirection == Direction.RIGHT) exits.push(this.getRotatedDirection(Direction.LEFT));
	if (baseEntryDirection == Direction.DOWN) exits.push(this.getRotatedDirection(Direction.UP));
	if (baseEntryDirection == Direction.LEFT) exits.push(this.getRotatedDirection(Direction.RIGHT));
	
	return exits;
};