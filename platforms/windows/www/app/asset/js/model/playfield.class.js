﻿/**
 * @module asset/js/model/playfield
 */
define (["model/tile.class", "model/position.class", "model/direction.class", "model/color.class", "model/stream.class"],
		function (Tile, Position, Direction, Color, Stream) {
	
	/**
	 * Class that represent the square-grid of tiles that forms the playfield.
	 * 
	 * @param {integer} rowCount The number of rows in the square-grid
	 * @param {integer} columnCount The number of columns in the square-grid
	 * 
	 * @constructor
	 * @alias module:asset/js/model/playfield
	 * 
	 * @author Konstantin Schaper
	 */
	function Playfield (rowCount, columnCount) {
		
		/* ****************** */
		/* 	   Constructor	  */
		/* ****************** */
		
		// Verify rowCount parameter
		if (rowCount !== undefined) {
			// Verify input type
			if (!isInteger(rowCount))
				throw "The parameter 'rowCount' has to be of type 'integer'.";
		} else 
			throw "The paremeter 'rowCount' has to be provided. Given: " + rowCount;
		
		// Verify columnCount parameter
		if (columnCount !== undefined) {
			// Verify input type
			if (!isInteger(columnCount))
				throw "The parameter 'columnCount' has to be of type 'integer'.";
		} else 
			throw "The paremeter 'columnCount' has to be provided. Given: " + columnCount;
		
		/* ****************** */
		/*   Private fields   */
		/* ****************** */
		
		/**
		 * @type {Tile[]}
		 */
		var tiles = {};
		
		/* ****************** */
		/*  Private methods   */
		/* ****************** */
		
		/**
		 * Internal method that is called whenever the number of rows or columns changes,
		 * allows event listeners to be called or internal state changes to occur.
		 * If the size of the playfield is reduced, all tiles that are out of the new boundaries
		 * are deleted.
		 */
		function updatedDimensionCallback() {
			// Gather all tiles outside dimensions
			var toDelete = {};
			for (var pos in tiles) {
				var tile = tiles[pos];
				
				if (typeof tile === 'undefined') continue;
				
				var tilePosition = tile.getPosition();
				// Check if the tile position matches its position in the tiles object
				if (!tilePosition.equals(Position.fromString(pos)))
					throw "The values for 'tilePosition' and 'pos' are not equal.";
				// Check if the tile position is out of dimensions
				if (tilePosition.getRow() >= rowCount || tilePosition.getColumn() >= columnCount) {
					toDelete[tilePosition] = tile;
				};
			};
			
			// Remove all tiles that are outside dimensions
			for (var pos in toDelete) {
				this.removeTile(pos);
			}
		};
		
		/* ****************** */
		/* Privileged methods */
		/* ****************** */
		
		/**
		 * Returns the tile on the specified position or null, either if
		 * there is no tile at the given location or if the given position is out
		 * of the playfield's bounds.
		 * 
		 * @param {Position} position The position to get the tile for
		 * @returns {Tile|undefined} 
		 */
		this.getTile = function(position) {
			// Verify position parameter
			if (position !== undefined) {
				// Verify input type
				if (position instanceof Position) {
					// Verify definition and bounds
					return tiles[position.toString()];
				} else {
					throw "The parameter 'position' has to be of type 'String' or 'Position'.";
				}
			} else 
				return undefined;
		};
		
		/**
		 * Returns the tile at the given position adding the offset of the given direction.
		 * 
		 * @param {Position} position
		 * @param {Direction} direction
		 * @returns {Tile|undefined}
		 */
		this.getAdjacentTile = function(position, direction) {
			return this.getTile(position.getAdjacent(direction));
		};
		
		/**
		 * Attempts to place the given tile on the play field on the position specified within the tile object.
		 * 
		 * @param {Tile} tile The tile to place on the play field
		 * @throws {Error} If the input is invalid
		 */
		this.placeTile = function (tile) {
			// Verify tile parameter
			if (tile !== undefined) {
				// Verify input type
				if (tile instanceof Tile) {
					tiles[tile.getPosition().toString()] = tile;
				} else {
					throw new Error("The parameter 'tile' has to be of type 'Tile'.");
				}
			} else 
				throw new Error("The paremeter 'tile' has to be provided. Given: " + tile);
		};
		
		/**
		 * Removes the tile at the specified position.
		 * 
		 * @param {Position|String} position
		 * @throws {Error} If the input value is undefined or is of incorrect type
		 */
		this.removeTile = function (position) {
			
			// Verify position parameter
			if (position !== undefined) {
				if (position instanceof Position) position = position.toString();
				if (typeof position !== "string") throw new Error("The parameter 'position' has to be either of type 'Position' or 'String'.");
				delete tiles[position];
			} else 
				throw new Error("The paremeter 'position' has to be provided. Given: " + position);
		};
		
		/**
		 * Swaps the tiles at the specified positions, unless
		 * either of them is not movable.
		 * 
		 * @param {Position} from The position of the tile to move
		 * @param {Position} to The position to move the tile to
		 * @throws {Error} If one of the input values is undefined or is of incorrect type
		 */
		this.moveTile = function (from, to) {
			
			// Verify from parameter
			if (from !== undefined) {
				// Verify input type
				if (!(from instanceof Position)) {
					throw "The parameter 'from' has to be of type 'Position'.";
				}
			} else 
				throw "The paremeter 'from' has to be provided. Given: " + from;
			
			// Verify to parameter
			if (to !== undefined) {
				// Verify input type
				if (!(to instanceof Position)) {
					throw "The parameter 'to' has to be of type 'Position'.";
				}
			} else 
				throw "The paremeter 'to' has to be provided. Given: " + to;
			
			var tileFrom = tiles[from.toString()];
			var tileTo = tiles[to.toString()];
			
			if (!tileFrom.isMovable() || !(tileTo == null || tileTo.isMovable())) return;
			
			if (tileFrom !== undefined) tileFrom.setPosition(to);
			if (tileTo !== undefined) tileTo.setPosition(from);
			tiles[to.toString()] = tileFrom;
			
			if (tileTo !== undefined)
				tiles[from.toString()] = tileTo;
			else
				delete tiles[from.toString()];
		};
		
		/**
		 * @returns {number} The number of rows in the square-grid
		 */
		this.getRowCount = function() {
			return rowCount;
		};
		
		/**
		 * Overrides the rowCount with the given value.
		 * 
		 * @param {integer} newRowCount
		 * @throws {Error} If the input value is undefined or is of incorrect type
		 */
		this.setRowCount = function(newRowCount) {
			// Verify newRowCount parameter
			if (newRowCount !== undefined) {
				// Verify input type
				if (!isInteger(newRowCount))
					throw "The parameter 'newRowCount' has to be of type 'integer'.";
				else {
					rowCount = newRowCount;
					updatedDimensionCallback();
				}
			} else 
				throw "The paremeter 'newRowCount' has to be provided. Given: " + newRowCount;
		};
		
		/**
		 * @returns {number} The number of columns in the square-grid
		 */
		this.getColumnCount = function() {
			return columnCount;
		};
		
		/**
		 * Overrides the columnCount with the given value.
		 * 
		 * @param {integer} newColumnCount
		 * @throws {Error} If the input value is undefined or is of incorrect type
		 */
		this.setColumnCount = function(newColumnCount) {
			// Verify newColumnCount parameter
			if (newColumnCount !== undefined) {
				// Verify input type
				if (!isInteger(newColumnCount))
					throw "The parameter 'newColumnCount' has to be of type 'integer'.";
				else {
					columnCount = newColumnCount;
					updatedDimensionCallback();
				}
			} else 
				throw "The paremeter 'newColumnCount' has to be provided. Given: " + newColumnCount;
		};
		
		/**
		 * Overrides both the rowCount and the columnCount with the given values.
		 * 
		 * @param {integer} newRowCount
		 * @param {integer} newColumnCount
		 * @throws {Error} If one of the input values is undefined or is of incorrect type
		 */
		this.setDimensions = function(newRowCount, newColumnCount) {
			// Verify newRowCount parameter
			if (newRowCount === undefined) {
				throw "The paremeter 'newRowCount' has to be provided. Given: " + newRowCount;
			} else if (!isInteger(newRowCount)) {
				throw "The parameter 'newRowCount' has to be of type 'integer'.";
			}
				
			// Verify newColumnCount parameter
			if (newColumnCount === undefined) {
				throw "The paremeter 'newColumnCount' has to be provided. Given: " + newColumnCount;
			} else if (!isInteger(newColumnCount)) {
				throw "The parameter 'newColumnCount' has to be of type 'integer'.";
			}
				
			rowCount = newRowCount;
			columnCount = newColumnCount;
			updatedDimensionCallback.call(this);
		};
		
		/**
		 * Creates a clone of this playfield and all tiles placed on it,
		 * marking all tiles of the clone as preplaced and therefore
		 * immovable and unrotatable.
		 */
		this.createPlayModeClone = function () {
			var playfieldClone = new Playfield(rowCount, columnCount);
			for (var pos in tiles) {
				var originalTile = tiles[pos];
				var tileClone = new originalTile.constructor(true, Position.fromString(pos), originalTile.getAdditionalValues(), originalTile.getRotation());
				playfieldClone.placeTile(tileClone);
			}
			return playfieldClone;
		};
		
		/**
		 * @returns {Tile[]} An array of all tiles that are marked as source tiles.
		 */
		this.getSources = function () {
			var sources = [];
			
			for (var pos in tiles) {
				var tile = tiles[pos];
				if (tile.isSource()) sources.push(tile);
			};
			
			return sources;
		};
		
		/**
		 * @returns {Tile[]} An array of all tiles that are marked as destination tiles.
		 */
		this.getDestinations = function () {
			var destinations = [];
			
			for (var pos in tiles) {
				var tile = tiles[pos];
				if (tile.isDestination()) destinations.push(tile);
			};
			
			return destinations;
		};
		
		/**
		 * Tests if this playfield is error free:
		 * 0. Sources generate streams out of each opening in the color they are filled with.
		 * 1. No stream must be leaking onto an empty tile
		 * 2. No stream must be leaking onto a tile that does not have an opening into that direction
		 * 3. Streams of different colors must not collide
		 * 4. All destinations have to be reached
		 * 5. There has to be at least one source and one destination
		 * 6. The tile code must not throw an exception
		 * 7. No tile event handler must signal an error by returning 'undefined'
		 * 
		 * @returns {object[]} An array containing all errors that occurred during verification
		 */
		this.verify = function () {
			var sources = this.getSources();
			var destinations = this.getDestinations();
			var subErrors = null;
			var errors = [];
			
			// 5: Each level needs at least one source and one destination
			if (sources.length == 0 || destinations.length == 0) {
				errors.push({
					"error": 5
				});
			}
			
			sources.forEach(function (source, index, array) {
				for (var baseExitDirection in source.getBaseOpenings()) {
					var rotatedExitDirection = source.getRotatedDirection(baseExitDirection);
					var entryDirection = Direction.negate(rotatedExitDirection);
					var color = source.getOpening(baseExitDirection);
					subErrors = this.evaluateTile(source.getPosition().getAdjacent(rotatedExitDirection), entryDirection, color, destinations);
					if (subErrors != null) errors = errors.concat(subErrors);
				};
			}, this);
			
			// 4: Not all destinations reached
			if (errors.length == 0 && destinations.length != 0) {
				destinations.forEach(function (destinationTile, index, array) {
					errors.push({
						"error": 4,
						"position": destinationTile.getPosition()
					});
				}, this);
			}
			
			return errors;
		};
		
		/**
		 * Recursively evaluates the tile at the given position from the given entryDirection with the given
		 * initial stream color and either returns an array of all occurred errors on failure or null on success.
		 * 
		 * @param {Position} position
		 * @param {Direction} entryDirection
		 * @param {Color} color L
		 * @param {Tile[]} destinations
		 * @returns {Array|null} An array containing all occurred error objects or null on success
		 */
		this.evaluateTile = function (position, entryDirection, color, destinations) {
			var tile = this.getTile(position);
			var stream = new Stream(color, position, tile, entryDirection);
			var eventHandlerResult;
			
			try {
			
				// 1: Leaking onto an empty tile
				if (typeof tile === "undefined")
					return [{
						"error": 1,
						"position": position
					}];
					
				// Fire preEntry event
				eventHandlerResult = tile.preEntry(stream);
				if (typeof eventHandlerResult !== "undefined" && !eventHandlerResult) {
					// 7: An event handler signaled an error
					return [{
						"error": 7,
						"tile": tile
					}];
				}
					
				// Get entry opening
				var entryColor = tile.getRotatedOpening(stream.getNextEntryDirection());
				var hasEntryInDirection = Color.verify(entryColor);
				
				// 2: Leaking a liquid onto an adjacent tile with no opening in this direction
				if (!hasEntryInDirection)
					return [{
						"error": 2,
						"tile": tile
					}];
					
				var isFilled = entryColor !== Color.NONE;
				if (isFilled) {
					// 3: Two streams of different colours collide or destination has different color requirements
					if (entryColor !== stream.getColor()) {
						return [{
							"error": 3,
							"tile": tile
						}];
					} else {
						// Check for destination
						if (tile.isDestination()) {
							destinations.remove(tile);
						}
						// Streams combined and/or destination reached, no error and stream is terminated
						return null;
					}
				} else {
					// Fill the entry
					tile.fillOpening(stream.getNextEntryDirection(), stream.getColor());
				}
				
				// Fire postEntry event
				eventHandlerResult = tile.postEntry(stream);
				if (typeof eventHandlerResult !== "undefined" && !eventHandlerResult) {
					// 7: An event handler signaled an error
					return [{
						"error": 7,
						"tile": tile
					}];
				}
				
				// Get exit openings
				var exits = tile.getExits(stream.getNextEntryDirection());
				
				// Iterate over exits and evaluate adjacent tiles
				var subResult = null;
				var errors = [];
				exits.forEach(function (exitDirection, index, array) {
					
					// Fill the exit
					tile.fillOpening(exitDirection, stream.getColor());
					
					// Fire preExit event
					eventHandlerResult = tile.preExit(exitDirection, stream);
					if (typeof eventHandlerResult !== "undefined" && !eventHandlerResult) {
						// 7: An event handler signaled an error
						return [{
							"error": 7,
							"tile": tile
						}];
					}
					
					// Move the stream cursor to the next tile
					stream.setNextEntryDirection(Direction.negate(exitDirection));
					stream.setNextTilePosition(tile.getPosition().getAdjacent(exitDirection));
					
					// Fire postExit event
					eventHandlerResult = tile.postExit(exitDirection, stream);
					if (typeof eventHandlerResult !== "undefined" && !eventHandlerResult) {
						// 7: An event handler signaled an error
						return [{
							"error": 7,
							"tile": tile
						}];
					}
					
					// Recursive evaluation call
					subResult = this.evaluateTile(stream.getNextTilePosition(), stream.getNextEntryDirection(), stream.getColor(), destinations);
					
					// Add error to list
					if (subResult != null) errors = errors.concat(subResult);
				}, this);
				
				// Return the result
				return errors.length > 0 ? errors : null;
				
			} catch (exception) {
				console.error("The parsed code of a tile threw an exception:");
				console.dir(exception);
				
				// 6: The internal tile code threw an exception
				return [{
					"error": 6,
					"tile": tile
				}];
			}
			
		};
		
		/**
		 * @returns {number} Returns the sum of all tile values, only taking tiles into account that are not pre-placed.
		 */
		this.getPlacedTilesValue = function () {
			var result = 0;
			
			for (var pos in tiles) {
				var tile = tiles[pos];
				if (!tile.isPreplaced())
					result += tile.getPointValue();
			};
			
			return result;
		};
		
		/**
		 * Resets all tiles.
		 * @see Tile.reset
		 */
		this.reset = function () {
			for (var pos in tiles) {
				tiles[pos].reset();
			};
		};
		
		/**
		 * Returns the part of this object to be serialized via JSON.stringify.
		 * 
		 * @returns {Object}
		 */
		this.toJSON = function() {
			return {
				rowCount: this.getRowCount(),
				columnCount: this.getColumnCount(),
				"tiles": tiles
			};
		};
		
	};
	
	/**
	 * Deserializes the given json object to a playfield object.
	 * 
	 * @param {Object} tileTypes Object containing all the prototypes for the tiletypes
	 * @param {Object} jsonObject
	 * @returns {Playfield}
	 */
	Playfield.fromJSON = function(tileTypes, jsonObject) {
		// Create the initial empty playfield object
		var playfieldObject = new Playfield(jsonObject.rowCount, jsonObject.columnCount);
	
		// Go through all serialized tile values
		for (var posString in jsonObject.tiles) {
			// Deserialize the json tile definition
			var tileObject = jsonObject.tiles[posString];
			var pos = Position.fromJSON(posString);
			var newTile = new tileTypes[tileObject.tileTypeId.toLowerCase()](false, pos, tileObject.additionalValues, tileObject.rotation);
			
			// Place the deserialized tile onto the playfield
			playfieldObject.placeTile(newTile);
		}
		
		return playfieldObject;
	};

	// Return class definition to dependency container
	return Playfield;
});