﻿/**
 * @module asset/js/model/level
 */
define(["jquery", "model/playfield.class"], function ($, Playfield) {

	/**
	 * Class which holds information about a specific level.
	 * 
	 * @param {string} title The levels name
	 * @param {Playfield} [playfield] The initial playfield data
	 * @param {object} [baseToolAvailabilityCounts]
	 * @param {string} tileTypesLocationURL (Immutable)
	 * @param {string} tileTypesImageLocationURL (Immutable)
	 * 
	 * @constructor
	 * @alias module:asset/js/model/level
	 * 
	 * @author Konstantin Schaper
	 */
	function Level (title, playfield, baseToolAvailabilityCounts, tileTypesLocationURL, tileTypesImageLocationURL) {
		
		// Verify title parameter
		if (title !== undefined) {
			// Verify input type
			if (typeof(title) !== "string")
				throw new Error("The parameter 'title' has to be of type 'String', given: " + typeof(title));
			else if (title.length === 0)
				throw new Error ("The parameter 'title' has to be at least one character long.");
		} else 
			throw new Error("The paremeter 'title' has to be provided.");
		
		/* ****************** */
		/*   Private Members  */
		/* ****************** */
		
		// If the baseToolAvailabilityCounts has not been provided, create an empty map object 
		if(typeof baseToolAvailabilityCounts === 'undefined') baseToolAvailabilityCounts = {};
		else if (typeof baseToolAvailabilityCounts !== 'object') 
			throw new Error("The parameter baseToolAvailabilityCounts has to be either undefined or of type 'object', given: " 
					+ typeof(baseToolAvailabilityCounts));
		
		// If the playfield parameter is not provided, create a minimally large playfield
		if(!(playfield instanceof Playfield)) playfield = new Playfield(3,3);
		
		/**
		 * Modifiable in-memory copy of the baseToolAvailabilityCounts object.
		 * 
		 * @type {Object}
		 */
		var currentToolAvailabilityCounts = $.extend({}, baseToolAvailabilityCounts);
		
		/**
		 * Contains all prototypes for all tile types. Has to be initialized by
		 * {@link Level.loadToolTileTypes}.<br>
		 * <br>
		 * The returned object has the following layout:<br>
		 * {string} tileTypeId => {function} tileTypeClass
		 * 
		 * @type {Object}
		 */
		var tileTypes = {};
		
		/**
		 * A map containing the base64 encoded image strings
		 * for all non-default tile types in this level.<br>
		 * <br>
		 * The returned object has the following layout:<br>
		 * {string} fileUrl => {string} base64EncodedImageString
		 * 
		 * @type {Object}
		 */
		var tileTypeImageCache = {};
		
		/* ****************** */
		/* Privileged methods */
		/* ****************** */
		
		/**
		 * @returns {string} The current title of this level
		 */
		this.getTitle = function() {
			return title;
		};
		
		/**
		 * @param {string} newTitle The new value for the title attribute
		 */
		this.setTitle = function(newTitle) {
			title = newTitle;
		};
		
		/**
		 * @returns {Playfield} The active playfield model
		 */
		this.getPlayfield = function() {
			return playfield;
		};
		
		/**
		 * @param {Playfield} newPlayfield The new value for the playfield attribute
		 */
		this.setPlayfield = function(newPlayfield) {
			playfield = newPlayfield;
		};
		
		/**
		 * @returns {string} The url which points to the folder where the scripts of the tile types of this level are stored
		 */
		this.getTileTypesLocationURL = function() {
			return tileTypesLocationURL;
		};
		
		/**
		 * @returns {string} The url which points to the folder where the images of the tile types of this level are stored
		 */
		this.getTileTypesImageLocationURL = function() {
			return tileTypesImageLocationURL;
		};
		
		/**
		 * Returns the number of tiles of the specific type that are still available to be placed,
		 * returns undefined if the specific tile type is disabled.
		 * 
		 * @param {string} tileTypeId
		 * @returns {number|undefined}
		 */
		this.getToolAvailabilityCount = function (tileTypeId) {
			return currentToolAvailabilityCounts[tileTypeId];
		};
		
		/**
		 * Returns the number of tiles of the specific type that are available to be placed at the start of the level,
		 * returns undefined if the specific tile type is disabled.
		 * 
		 * @param {string} tileTypeId
		 * @returns {number|undefined}
		 */
		this.getBaseToolAvailabilityCount = function (tileTypeId) {
			return baseToolAvailabilityCounts[tileTypeId];
		};
		
		/**
		 * @returns {string[]} An array containing all defined tool types
		 */
		this.getTools = function () {
			return Object.keys(baseToolAvailabilityCounts);
		};
		
		/**
		 * @returns {Object} A map containing the prototypes for all loaded tile types for this level.
		 */
		this.getTileTypes = function () {
			return tileTypes;
		};
		
		/**
		 * Overrides the map of loaded tile types with the provided one.<br>
		 * <i>WARNING:</i> Internal method so use with caution.
		 * 
		 * @param {object} newTileTypes
		 */
		this.setTileTypes = function (newTileTypes) {
			tileTypes = newTileTypes;
		};
		
		/**
		 * Defines the number of tiles of the specific type that the user can still place.
		 * If the second argument is 'undefined' then the entry will be removed and the tile type
		 * becomes completely unavailable.
		 * 
		 * @param {String} tileTypeId
		 * @param {Number|undefined} count
		 */
		this.setBaseToolAvailabilityCount = function (tileTypeId, count) {
			if (typeof count === 'undefined')
				delete baseToolAvailabilityCounts[tileTypeId];
			else if (isInteger(count))
				baseToolAvailabilityCounts[tileTypeId] = count;
		};
		
		/**
		 * Defines the number of tiles of the specific type that the user can place from the start of a level.
		 * If the second argument is 'undefined' then the entry will be removed and the tile type
		 * becomes completely unavailable.
		 * 
		 * @param {String} tileTypeId
		 * @param {Number|undefined} count
		 */
		this.setToolAvailabilityCount = function (tileTypeId, count) {
			if (typeof count === 'undefined')
				delete currentToolAvailabilityCounts[tileTypeId];
			else if (isInteger(count))
				currentToolAvailabilityCounts[tileTypeId] = count;
		};
		
		/**
		 * @param {object} defaultTileTypes A map containing all default tile types (tileTypeId => Tile-class pairs)
		 * @returns {number} The amount of points if all tiles available in this level were placed.
		 */
		this.getBaseToolsPointSum = function (defaultTileTypes) {
			var result = 0;
			var tileTypes = Object.assign({}, defaultTileTypes, this.getTileTypes());
			for (var toolId in baseToolAvailabilityCounts) {
				var toolAvailCount = baseToolAvailabilityCounts[toolId];
				var toolClass = tileTypes[toolId.toLowerCase()].prototype;
				var toolValue = toolClass.getPointValue();
				var value = toolValue * toolAvailCount;
				result += value;
			}
			return result;
		};
		
		/**
		 * Loads all scripts for all tile types contained in this level,
		 * excluding the default tile types defined by the application configuration.
		 * 
		 * @param {Application} application The application context to load the tile types through
		 * @returns {Deferred[]} An array of deferred objects which are loading the tile types asynchronously
		 */
		this.loadToolTileTypes = function(application) {
			// Remove default tile type ids from tools to load (They are loaded separately from a different location
			var tileTypeIdsToLoad = this.getNonDefaultTileTypeIds(application);
			
			// Define the initializer function which is executed every time a new tile type has been successfully loaded
			var tileTypeInitializer = (this.addTileType).bind(this);
			
			// Load all tile types in parallel
			return application.loadTileTypes(tileTypeIdsToLoad, this.getTileTypesLocationURL(), tileTypeInitializer);
		};
		
		/**
		 * Filters all the defined tools in this level by excluding default tile types.
		 * 
		 * @param {Application} application The application context which defines the default tile type ids
		 * @returns {string[]} The filtered array of tool id strings
		 */
		this.getNonDefaultTileTypeIds = function (application) {
			// Get default tile type ids
			var defaultTileTypeIds = application.getDefaultTileTypeIds();
			
			// Remove default tile type ids from tools to load (They are loaded separately from a different location
			var nonDefaultTileTypeIds = this.getTools().filter(function (item) {
				return defaultTileTypeIds.indexOf(item) === -1;
			});
			
			// Return the filtered array
			return nonDefaultTileTypeIds;
		};
		
		/**
		 * Registers the given Tile class with this level.
		 * 
		 * @param {Function} tileTypeClass The Tile sub-class to register
		 * @param {String} [tileTypeId] Optional way to define the key for the tile type created
		 */
		this.addTileType = function (tileTypeClass, tileTypeId) {
			this.getTileTypes()[typeof tileTypeId !== "undefined" ? tileTypeId.toLowerCase() : tileTypeClass.name.toLowerCase()] = tileTypeClass;
		};
		
		/**
		 * Clones this level and the corresponding playfield,
		 * marking all cloned tiles as preplaced, making them
		 * immovable and unrotatable.
		 */
		this.createPlayModeClone = function () {
			var playfieldClone = playfield.createPlayModeClone();
			var toolAvailabilityCountsClone = $.extend({}, baseToolAvailabilityCounts);
			var levelClone = new Level(title, playfieldClone, toolAvailabilityCountsClone, tileTypesLocationURL, tileTypesImageLocationURL);
			levelClone.setTileTypes(Object.assign({}, this.getTileTypes()));
			levelClone.setTileTypeImageCache(Object.assign({}, this.getTileTypeImageCache()));
			
			return levelClone;
		};
		
		/**
		 * Clears the complete base64 image cache.
		 */
		this.clearTileTypeImageCache = function () {
			for (var property in tileTypeImageCache) delete tileTypeImageCache[property];
		};
		
		/**
		 * @param {string} fileName The name of the image file to cache
		 * @param {string} imageString Base64 encoded image string
		 */
		this.cacheTileTypeImage = function (fileName, imageString) {
			tileTypeImageCache[fileName] = imageString;
		};
		
		/**
		 * @param {string} tileTypeId The non-default tile type id to get the image for
		 * @returns {string|undefined} The base64 encoded image string or undefined, if no cached image for that tile type exists.
		 */
		this.getCachedTileTypeImage = function (tileTypeId) {
			return tileTypeImageCache[tileTypeId];
		};
		
		/**
		 * @returns {Object} A map containing all image files for this level
		 */
		this.getTileTypeImageCache = function () {
			return tileTypeImageCache;
		};
		
		/**
		 * Overrides the current image cache with the provided one.<br>
		 * <i>WARNING:</i> Internal method so use with caution.
		 * 
		 * @param {object} newTileTypeImageCache A map containing all image files with their names and the corresponding base64 encoded image string
		 */
		this.setTileTypeImageCache = function (newTileTypeImageCache) {
			tileTypeImageCache = newTileTypeImageCache;
		};
		
		/**
		 * Returns the part of this object to be serialized via JSON.stringify.
		 * 
		 * @returns {Object}
		 */
		this.toJSON = function() {
			return {
				title: this.getTitle(),
				tileTypesLocationURL: this.getTileTypesLocationURL(),
				tileTypesImageLocationURL: this.getTileTypesImageLocationURL(),
				toolAvailability: baseToolAvailabilityCounts,
				playfield: this.getPlayfield()
			};
		};
		
		/**
		 * Resets the level to its base state.
		 */
		this.reset = function () {
			this.getPlayfield().reset();
		};
		
	};
	
	/**
	 * Representing a partially loaded level,
	 * which still misses the loading process of the 
	 * associated playfield.
	 * 
	 * @typedef {Object} LevelLoadingResult
	 * @property {Level} level The level that is being loaded
	 * @property {object} playfield The json object representing the playfield
	 */
	
	/**
	 * Deserializes the given json string to a level object.
	 * This does include the playfield object in the result,
	 * but does not itself deserialize the playfield (The playfield
	 * for the returned level will be initially 'undefined').
	 * 
	 * @param {Application} application The application context
	 * @param {String} json The level json string
	 * @returns {LevelLoadingResult} The result object
	 */
	Level.fromJSON = function(application, json) {
		// Parse the json string
		var jsonObject = JSON.parse(json);
		
		// Instantiate the level object and return it
		var level = new Level(jsonObject.title, undefined, jsonObject.toolAvailability,
				jsonObject.tileTypesLocationURL, jsonObject.tileTypesImageLocationURL);
		
		// Return the LevelLoadingResult
		return {
			"level": level,
			"playfield": jsonObject.playfield
		};
	};
	
	// Return class definition to dependency container
	return Level;
	
});