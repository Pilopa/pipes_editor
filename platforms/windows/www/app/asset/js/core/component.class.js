﻿/**
 * @module asset/js/core/component
 */
define (["jquery", "jquerymobile"], function ($) {

	/**
	 * Base class for all custom view components.
	 * All dependencies should be declared by the constructor parameters.
	 * The final view creation is performed by the <code>injectInto</code> method.
	 * 
	 * @constructor
	 * @alias module:asset/js/core/component
	 * @author Konstantin Schaper
	 */
	function Component () {
		
		/* ****************** */
		/* 	 Private Members  */
		/* ****************** */
		
		/**
		 * JQuery object representing this component instance
		 * 
		 * @type {JQuery}
		 */
		var view = undefined;
		
		/**
		 * Loads the CSS for this component if present
		 */
		var loadStyleSheet = function () {
			var styleUrl = this.getStyleURL();
			if (typeof styleUrl !== 'undefined') $("head").append('<link rel="stylesheet" href="' + styleUrl + '" />');
		};
		
		/**
		 * Asynchronously loads the HTML view of this component and
		 * returns the deferred object.
		 * 
		 * @returns {Deferred}
		 */
		var loadView = function () {
			return $.get(this.getViewURL());
		};
		
		/* ****************** */
		/* Privileged methods */
		/* ****************** */
		
		/**
		 * Asynchronously creates and injects this component's view into the given target and activates it.
		 * The component is not going to be visible until this method is called and it's promise resolved.
		 * 
		 * @param {String|JQuery} target The target to inject into
		 * @param (Optional) {Boolean} onlyAppend If true, the targets content is not replaced
		 * @param {Promise} The promise object for the asynchronous action
		 */
		this.injectInto = function (target, onlyAppend) {
			var replace = onlyAppend !== true;
			
			// Create the promise for the asynchronous action and return it
			return Promise.resolve(loadView.apply(this)).then((function (data) {
					
					// Load style sheet
					loadStyleSheet.apply(this);
					
					// Define the view variable with retrieved html content
					view = $(data);
					
					// Get the injection target
					var jqueryTarget = $(target);
					
					// If not defined otherwise, replace the content of the given selector
					if (replace) jqueryTarget.empty();
					
					// Append the view and enhance it with JQuery
					jqueryTarget.append(view).enhanceWithin();
					
					// Bind events
					this.bindEvents();
					
					// Perform component-specific initialization
					this.initialize();
					
					// Resolve the promise by returning the injected object
					return this;
					
				}).bind(this));
		};
		
		/**
		 * Remember: The view is not present before the Component.injectInto has been called.
		 * 
		 * @returns {JQuery} The JQuery object representing this component instance
		 */
		this.getView = function () {
			return view;
		};
		
	};
	
	/* ****************** */
	/*   Static Methods   */
	/* ****************** */
	
	/**
	 * Injects the provided components in order and waits for the first to be initialized to inject the next one.
	 * 
	 * @param {Component[]} components Array of components to inject sequentially
	 * @param {String[]|JQuery[]|String|JQuery} targets Array of targets of the components or a single argument that is applied to all components
	 * @param {boolean[]|boolean} appendOnly Array of appendOnly values for the components or a single argument that is applied to all components
	 */
	Component.injectAll = function (components, targets, appendOnly) {
		var index = 0;
		
		// Initialize the promise chain with initial values
		var chain = Promise.resolve(
			{
				"components": components,
				"targets": targets,
				"appendOnly": appendOnly,
				"index": index
			}
		);
		
		// Create the chain callback
		var injectNext = function (params) {
			// Read params
			var components = params.components;
			var targets = params.targets;
			var appendOnlyValues = params.appendOnly;
			var index = params.index++;
			
			// Determine component to inject next
			var component = components[index];
			var target = typeof targets === "object" ? targets[index] : targets;
			var appendOnly = typeof appendOnlyValues === "array" ? appendOnlyValues[index] : appendOnlyValues;
			
			console.info("Component.injectAll.injectNext: " + index);
			
			// Inject component and return result
			return component
			.injectInto(target, appendOnly)
			.then (function (component) {
				return params; // Return input with incremented index to next element in chain
			});
		};
		
		// Construct the chain
		while (index++ < components.length) {
			console.info("Adding to chain: " + (index - 1));
			chain = chain.then(injectNext);
		}
			
		// Return the chain
		console.info("Returning chain");
		return chain;
	};
	
	/* ****************** */
	/*   Public Methods   */
	/* ****************** */
	
	/**
	 * Abstract method which returns the URL string
	 * to the HTML view of this component type.
	 */
	Component.prototype.getViewURL = function() {
		throw "Abstract method 'getViewURL' is not implemented";
	};
	
	/**
	 * Abstract method which returns the URL string
	 * to the CSS of this component type. 
	 * Default implementation does omit styling.
	 */
	Component.prototype.getStyleURL = function() {
		return undefined;
	};
	
	/**
	 * Binds all necessary events for this object to function.
	 * The view variable is available at the point this function is internally called.
	 * Should be overridden by all inheriting classes.
	 */
	Component.prototype.bindEvents = function() {
		// Do nothing
	};
	
	/**
	 * Called internally after this component has been injected into its target.
	 * Should be overridden by all inheriting classes to perform extended initialization.
	 */
	Component.prototype.initialize = function () {
		// Do nothing
	};

	// Return class definition to dependency container
	return Component;
});