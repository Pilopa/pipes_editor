﻿/**
 * @module asset/js/core/dialog_component
 */
define (["jquery", "core/component.class"], function ($, Component) {

	/**
	 * Inherits from the Component class and represents a popup with content.
	 * The html view for components of this type should have a div wrapper as root with class
	 * "dialog" and the data-role "popup" defined.
	 * 
	 * @constructor
	 * @alias module:asset/js/core/dialog_component
	 * 
	 * @author Konstantin Schaper
	 * @see http://api.jquerymobile.com/popup/
	 */
	function DialogComponent() {
		
		// Super constructor call
		DialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * The dialog which opened this dialog.
		 * 
		 * @type {DialogComponent}
		 */
		this.previous = undefined;
		
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	DialogComponent.prototype = Object.create(Component.prototype);
	DialogComponent.prototype.constructor = DialogComponent;
	DialogComponent.prototype.parent = Component.prototype;
	
	/* ****************** */
	/*    Static Fields   */
	/* ****************** */
	
	/**
	 * Defines how long the system waits until the previous dialog should be opened,
	 * this is to prevent an inconvenient behaviour of JQuery mobile regarding the closing 
	 * and opening of dialogs.
	 * 
	 * @type {number}
	 */
	DialogComponent.backTimeout = 25; //ms
	
	/* ****************** */
	/*   Public Methods   */
	/* ****************** */
	
	/**
	 * Opens this dialog.
	 */
	DialogComponent.prototype.open = function () {
		this.getView().popup("open");
	};
	
	/**
	 * Closes this dialog.
	 */
	DialogComponent.prototype.close = function () {
		this.getView().popup("close");
	};
	
	/**
	 * Closes this dialog and opens the previously displayed dialog, if present.
	 */
	DialogComponent.prototype.back = function () {
		this.close();
		if (this.previous) {
			setTimeout((function () {
				this.previous.open();
				this.previous = undefined;
			}).bind(this), DialogComponent.backTimeout);
		}
	};
	
	/**
	 * Switches to this dialog from another one.
	 * 
	 * @param {DialogComponent} fromDialogComponent The dialog which initiated the switching process
	 */
	DialogComponent.prototype.switchFrom = function(fromDialogComponent) {
		this.previous = fromDialogComponent;
		this.addBackButton();
		fromDialogComponent.getView().one("popupafterclose", (this.open).bind(this)).popup("close");
	};
	
	/**
	 * Adds a back button to the jquery mobile header (data-role='header') element of this dialog.
	 * Does nothing if a back button is already present.
	 */
	DialogComponent.prototype.addBackButton = function () {
		if (!this.getView().find("button.back").length) {
			// Create the button element
			var button = $('<button class="back ui-btn-left" data-theme="a" data-icon="back" data-iconpos="notext">Back</button>');
			
			// Bind the click event to add functionality
			button.click((this.back).bind(this));
			
			// Add the button to the view
			this.getView().find("div[data-role='header']").prepend(button);
			this.getView().enhanceWithin();
		}
	};
	
	// Return class definition to dependency container
	return DialogComponent;

});