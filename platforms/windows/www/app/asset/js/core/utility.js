﻿/**
 * @param value The variable to verify
 * @returns {Boolean} Whether the specified value is an integer or an integer convertible string.
 * @author krisk
 * @see http://stackoverflow.com/questions/14636536/how-to-check-if-a-variable-is-an-integer-in-javascript
 */
function isInteger(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value));
}

/**
 * Extends the default array functionality by allowing arrays to remove elements.
 * 
 * @param {?} element.. A number of parameters representing the elements to remove
 * @returns {Array} The array
 * @author kennebec
 * @see http://stackoverflow.com/questions/3954438/remove-item-from-array-by-value
 * @see http://stackoverflow.com/questions/948358/adding-custom-functions-into-array-prototype
 */
Object.defineProperty(Array.prototype, 'remove', {
	enumerable: false,
	value: function() {
	    var what, a = arguments, L = a.length, ax;
	    while (L && this.length) {
	        what = a[--L];
	        while ((ax = this.indexOf(what)) !== -1) {
	            this.splice(ax, 1);
	        }
	    }
	    return this;
	}
});

/**
 * Extends the default array functionality by allowing arrays to remove duplicate entries.
 * 
 * @returns {Array} A copy of this array, without duplicate entries
 */
Object.defineProperty(Array.prototype, 'unique', {
	enumerable: false,
	value: function() {
	    var a = this.concat();
	    
	    for(var i=0; i<a.length; ++i) {
	        for(var j=i+1; j<a.length; ++j) {
	            if(a[i] === a[j])
	                a.splice(j--, 1);
	        }
	    }

	    return a;
	}
});

/**
 * @see {@link http://stackoverflow.com/questions/1026069/how-do-i-make-the-first-letter-of-a-string-uppercase-in-javascript}
 */
String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

/**
 * @see https://stackoverflow.com/questions/646628/how-to-check-if-a-string-startswith-another-string
 * @author Mark Byers
 */
if (!String.prototype.startsWith) {
	String.prototype.startsWith = function (needle) {
		return this.lastIndexOf(needle, 0) === 0;
	};
}
