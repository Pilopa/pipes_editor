/**
 * Constructor for the Corner class, which inherits the Tile class.
 * 
 * @param {boolean} preplaced Defines whether the tile can be moved or rotated, immutable value
 * @param {Position} position Initial position of the tile
 * @param {Direction} [initialRotation] The initial rotation value for this tile
 * @author Konstantin Schaper
 * @deprecated Replaced by '.ttd' files
 */
function Corner (preplaced, position, initialRotation) {
	// Define base openings
	var baseOpenings = {};
	baseOpenings[Direction.LEFT] = Color.NONE;
	baseOpenings[Direction.DOWN] = Color.NONE;
	
	// Call super constructor
	Corner.prototype.parent.constructor.call(this, preplaced, position, baseOpenings, undefined, initialRotation);

};

/* ************************ */
/*  Inheritance Definition  */
/* ************************ */

Corner.prototype = Object.create(Tile.prototype);
Corner.prototype.constructor = Corner;
Corner.prototype.parent = Tile.prototype;

Corner.prototype.getTileTypeId = function () {
	return "Corner";
};

/**
 * Corners are the only way to make streams cross without colliding and are therefore very powerful.
 */
Corner.prototype.getPointValue = function () {
	return 15;
};