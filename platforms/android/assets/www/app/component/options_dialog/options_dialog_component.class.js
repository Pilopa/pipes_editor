/**
 * @module component/optionsDialog
 */
define (["mcustomscrollbar", "core/dialog_component.class", "core/application_state.class"], function (mCustomScrollbar, DialogComponent, ApplicationState) {
	
	/**
	 * DialogComponent which provides the user with different forms to manipulate both the current level's settings as well
	 * as global application settings. The changes are not persisted through this class and are lost upon page reload.
	 * 
	 * @param {Application} application An application to manipulate data of
	 * @param {PlayfieldComponent} playfieldComponent The playfield to update when options change
	 * 
	 * @constructor
	 * @alias module:component/optionsDialog
	 * 
	 * @author Konstantin Schaper
	 */
	function OptionsDialogComponent(application, playfieldComponent) {
		// Super constructor call
		OptionsDialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * @type {PlayfieldComponent}
		 */
		this.playfieldComponent = playfieldComponent;
		
		/**
		 * @type {Applcation}
		 */
		this.application = application;
		
		/**
		 * @type {IScroll}
		 */
		this.scroller = undefined;
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	OptionsDialogComponent.prototype = new DialogComponent;
	OptionsDialogComponent.prototype.constructor = OptionsDialogComponent;
	OptionsDialogComponent.prototype.parent = DialogComponent.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	OptionsDialogComponent.prototype.getViewURL = function() {
		return "app/component/options_dialog/options_dialog_view.html";
	};
	
	OptionsDialogComponent.prototype.getStyleURL = function() {
		return "app/component/options_dialog/options_dialog_style.css";
	};
	
	OptionsDialogComponent.prototype.bindEvents = function() {
		this.getView().on("popupafteropen", (this.onOpen).bind(this));
		this.getView().find(".options-apply").click((this.apply).bind(this));
	};
	
	OptionsDialogComponent.prototype.initialize = function () {
		// Create scroll bar
		this.getView().find(".scroller").mCustomScrollbar({
			autoHideScrollbar: true,
			autoExpandScrollbar: true,
			scrollInertia: 100
		});
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method that is executed whenever this dialog is opened in any way.
	 * Retrieves information from the associated application's active level to initialize the values
	 * of the forms. Either enables or disables the level-options-form, depending on application state.
	 */
	OptionsDialogComponent.prototype.onOpen = function (event) {
		this.getView().find(".option-level-title").val(this.application.activeLevel.getTitle());
		this.getView().find(".option-level-dimensions-rows").val(this.application.activeLevel.getPlayfield().getRowCount()).slider("refresh");
		this.getView().find(".option-level-dimensions-cols").val(this.application.activeLevel.getPlayfield().getColumnCount()).slider("refresh");
		if (this.application.getAppState() === ApplicationState.PLAY) {
			this.getView().find(".level-options input[type='text']").textinput("disable");
			this.getView().find(".level-options input[data-type='range']").slider("disable");
		} else if (this.application.getAppState() === ApplicationState.EDITOR) {
			this.getView().find(".level-options input[type='text']").textinput("enable");
			this.getView().find(".level-options input[data-type='range']").slider("enable");
		}
	};
	
	/**
	 * Event handler method that is executed whenever the "Submit" button is clicked.
	 * Reads the form values and modifies both the associated application and it's active level.
	 */
	OptionsDialogComponent.prototype.apply = function () {
		// Grab input
		var newRows = this.getView().find(".option-level-dimensions-rows").val();
		var newCols = this.getView().find(".option-level-dimensions-cols").val();
		var newTitle = this.getView().find(".option-level-title").val();
		
		// Verify input
		// - All current input fields are restricted to valid values by html5/jquery mobile.
		
		// Apply input
		this.application.activeLevel.setTitle(newTitle);
		this.application.activeLevel.getPlayfield().setDimensions(newRows, newCols);
		this.playfieldComponent.resetCells();
		this.playfieldComponent.updateBounds();
	};
	
	// Return class definition to dependency container
	return OptionsDialogComponent;
	
});