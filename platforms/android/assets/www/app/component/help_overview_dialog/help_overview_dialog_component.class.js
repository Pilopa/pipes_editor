/**
 * @module component/helpOverviewDialog
 */
define(["jquery", "mcustomscrollbar", "core/dialog_component.class"], function ($, mCustomScrollbar, DialogComponent) {

	/**
	 * DialogComponent for displaying the help keywords in a grid.
	 * When a keyword is selected, the associated help slide is opened
	 * in the referenced HelpSlidesDialogComponent. The keywords are defined in the
	 * view code (HTML) of this component.
	 * 
	 * @param {HelpSlidesDialogComponent} helpSlidesDialogComponent
	 * 
	 * @constructor
	 * @alias module:component/helpOverviewDialog
	 * 
	 * @author Konstantin Schaper
	 */
	function HelpOverviewDialogComponent (helpSlidesDialogComponent) {
		// Super constructor call
		HelpOverviewDialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		this.helpSlidesDialogComponent = helpSlidesDialogComponent;
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	HelpOverviewDialogComponent.prototype = new DialogComponent;
	HelpOverviewDialogComponent.prototype.constructor = HelpOverviewDialogComponent;
	HelpOverviewDialogComponent.prototype.parent = DialogComponent.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	HelpOverviewDialogComponent.prototype.getViewURL = function() {
		return "app/component/help_overview_dialog/help_overview_dialog_view.html";
	};
	
	HelpOverviewDialogComponent.prototype.getStyleURL = function() {
		return "app/component/help_overview_dialog/help_overview_dialog_style.css";
	};
	
	HelpOverviewDialogComponent.prototype.bindEvents = function() {
		this.getView().find("button").click((this.onButtonClick).bind(this));
	};
	
	HelpOverviewDialogComponent.prototype.initialize = function () {
		// Create scroll bar
		this.getView().find(".scroller").mCustomScrollbar({
			autoHideScrollbar: true,
			autoExpandScrollbar: true,
			scrollInertia: 100
		});
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method being called when one of the keyword buttons is being clicked.
	 * Opens the associated HelpSlide.
	 * Overrides the default behavior so the id of the selected keyword is stored for later reference
	 * by the HelpSlidesDialogComponent.
	 */
	HelpOverviewDialogComponent.prototype.onButtonClick = function (event) {
		var keywordId = $(event.target).data("keywordid");
		this.helpSlidesDialogComponent.switchFrom(this, keywordId);
	};
	
	// Return class definition to dependency container
	return HelpOverviewDialogComponent;
});