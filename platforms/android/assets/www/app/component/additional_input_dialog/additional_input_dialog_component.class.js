/**
 * @module component/additionalInputDialog
 */
define (["jquery", "mcustomscrollbar", "core/dialog_component.class"], function ($, mCustomScrollbar, DialogComponent) {

	/**
	 * DialogComponent which lets the user insert additional values when placing a 
	 * Tile on the Playfield.
	 * 
	 * @param {Application} application The application context to use
	 * 
	 * @constructor
	 * @alias module:component/additionalInputDialog
	 * 
	 * @author Konstantin Schaper
	 */
	function AdditionalInputDialogComponent (application) {
		// Super constructor call
		AdditionalInputDialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * Will be injected by the drop
		 * event that opens this pop-up.
		 * 
		 * Layout:
		 * <code>
		 * {
		 * 		tileTypeId: <string>,
		 * 		row: <integer>,
		 * 		column: <integer>
		 * }
		 * </code>
		 */
		this.data = {};
		
		/**
		 * The playfield on which to place the tile once the 
		 * options are defined by the user.
		 * 
		 * @type {PlayfieldComponent}
		 */
		this.playfieldComponent = undefined;
		
		/**
		 * @type {Application}
		 */
		this.application = application;
		
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	AdditionalInputDialogComponent.prototype = new DialogComponent;
	AdditionalInputDialogComponent.prototype.constructor = AdditionalInputDialogComponent;
	AdditionalInputDialogComponent.prototype.parent = DialogComponent.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	AdditionalInputDialogComponent.prototype.getViewURL = function() {
		return "app/component/additional_input_dialog/additional_input_dialog_view.html";
	};
	
	AdditionalInputDialogComponent.prototype.getStyleURL = function() {
		return "app/component/additional_input_dialog/additional_input_dialog_style.css";
	};
	
	AdditionalInputDialogComponent.prototype.bindEvents = function() {
		this.getView().on("popupafteropen", this, (this.onOpen).bind(this));
		this.getView().find(".additional-inputs-apply").click((this.apply).bind(this));
	};
	
	AdditionalInputDialogComponent.prototype.initialize = function () {
		// Create scroll bar
		this.getView().find(".scroller").mCustomScrollbar({
			autoHideScrollbar: true,
			autoExpandScrollbar: true,
			scrollInertia: 100
		});
	};
	
	// === DialogComponent ===
	
	AdditionalInputDialogComponent.prototype.open = function (data, playfieldComponent) {
		this.data = data;
		this.playfieldComponent = playfieldComponent;
		this.parent.open.apply(this);
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method that is called when this dialog is being opened.
	 * Constructs the correct form for the defined data.
	 * 
	 * @see AdditionalInputDialogComponent.data
	 */
	AdditionalInputDialogComponent.prototype.onOpen = function (event) {
		// Get the form
		var inputForm = this.getView().find(".additional-inputs-form");
		
		// Clear the form
		inputForm.empty();
		
		// Define the header title
		this.getView().find(".additional-inputs-tile-type-id").text(this.data.tileTypeId.capitalizeFirstLetter());
		
		// Grab the tile type class object
		var tileType = this.application.getTileTypes()[this.data.tileTypeId];
		
		// Go through values and create input fields
		for (var property in tileType.additionalValues) {
			var typeObject = tileType.additionalValues[property];
			var html = "<label for='additional-inputs-value-" + property + "'>" + property.capitalizeFirstLetter() + ":";
			switch (typeObject['type']) { // See Tile.additionalValues
				case "integer":
					html +=
					"<input type='range' class='additional-inputs-value-" + property + "' data-popup-enabled='true'" + 
					(typeof typeObject['min'] !== "undefined" ? " min='" + typeObject['min'] + "'" : "") +
					(typeof typeObject['max']  !== "undefined" ? " max='" + typeObject['max'] + "'" : "") +
					(typeof typeObject['default']  !== "undefined" ? " value='" + typeObject['default'] + "'" : "")
					+ ">";
					break;
				case "color":
					html +=
						"<select class='additional-inputs-value-" + property + "'>" +
							"<option value='1'>Green</option>" +
							"<option value='2'>Orange</option>" +
						"<select>";
					break;
				case "direction":
					html +=
						"<select class='additional-inputs-value-" + property + "'>" +
							"<option value='0'>Up</option>" +
							"<option value='1'>Right</option>" +
							"<option value='2'>Down</option>" +
							"<option value='3'>Left</option>" +
						"<select>";
					break;
			}
			html += "</label>";
			$(html).prependTo(inputForm);
		}
		
		// Active jquery styling on form elements
		inputForm.enhanceWithin();
	};
	
	/**
	 * Event handler method that is called when the form is being submitted.
	 * Reads the form and calls the defined PlayfieldComponent to place a tile with the
	 * user provided values.
	 */
	AdditionalInputDialogComponent.prototype.apply = function (event) {
		// Grab input
		var additionalValues = {};
		
		// Grab the tile type class object
		var tileType = this.application.getTileTypes()[this.data.tileTypeId];
		
		// Go through values and create input fields
		for (var property in tileType.additionalValues) {
			var propertyInputField = this.getView().find(".additional-inputs-value-" + property + ":not(span)");
			additionalValues[property] = propertyInputField.val();
		}
		
		// Place the tile and close the dialog
		this.close();
		this.playfieldComponent.place(this.data.column, this.data.row, this.data.tileTypeId, additionalValues);
		
	};

	// Return class definition to dependency container
	return AdditionalInputDialogComponent;
	
});