/**
 * @module component/tileTypeEditorDialog
 */
define (["mcustomscrollbar", "ace/ace", "core/dialog_component.class", "model/tile.class"], function (mCustomScrollbar, ace, DialogComponent, Tile) {
	
	/**
	 * DialogComponent which provides a script editor for the TileTypeDefinition script language to create new tile types or editor existing ones.
	 * 
	 * @param {Application} application An application to manipulate data of
	 * 
	 * @constructor
	 * @alias module:component/tileTypeEditorDialog
	 * 
	 * @author Konstantin Schaper
	 * @since 1.0.1
	 */
	function TileTypeEditorDialogComponent(application) {
		// Super constructor call
		TileTypeEditorDialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * @type {Applcation}
		 */
		this.application = application;
		
		/**
		 * @type {Ace}
		 */
		this.editor = undefined;
		
		/**
		 * The tile type class to modify.
		 * Is undefined when creating a new tile type.
		 * 
		 * @type {function}
		 */
		this.tileTypeClass = undefined;
		
		/**
		 * Whether the tile type should be modifiable.
		 * 
		 * @type {boolean}
		 */
		this.editable = true;
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	TileTypeEditorDialogComponent.prototype = new DialogComponent;
	TileTypeEditorDialogComponent.prototype.constructor = TileTypeEditorDialogComponent;
	TileTypeEditorDialogComponent.prototype.parent = DialogComponent.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	TileTypeEditorDialogComponent.prototype.getViewURL = function() {
		return "app/component/tile_type_editor_dialog/tile_type_editor_dialog_view.html";
	};
	
	TileTypeEditorDialogComponent.prototype.getStyleURL = function() {
		return "app/component/tile_type_editor_dialog/tile_type_editor_dialog_style.css";
	};
	
	TileTypeEditorDialogComponent.prototype.bindEvents = function() {
		this.getView().on("popupafteropen", (this.onOpen).bind(this));
		this.getView().find(".tile-type-submit").click((this.onSubmitButtonClick).bind(this));
	};
	
	TileTypeEditorDialogComponent.prototype.initialize = function () {
		// Create Editor
		this.editor = ace.edit(this.getView().find(".editor").get(0));
		this.editor.setTheme("ace/theme/monokai");
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	/**
	 * Event handler method that is executed whenever this dialog is opened in any way.
	 * Retrieves the information provided on the opening of this dialog and inserts the given text into the editor.
	 */
	TileTypeEditorDialogComponent.prototype.onOpen = function (event) {
		// Update editor content
		if (typeof this.data.tileTypeClass === "function" && typeof this.data.tileTypeClass.prototype.getTileTypeDefinitionCode === "function") {
			this.editor.setValue(this.data.tileTypeClass.prototype.getTileTypeDefinitionCode());
		} else {
			this.editor.setValue("");
		}
		
		// Set editability
		this.editor.setReadOnly(!this.data.editable);
		
		// Create
		if (this.data.editable && typeof this.data.tileTypeClass !== "undefined") {
			this.getView().find(".tile-type-editor-title").text("Modify: " + this.data.tileTypeClass.name);
			this.getView().find(".tile-type-submit").text("Update").removeClass("ui-disabled");
			
		// Modify
		} else if (this.data.editable && typeof this.data.tileTypeClass === "undefined") {
			this.getView().find(".tile-type-editor-title").text("Create new");
			this.getView().find(".tile-type-submit").text("Create").removeClass("ui-disabled");
			
		// View
		} else if (!this.data.editable && typeof this.data.tileTypeClass !== "undefined") {
			this.getView().find(".tile-type-editor-title").text("View: " + this.data.tileTypeClass.name);
			this.getView().find(".tile-type-submit").text("Read-only").addClass("ui-disabled");
		}
	};
	
	TileTypeEditorDialogComponent.prototype.onSubmitButtonClick = function (event) {
		// Submit Changes
		this.application.createTileType(this.editor.getSession().getValue(), (this.onSubmitEvaluated).bind(this));
	};
	
	TileTypeEditorDialogComponent.prototype.onSubmitEvaluated = function (result, error) {
		if (typeof error === "object") {
			this.editor.getSession().setAnnotations([
			{
				row: error.location.start.line - 1,
				column: Math.floor((error.location.start.column + error.location.end.column) / 2),
				text: error.message,
				type: "error"
			}
			                                         ]);
		} else if (typeof error === "string") {
			this.editor.getSession().setAnnotations([
 			{
 				row: 0,
 				column: 0,
 				text: error,
 				type: "error"
 			}
			                             			]);
		} else if (typeof result === "function") {
			if (!this.application.isDefaultTileType(result)) {
				this.application.addTileType(result);
				this.back({
					newTileType: result
				});
			} else {
				this.editor.getSession().setAnnotations([
     			{
     				row: 0,
     				column: 0,
     				text: "Default tile types must not be overwritten.",
     				type: "error"
     			}
				                             			]);
			}
		}
	};

	// Return class definition to dependency container
	return TileTypeEditorDialogComponent;
	
});