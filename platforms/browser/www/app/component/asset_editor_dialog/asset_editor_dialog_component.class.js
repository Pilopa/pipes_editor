/**
 * @module component/assetEditorDialog
 */
define (["jquery", "mcustomscrollbar", "core/dialog_component.class", "jquery.jstree"], function ($, mCustomScrollbar, DialogComponent, jstree) {
	
	/**
	 * DialogComponent which allows the user to modify custom assets (primarily images) available in their level.
	 * 
	 * @param {Application} application An application context to get the active level from
	 * 
	 * @constructor
	 * @alias module:component/assetEditorDialog
	 * 
	 * @author Konstantin Schaper
	 * @since 1.0.3
	 */
	function AssetEditorDialogComponent(application) {
		// Super constructor call
		AssetEditorDialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * @type {Applcation}
		 */
		this.application = application;
		
		/**
		 * @type {jstree}
		 */
		this.editor = undefined;
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	AssetEditorDialogComponent.prototype = new DialogComponent;
	AssetEditorDialogComponent.prototype.constructor = AssetEditorDialogComponent;
	AssetEditorDialogComponent.prototype.parent = DialogComponent.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	AssetEditorDialogComponent.prototype.getViewURL = function() {
		return "app/component/asset_editor_dialog/asset_editor_dialog_view.html";
	};
	
	AssetEditorDialogComponent.prototype.getStyleURL = function() {
		return "app/component/asset_editor_dialog/asset_editor_dialog_style.css";
	};
	
	AssetEditorDialogComponent.prototype.bindEvents = function() {
		this.getView().find(".asset-add").click((this.onAddButtonClick).bind(this));
		this.getView().find(".asset-edit").click((this.onEditButtonClick).bind(this));
		this.getView().find(".asset-delete").click((this.onDeleteButtonClick).bind(this));
		this.application.registerListener("onTileTypeAdded", (this.onTileTypeAdded).bind(this));
		this.application.registerListener("onAssetAdded", (this.onAssetAdded).bind(this));
		this.application.registerListener("onAssetUpdated", (this.onAssetUpdated).bind(this));
		this.application.registerListener("onAssetRemoved", (this.onAssetRemoved).bind(this));
		this.getView().find(".image-input").change((this.onImageInputChange).bind(this));
		this.getView().find(".image").click((this.onImageInputClick).bind(this));
		// TODO Add level loaded listener to reset the asset content
	};
	
	AssetEditorDialogComponent.prototype.initialize = function () {
		var that = this;
		
		// Create scroll bar
		this.getView().find(".scroller").mCustomScrollbar({
			axis: "xy",
			autoHideScrollbar: true,
			autoExpandScrollbar: true,
			scrollInertia: 100
		});
		
		// Create the drag listener
		$("document").on("dragstart", (function (event) {
			console.info("drag-start");
			this.getView().find(".selected-content .image").addClass("drag-active");
		}).bind(this));
		
		$("document").on("dragend", (function (event) {
			console.info("drag-end");
			this.getView().find(".selected-content .image").removeClass("drag-active");
		}).bind(this));
		
		// Create dropzone
		this.getView().find('.selected-content .image')
		  .on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
		    e.preventDefault();
		    e.stopPropagation();
		  })
		  .on('dragover dragenter', function() {
			 $(this).addClass("drag-over");
		  })
		  .on('dragleave dragend drop', function() {
	         $(this).removeClass("drag-over");
		  })
		  .on({
	         drop: function(e) {	
	             var file = e.dataTransfer.files[0];
	             that.updateSelectedNodeAssetImage(file);
	         }
	     });
		
		// Create Editor
		this.editor = $.jstree.create(this.getView().find(".editor"), {
			"plugins": ["dnd",  "unique", "types", "wholerow"],
			"core": {
				"check_callback": true,
				"themes": {
					"name": "default-dark",
					"responsive": true,
					"icons": true,
					"dots": true
				}
			},
			"types": {
				
				"fixed_folder": {
					"icon": "fa fa-folder-o",
					"valid_children": ["image", "folder", "fixed_folder"]
				},
				
				"folder": {
					"icon": "fa fa-folder",
					"valid_children": ["image", "folder"]
				},
				
				"image": {
					"icon": "fa fa-file-image-o",
					"valid_children": []
				},
				
				"default_folder": {
					"icon": "fa fa-folder-o",
					"valid_children": ["default_image"]
				},
				
				"default_image": {
					"icon": "fa fa-file-o",
					"valid_children": []
				}
			}
		});
		
		// Initialize editor values for default level
		this.editor.create_node(null, {
			"id": "Default_Assets_Folder",
			"text": "Default",
			"type": "default_folder",
			"state" : { "opened" : true }
		});
		
		this.editor.create_node(null, {
			"id": "Custom_Assets_Folder",
			"text": "Custom Images",
			"type": "fixed_folder",
			"state" : { "opened" : true }
		});
		
		this.editor.create_node("Custom_Assets_Folder", {
			"id": "Custom_Tile_Type_Folder",
			"text": "Tile Types",
			"type": "fixed_folder",
			"state" : { "opened" : true }
		});
		
		// Register events
		this.editor.get_container().on("changed.jstree", (this.onEditorSelectionChanged).bind(this));
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	AssetEditorDialogComponent.prototype.onAssetAdded = function (assetName, assetBlobUrl) {
		
	};
	
	AssetEditorDialogComponent.prototype.onAssetUpdated = function (oldAssetName, newAssetName, assetBlobUrl) {
		
	};
	
	AssetEditorDialogComponent.prototype.onAssetRemoved = function (assetName, assetBlobUrl) {
		
	};
	
	AssetEditorDialogComponent.prototype.onTileTypeAdded = function (tileTypeClass) {
		// Default tile types are automatically added for reference, but cannot be modified
		if (this.application.isDefaultTileType(tileTypeClass))
			this.editor.create_node("Default_Assets_Folder", {
				"id": tileTypeClass.name,
				"text": tileTypeClass.name,
				"type": "default_image",
				"img": this.application.getTileTypeImageUrl(tileTypeClass.prototype.getTileTypeId())
			});
	};
	
	AssetEditorDialogComponent.prototype.onAddButtonClick = function (event) {
		var selectedNode = this.getSelectedAssetNode();
		this.editor.create_node(selectedNode.id, {
			"text": "New",
			"type": "image"
		});
	};
	
	AssetEditorDialogComponent.prototype.onEditButtonClick = function (event) {
		var selectedNode = this.getSelectedAssetNode();
		var oldId = selectedNode.id;
		this.editor.edit(selectedNode, (function (node, status, cancelled) {
			if (cancelled) return;
			
			// Check if an image was cached under the old id
			var exists = this.application.activeLevel.getTileTypeImageCache()[node.id];
			
			// Delete the old cached image if present
			if (exists) {
				delete this.application.activeLevel.getTileTypeImageCache()[oldId];
				
				// Re-cache the image under the new id
				this.application.activeLevel.cacheTileTypeImage(node.id, exists);
			} else if (node.original.img) {
				this.application.activeLevel.cacheTileTypeImage(node.id, node.original.img);
			}
			
			this.editor.set_id(node, node.text);
			
		}).bind(this));
	};
	
	AssetEditorDialogComponent.prototype.onDeleteButtonClick = function (event) {
		var selectedNode = this.getSelectedAssetNode();
		
		//
		this.editor.delete_node(selectedNode);
		
		// Update ui
		this.onEditorSelectionChanged();
	};
	
	AssetEditorDialogComponent.prototype.onImageInputClick = function (event) {
		if (this.getView().find('.selected-content').hasClass("active"))
			this.getView().find('.image-input').trigger('click');
	};
	
	AssetEditorDialogComponent.prototype.onImageInputChange = function (event) {
		this.updateSelectedNodeAssetImage(event.target.files[0]);
	};
	
	AssetEditorDialogComponent.prototype.onEditorSelectionChanged = function (e, data) {
		var node = typeof data !== "undefined" ? data.node : undefined;
		
		// Update selection content
		this.showContentFor(node);
		
		// Update button state
		if (!node) {
			this.getView().find(".selected-content").removeClass("active");
			this.getView().find(".buttons .asset-delete, .buttons .asset-edit, .buttons .asset-add").addClass("ui-state-disabled");
		} else {
			// -- Modify
			if (node.type.startsWith("default") || node.type.startsWith("fixed")) {
				this.getView().find(".selected-content").removeClass("active");
				this.getView().find(".buttons .asset-delete, .buttons .asset-edit").addClass("ui-state-disabled");
			} else {
				this.getView().find(".selected-content").addClass("active");
				this.getView().find(".buttons .asset-delete, .buttons .asset-edit").removeClass("ui-state-disabled");
			}
			// -- Add
			if (node.type.startsWith("default") || node.type === "image") 
				this.getView().find(".buttons .asset-add").addClass("ui-state-disabled");
			else
				this.getView().find(".buttons .asset-add").removeClass("ui-state-disabled");
		}
		
	};
	
	AssetEditorDialogComponent.prototype.updateSelectedNodeAssetImage = function (imageFile) {
		 var fileReader = new FileReader();
         var that = this;

         fileReader.onload = (function(imageFile) {
             return function(event) {
                 // Image file loaded
                 var imageUrl = event.target.result;
                 
                 // Call the application to update the asset
                 var selected = that.editor.get_selected(true);
                 that.application.activeLevel.cacheTileTypeImage(selected.text, imageUrl);
                 that.application.fireEvent("onAssetUpdated", "");
             };
         })(imageFile);

         fileReader.readAsDataURL(imageFile);  
	};
	
	AssetEditorDialogComponent.prototype.showContentFor = function (node) {
		if (!node) {
			// Text
			this.getView().find(".selected-content .name").text("");
			
			// Image
			this.getView().find(".selected-content .image").css({
				"background-image": undefined
	        });
		} else {
			// Text
			this.getView().find(".selected-content .name").text(node.text);
			
			// Image
			var imageUrl = node.original.img;
			this.getView().find(".selected-content .image").css({
				"background-image": (typeof imageUrl !== "undefined" ? imageUrl : "")
	        });
		}
	};
	
	/**
	 * @returns {object} The asset node currently selected in the tree view.
	 * @see jstree documentation
	 */
	AssetEditorDialogComponent.prototype.getSelectedAssetNode = function () {
		var selected = this.editor.get_selected(true);
		return selected.length === 0 ? undefined : selected[0];
	};

	// Return class definition to dependency container
	return AssetEditorDialogComponent;
	
});