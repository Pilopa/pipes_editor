/**
 * @module component/toolbox
 */
define (["jquery", "jqueryui", "touchpunch", "mcustomscrollbar", "core/component.class", "core/application_state.class"],
		function ($, $ui, touchpunch, mCustomScrollbar, Component, ApplicationState) {	

	/**
	 * UI Component which lets both players and creators place tiles on the playfield and displays the 
	 * number of tiles that can be still placed.
	 * 
	 * @param {Application} application
	 * @param {NavigationComponent} navigationComponent
	 * @param {PlayfieldComponent} playfieldComponent
	 * 
	 * @constructor
	 * @alias module:component/toolbox
	 * 
	 * @author Konstantin Schaper
	 */
	function ToolboxComponent(application, navigationComponent, playfieldComponent) {
		// Super constructor call
		ToolboxComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * @type {Application}
		 */
		this.application = application;
		
		/**
		 * @type {NavigationComponent}
		 */
		this.navigationComponent = navigationComponent;
		
		/**
		 * @type {PlayfieldComponent}
		 */
		this.playfieldComponent = playfieldComponent;
		
	}
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	ToolboxComponent.prototype = new Component;
	ToolboxComponent.prototype.constructor = ToolboxComponent;
	ToolboxComponent.prototype.parent = Component.prototype;
	
	/* ****************** */
	/* Overridden Methods */
	/* ****************** */
	
	// === Component ===
	
	ToolboxComponent.prototype.getViewURL = function() {
		return "app/component/toolbox/toolbox_view.html";
	};
	
	ToolboxComponent.prototype.getStyleURL = function() {
		return "app/component/toolbox/toolbox_style.css";
	};
	
	ToolboxComponent.prototype.bindEvents = function() {
		// Window resize
		$(window).on("window::resize", (function() {
			this.adjustSizes();
		}).bind(this));
		
		// Button clicks
		this.getView().find(".validate").click((this.onValidateButtonClick).bind(this));
		
		// Application Listeners
		this.application.registerListener("onStateChange", (this.onApplicationStateChange).bind(this));
		this.application.registerListener("onTilePlaced", (this.onApplicationTilePlaced).bind(this));
		this.application.registerListener("onTileRemoved", (this.onApplicationTileRemoved).bind(this));
		this.application.registerListener("onTileTypeAdded", (this.onTileTypeAdded).bind(this));
		this.application.registerListener("onTileTypeRemoved", (this.onTileTypeRemoved).bind(this));
		this.application.registerListener("onLevelLoaded", (this.onLevelLoaded).bind(this));
	};
	
	ToolboxComponent.prototype.initialize = function () {
		// Create scroll bar	
		this.getView().find(".scroller").mCustomScrollbar({
			autoHideScrollbar: true,
			autoExpandScrollbar: true,
			scrollInertia: 100
		});
		
		// Initialize the delete overlay
		this.getView().find(".toolbox-delete-overlay").droppable({
			accept: ".tile",
			activate: function (event, ui) {
				$(this).css({
					"z-index": 98,
					"opacity": 98
				});
			},
			deactivate: function (event, ui) {
				$(this).css({
					"z-index": 0,
					"opacity": 0
				});
			},
			drop: (function (event, ui) {
				var dragCR = ui.helper.data("cr");
				var dragSplit = dragCR.split(",");
				var dragCol = dragSplit[0];
				var dragRow = dragSplit[1];
				this.playfieldComponent.remove(dragCol, dragRow);
			}).bind(this)
		});
	};
	
	/* ****************** */
	/* 	 Public Methods   */
	/* ****************** */
	
	// === Event Handlers ===
	
	/**
	 * Event handler method that is being called whenever the associated application's state changes.
	 * Adjusts the view accordingly.
	 */
	ToolboxComponent.prototype.onApplicationStateChange = function (newState) {
		// Toggle button display
		if (newState === ApplicationState.EDITOR) {
			this.getView().find(".validate").hide();
			this.getView().find(".scroller").removeClass("playmode");
		} else if (newState === ApplicationState.PLAY) {
			this.getView().find(".validate").show();
			this.getView().find(".scroller").addClass("playmode");
			this.onTilePlacedOrRemoved();
		}
		
		// Recreate tools list
		this.resetTools();
	};
	
	ToolboxComponent.prototype.onTileTypeAdded = function (tileTypeClass) {
		this.resetTool(tileTypeClass.name);
		this.onTilePlacedOrRemoved();
	};
	
	ToolboxComponent.prototype.onTileTypeRemoved = function (tileTypeId) {
		this.resetTools();
		this.onTilePlacedOrRemoved();
	};
	
	/**
	 * Event handler method that is being called whenever a new placed is tile or removed within the associated application context.
	 * Updates the displayed point value.
	 */
	ToolboxComponent.prototype.onTilePlacedOrRemoved = function () {
		var toolsPointValue = this.application.activeLevel.getBaseToolsPointSum(this.application.getDefaultTileTypes());
		var placedTilesPointValue = this.application.activeLevel.getPlayfield().getPlacedTilesValue();
		this.getView().find(".validate .point-display").text(placedTilesPointValue <= 0 ? 0 : toolsPointValue - placedTilesPointValue);
	};
	
	/**
	 * Event handler method that is being called whenever the "Validate" button is clicked.
	 * Validates the associated application's active level and displays the result.
	 */
	ToolboxComponent.prototype.onValidateButtonClick = function (event) {
		var validateButton = this.getView().find(".validate");
		
		// Retrieve validation state
		var state = validateButton.data("state");
		
		if (typeof state === "undefined") {
			// Initialize UI
			validateButton.data("state", "validating").buttonMarkup({ icon: "clock" }).addClass("ui-state-disabled").find(".text").text("Validating...");
			this.navigationComponent.getView().find(".ui-tabs").tabs("disable", 0);
			this.playfieldComponent.disableInteraction();
			this.getView().addClass("disabled");
			
			// Start verification
			var errors = this.application.activeLevel.getPlayfield().verify();
			
			// Show result
			if (errors.length == 0) {
				validateButton.data("state", "validated").addClass("success").buttonMarkup({ icon: "check" }).removeClass("ui-state-disabled").find(".text").text("Reset");
			} else {
				console.dir(errors);
				this.playfieldComponent.showErrorHightlighting(errors);
				validateButton.data("state", "validated").addClass("failure").buttonMarkup({ icon: "alert" }).removeClass("ui-state-disabled").find(".text").text("Reset");
			}
	
		} else if (state === "validated") {
			// Reset the level
			this.application.activeLevel.reset();
			
			// Reset playfield
			this.playfieldComponent.resetErrorHighlighting();
			this.playfieldComponent.enableInteraction();
			this.getView().removeClass("disabled");
			
			// Reset the UI	
			this.navigationComponent.getView().find(".ui-tabs").tabs("enable", 0);
			validateButton.removeClass("success failure").removeData("state").buttonMarkup({ icon: "" }).find(".text").text("Verify");
		}
	};
	
	/**
	 * Event handler method that is being called whenever a tile is placed within the associated application's context.
	 * Updates the tool display for the tile type that has been placed.
	 */
	ToolboxComponent.prototype.onApplicationTilePlaced = function (tile) {
		if (this.application.getAppState() === ApplicationState.PLAY) {
			this.application.activeLevel.setToolAvailabilityCount(tile.getTileTypeId(), this.application.activeLevel.getToolAvailabilityCount(tile.getTileTypeId()) - 1);
			this.updateTool(tile.getTileTypeId());
			this.onTilePlacedOrRemoved();
		}
	};
	
	/**
	 * Event handler method that is being called whenever a tile is removed within the associated application's context.
	 * Updates the tool display for the tile type that has been removed.
	 */
	ToolboxComponent.prototype.onApplicationTileRemoved = function (tile) {
		if (this.application.getAppState() === ApplicationState.PLAY) {
			this.application.activeLevel.setToolAvailabilityCount(tile.getTileTypeId(), this.application.activeLevel.getToolAvailabilityCount(tile.getTileTypeId()) + 1);
			this.updateTool(tile.getTileTypeId());
			this.onTilePlacedOrRemoved();
		}
	};
	
	/**
	 * Event handler method that is being called whenever a new level is loaded within the associated application's context.
	 * Recreates the toolbox to match the new level data.
	 */
	ToolboxComponent.prototype.onLevelLoaded = function (error, level) {
		this.resetTools();
	};
	
	// == Other ==
	
	/**
	 * Event handler method that is being called when the user changes the tool availability of a tile type in editor mode.
	 * Updates the data model accordingly.
	 */
	ToolboxComponent.prototype.toolAvailabilityChange = function (event) {
		// Validate input
		var v = this.value;
		if (v === "" || typeof v === "undefined") {
			this.value = 0;
		} else if (!isInteger(v)) {
			this.value = v.slice(0, -1);
		} else {
			this.value = Math.min(v, event.data.application.config["maximum_tool_count"]);
		}
		
		// Override model
		var toolId = $(this).data("ttid");
		var count = parseInt(this.value);
		if (isInteger(count) && count > 0)
			event.data.application.activeLevel.setBaseToolAvailabilityCount(toolId, count);
	};
	
	/**
	 * Creates the tool display for the given tile type id.
	 * 
	 * @param {String} id The id of the tool to create a visual representation for
	 * @returns {JQuery} The created tool object
	 */
	ToolboxComponent.prototype.createTool = function (id) {
		// Define that = this object for use inside inner anonymous function
		var that = this;
		
		// - Create element
		var tool = $("<li class='tool' data-ttid='" + id.toLowerCase() + "'><div class='flex-container'><input class='count' data-ttid='" + id.toLowerCase() + "' type='number' min='0' step='1' value='0'><div class='tool-drag' data-ttid='" + id.toLowerCase() + "'></div></li>");
		tool.appendTo(this.getView().find(".toolbox-list"))
		// - Make Draggable
		.find(".tool-drag")
		.draggable({
			helper: "clone",
			delay: 150,				
			opacity: 0.75,
			revert: "invalid",
			revertDuration: 150,
			zIndex: 99,
			containment: "window",
			cancel: ".toolbox.disabled",
			scroll: false,
			appendTo: "body", // Fix for drag from within scrollable content
			start: function (event, ui) {
				// Result of the scrollbar fix cause height of the helper is otherwise undefined/zero
				// => While we are at it, just set the size to the droppable target size
				ui.helper.width(that.playfieldComponent.cellWidth); 
				ui.helper.height(that.playfieldComponent.cellHeight);
				
				// Center the helper to the cursor
				$(this).draggable('instance').offset.click = {
		            left: Math.floor(ui.helper.width() / 2),
		            top: Math.floor(ui.helper.height() / 2)
		        }; 
			}
		})
		// - Add Graphic
		.css({
			"background-image": this.application.getTileTypeImageUrl(id, this.application.activeLevel)
		})			
		// - Listen to input changes
		.parent()
		.find(".count")
		.on("change", this, this.toolAvailabilityChange);
		
		
		tool.parent().enhanceWithin();
		
		return tool;
	};
	
	/**
	 * Resizes all tool displays to fill the width of its parent.
	 * Each tool's height is 50% of its width and the width of the
	 * two squares within each tool display (availability count and tool image)
	 * is the same as its height (which is 100% of the tool container).
	 */
	ToolboxComponent.prototype.adjustSizes = function () {
		// Resize tool
		this.getView().find(".tool").each(function() {
			$(this).css("height", $(this).width() * 0.50);
		});
		
		// Resize tool contents
		this.getView().find(".tool .ui-input-text, .tool .tool-drag").each(function(index, element) {
			$(this).css("width", $(this).height());
		});
	};
	
	/**
	 * Deletes all existing tool displays and recreates them, 
	 * gathering the required data from the associated application's active level.
	 */
	ToolboxComponent.prototype.resetTools = function () {
		var toolboxList = this.getView().find(".toolbox-list");
		
		// Empty the list
		toolboxList.empty();
		
		// Get the tools to create
		var toolIds = this.application.activeLevel.getTools();
		
		// Create the tools
		toolIds.forEach(function (toolId, index, array) {
			this.resetTool(toolId);
		}, this);
		
		// Mark the toolbox as empty if there are no tools
		if (toolboxList.find("li").length <= 0) {
			this.getView().addClass("empty");
		} else {
			this.getView().removeClass("empty");
		}
	};
	
	ToolboxComponent.prototype.resetTool = function (toolId) {
		// In playmode, only defined tools that are available at least once will be displayed
		if (this.application.getAppState() === ApplicationState.PLAY) {
			var availabilityCount = this.application.activeLevel.getToolAvailabilityCount(toolId);
			if (availabilityCount === "undefined" || availabilityCount <= 0) return;
		}
		
		// Try to retrieve the tool representation
		var toolElement = this.getView().find(".tool[data-ttid='" + toolId + "']");
		
		// Check whether the tool display exists already
		if (toolElement.length)
			this.updateTool(toolId, toolElement);
		else
			this.updateTool(toolId, this.createTool(toolId));
	};
	
	/**
	 * Calls ToolboxComponent.updateTool for each tool display within the toolbox.
	 */
	ToolboxComponent.prototype.updateTools = function () {
		this.getView().find(".tool").each((function (index, element) {
			this.updateTool($(element).data("ttid"), element);
		}).bind(this));
	};
	
	/**
	 * Updates the tool display of the tool with the given id.
	 * Uses the second parameter, if present, or attempts to find it itself otherwise.
	 * 
	 * @param {Integer} id The tile type id of the tool to update.
	 * @param {JQuery} [element] Optional tool display element to use
	 */
	ToolboxComponent.prototype.updateTool = function(id, element) {
		if (typeof element === "undefined") element = this.getView().find(".tool[data-ttid='" + id.toLowerCase() + "']");
		
		var toolCount = $(element).find(".count");
		var toolDrag = $(element).find(".tool-drag");
		
		// Update tool count if application state is PLAY
		if (this.application.getAppState() === ApplicationState.PLAY) {
			var availabilityCount = this.application.activeLevel.getToolAvailabilityCount(id);
			
			// Count
			toolCount.textinput("disable");
			if(typeof availabilityCount === 'undefined') {
				toolCount.val(0);
			} else {
				toolCount.val(availabilityCount);
			}
			
			// Drag
			if (availabilityCount > 0) {
				toolDrag.draggable("enable");
			} else {
				toolDrag.draggable("disable");
			}
		} else if (this.application.getAppState() === ApplicationState.EDITOR) {
			var availabilityCount = this.application.activeLevel.getBaseToolAvailabilityCount(id);
			
			// Count
			toolCount.textinput("enable");
			if(typeof availabilityCount === 'undefined') {
				toolCount.val(0);
			} else {
				toolCount.val(availabilityCount);
			}
				
			// Drag
			toolDrag.draggable("enable");
		}
	};

	// Return class definition to dependency container
	return ToolboxComponent;
});