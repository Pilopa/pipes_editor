/**
 * @module asset/js/core/dialog_component
 */
define (["jquery", "core/component.class"], function ($, Component) {

	/**
	 * Inherits from the Component class and represents a popup with content.
	 * The html view for components of this type should have a div wrapper as root with class
	 * "dialog" and the data-role "popup" defined.
	 * 
	 * @constructor
	 * @alias module:asset/js/core/dialog_component
	 * 
	 * @author Konstantin Schaper
	 * @see http://api.jquerymobile.com/popup/
	 */
	function DialogComponent() {
		
		// Super constructor call
		DialogComponent.prototype.parent.constructor.call(this);
		
		/* ****************** */
		/* 	 Public Members   */
		/* ****************** */
		
		/**
		 * The dialog which opened this dialog.
		 * 
		 * @type {DialogComponent}
		 */
		this.previous = undefined;
		
		/**
		 * Information provided by the context which opened the dialog.
		 * 
		 * @type {anything|array|undefined}
		 */
		this.data = undefined;
		
	};
	
	/* ************************ */
	/*  Inheritance Definition  */
	/* ************************ */
	
	DialogComponent.prototype = Object.create(Component.prototype);
	DialogComponent.prototype.constructor = DialogComponent;
	DialogComponent.prototype.parent = Component.prototype;
	
	/* ****************** */
	/*    Static Fields   */
	/* ****************** */
	
	/**
	 * Defines how long the system waits until the previous dialog should be opened,
	 * this is to prevent an inconvenient behaviour of JQuery mobile regarding the closing 
	 * and opening of dialogs.
	 * 
	 * @type {number}
	 */
	DialogComponent.backTimeout = 25; //ms
	
	/* ****************** */
	/*   Public Methods   */
	/* ****************** */
	
	/**
	 * Opens this dialog. All arguments passed into this function are stored in
	 * the accessible "data" attribute of the dialog. If there is only one arguments present,
	 * it will be set as the new data value instead of the arguments array. If there are no arguments provided,
	 * the data field will be voided.
	 */
	DialogComponent.prototype.open = function () {
		if (arguments.length > 1) this.data = arguments;
		else if (arguments.length === 1) this.data = arguments[0];
		else this.data = undefined;

		this.getView().popup("open");
	};
	
	/**
	 * Closes this dialog.
	 */
	DialogComponent.prototype.close = function () {
		this.getView().popup("close");
	};
	
	/**
	 * Closes this dialog and opens the previously displayed dialog, if present.
	 * All arguments passed to this method are forwarded to the "open" method of the previous dialog.
	 */
	DialogComponent.prototype.back = function () {
		this.close();
		var outerArguments = arguments;
		if (this.previous) {
			setTimeout((function () {
				this.previous.open.apply(this.previous, outerArguments);
				this.previous = undefined;
			}).bind(this), DialogComponent.backTimeout);
		}
	};
	
	/**
	 * Switches to this dialog from another one. All arguments beyond the first are forwarded to the dialog's "open" method as a single array argument.
	 * 
	 * @param {DialogComponent} fromDialogComponent The dialog which initiated the switching process
	 */
	DialogComponent.prototype.switchFrom = function(fromDialogComponent) {
		// Get arguments except first
		var outerArguments = Array.prototype.slice.call(arguments, 1);
		
		// Store dialog to later go back to
		this.previous = fromDialogComponent;
		
		// Add the back button to the UI
		this.addBackButton();
		
		// Open Self
		var openSelf = (function () {
			this.open.apply(this, outerArguments);
		}).bind(this);
		
		// Close
		if (typeof fromDialogComponent !== "undfined") 
			fromDialogComponent.getView().one("popupafterclose", openSelf).popup("close");
		else
			openSelf();
		
	};
	
	/**
	 * Adds a back button to the jquery mobile header (data-role='header') element of this dialog.
	 * Does nothing if a back button is already present.
	 */
	DialogComponent.prototype.addBackButton = function () {
		if (!this.getView().find("button.back").length) {
			// Create the button element
			var button = $('<button class="back ui-btn-left" data-theme="a" data-icon="back" data-iconpos="notext">Back</button>');
			
			// Bind the click event to add functionality
			button.click((this.back).bind(this));
			
			// Add the button to the view
			this.getView().find("div[data-role='header']").prepend(button);
			this.getView().enhanceWithin();
		}
	};
	
	// Return class definition to dependency container
	return DialogComponent;

});