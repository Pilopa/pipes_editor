define(function () {

	/**
	 * Enumeration defining the different states the application can be in.
	 * 
	 * @author Konstantin Schaper
	 * @exports asset/js/model/application_state
	 */
	var ApplicationState = {
			
		NONE: 0,
		EDITOR: 1,
		PLAY: 2,
		
		/**
		 * @param value The variable to verify
		 * @returns {boolean} Whether or not the given value is an ApplicationState.
		 */
		verify: function (value) {
			return isInteger(value) && value >= 0 && value <= 2;
		},
		
		/**
		 * Array containing all values defined by this enumeration.
		 * 
		 * @type {array}
		 */
		values: [this.NONE, this.EDITOR, this.PLAY]
	};
	
	// Return class definition to dependency container
	return ApplicationState;

});