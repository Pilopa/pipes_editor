/**
 * @module asset/js/core/event_manager
 */
define(function () {

	/**
	 * Manages a simple event system based on the observer pattern.
	 * 
	 * @param {string[]} eventTypes An array containing all valid event types.
	 * 
	 * @constructor
	 * @alias module:asset/js/core/event_manager
	 * 
	 * @author Konstantin Schaper
	 * @since 1.0.3
	 */
	function EventManager(eventTypes) {
		
		/* ****************** */
		/* 	 Private Members  */
		/* ****************** */
		
		/**
		 * Contains all the list for event handlers to register in.
		 * 
		 * @type {object.<string, function[]>}
		 */
		var appListeners = {};
		
		/* ****************** */
		/*   Public Members   */
		/* ****************** */
		
		/**
		 * 
		 */
		this.getEventTypes = function () {
			return eventTypes;
		};
		
		/**
		 * Registers the given listener for the given event.
		 * 
		 * @param {String} event
		 * @param {Function} listener
		 * @throws {Error} When the given event is not supported
		 */
		this.registerListener = function (event, listener) {
			if (appListeners[event] instanceof Array)
				appListeners[event].push(listener);
			else
				throw new Error("Event type not recognized");
		};
		
		/**
		 * Registers the given listener for the given event.
		 * 
		 * @param {string} event
		 * @param {Function} listener
		 * @throws {Error} When the given event is not supported
		 */
		this.deregisterListener = function (event, listener) {
			if (appListeners[event] instanceof Array)
				appListeners[event].remove(listener);
			else
				throw new Error("Event type not recognized");
		};
		
		/**
		 * Calls all listeners registered for the given event type to handle
		 * the given event data.
		 * 
		 * @param {string} eventType The type of event to fire
		 * @param {Array} args All further arguments forwarded to listeners
		 */
		this.fireEvent = function (eventType, args) {
			var listeners = appListeners[eventType];
			if (listeners instanceof Array) {
				var arrayLength = listeners.length;
				for (var i = 0; i < arrayLength; i++) {
				    var listener = listeners[i];
				    if (typeof listener === 'function')
				    	listener.apply(listener, args);
				}
			}
		};
		
		/* ****************** */
		/* 	   Constructor    */
		/* ****************** */
		
		// Initialize event types
		for (var index = 0; index < eventTypes.length; index++) {
			var eventType = eventTypes[index];
			appListeners[eventType] = [];
		}
		
	};
	
	// Return class definition to dependency container
	return EventManager;

});