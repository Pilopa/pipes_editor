/**
 * Constructor for the Wall class, which inherits the Tile class.
 * 
 * @param {boolean} preplaced Defines whether the tile can be moved or rotated, immutable value
 * @param {Position} position Initial position of the tile
 * @param {Direction} [initialRotation] The initial rotation value for this tile
 * @author Konstantin Schaper
 * @deprecated Replaced by '.ttd' files
 */
function Wall (preplaced, position, initialRotation) {
	// Call super constructor
	Wall.prototype.parent.constructor.call(this, preplaced, position, {}, undefined, initialRotation);
};

/* ************************ */
/*  Inheritance Definition  */
/* ************************ */

Wall.prototype = Object.create(Tile.prototype);
Wall.prototype.constructor = Wall;
Wall.prototype.parent = Tile.prototype;

Wall.prototype.getTileTypeId = function () {
	return "Wall";
};

Wall.prototype.isRotatable = function () {
	return false;
};