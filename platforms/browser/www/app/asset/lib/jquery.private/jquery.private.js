// No conflict version of the jquery amd module
// # Does not define global variables
define("jquery-private", ['jquery'], function (jq) {
	return jq.noConflict(true);
});