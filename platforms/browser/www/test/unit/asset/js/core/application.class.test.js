define(["core/application.class", "core/application_state.class"], function (Application, ApplicationState) {
	
	QUnit.module( "Application.class", function () {
		
		QUnit.test("setAppState", function (assert) {
			// Arrange
			var testApp = new Application();
			var act = undefined;
			
			// Act
			act = function () { testApp.setAppState(undefined); };
	
			// Assert
			assert.raises(act, "Setting the appState to undefined must throw error to pass.");
			
			// Act
			act = function () { testApp.setAppState(null); };
	
			// Assert
			assert.raises(act, "Setting the appState to null must throw error to pass.");
			
			// Act
			act = function () { testApp.setAppState(-1); };
	
			// Assert
			assert.raises(act, "Setting the appState to a negative number must throw error to pass.");
			
			// Act
			testApp.setAppState(ApplicationState.NONE);
	
			// Assert
			assert.equal(testApp.getAppState(), ApplicationState.NONE, "getAppState must return ApplicationState.NONE to pass.");
			
			// Act
			testApp.setAppState(ApplicationState.EDITOR);
	
			// Assert
			assert.equal(testApp.getAppState(), ApplicationState.EDITOR, "getAppState must return ApplicationState.EDITOR to pass.");
			
			// Act
			testApp.setAppState(ApplicationState.PLAY);
	
			// Assert
			assert.equal(testApp.getAppState(), ApplicationState.PLAY, "getAppState must return ApplicationState.PLAY to pass.");
		});
		
		QUnit.test("registerListener", function (assert) {
			// Arrange
			var testApp = new Application();
			var act = undefined;
			
			// Act
			act = function () { testApp.registerListener(undefined, undefined); };
	
			// Assert
			assert.raises(act, "Registering a listener on an undefined event must throw an error to pass");
			
			// Act
			act = function () { testApp.registerListener("incorrectEventType", undefined); };
	
			// Assert
			assert.raises(act, "Registering a listener on an incorrect event must throw an error to pass");
		});
		
		QUnit.test("fireEvent", function (assert) {
			
			// #Test 1
			
			// Arrange
			var testApp = new Application();
			var eventHandlerFiredCounter = 0;
			var eventHandler = function () { eventHandlerFiredCounter++; };
			
			// Act
			testApp.registerListener("onStateChange", eventHandler);
			testApp.fireEvent("onStateChange", []);
			
			// Assert
			assert.equal(eventHandlerFiredCounter, 1, "The number of event handlers contacted after a 'fireEvent' call with one eventHandler registered is one.");
			
			// #Test 2
			
			// Arrange
			var testApp = new Application();
			var eventHandlerFiredCounter = 0;
			var eventHandler = function () { eventHandlerFiredCounter++; };
			
			// Act
			testApp.registerListener("onStateChange", eventHandler);
			testApp.fireEvent("onStateChange", []);
			testApp.deregisterListener("onStateChange", eventHandler);
			testApp.fireEvent("onStateChange", []);
			
			// Assert
			assert.equal(eventHandlerFiredCounter, 1, "Registering a listener, firing an event, deregistering the listener" +
					" and then firing the event again should result in a total of one calls to the event handler.");
		});
		
		QUnit.test("createTileType", function (assert) {
			
			// Arrange
			var testApp = new Application();
			var mockCallback = function (tileTypeClass) {};
			
			// Act
			var act = function () { testApp.createTileType("Wall", "TileType: Wall override isRotatable with false", mockCallback); };
			
			// Assert
			assert.raises(act, "Creating a new tile type without first initializing the application (parser), should throw an error");
		});
		
	});
	
});